/*
Navicat MySQL Data Transfer

Source Server         : local_mysql
Source Server Version : 50173
Source Host           : localhost:3306
Source Database       : graduationdb

Target Server Type    : MYSQL
Target Server Version : 50173
File Encoding         : 65001

Date: 2018-06-14 21:35:30
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_application
-- ----------------------------
DROP TABLE IF EXISTS `t_application`;
CREATE TABLE `t_application` (
  `main_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '默认主键 没实际含义',
  `apply_id` varchar(30) NOT NULL DEFAULT '' COMMENT '申请id，时间戳',
  `asset_id` int(11) DEFAULT NULL COMMENT '申请的设备id',
  `asset_name` varchar(30) DEFAULT NULL COMMENT '设备名称',
  `apply_number` int(11) NOT NULL COMMENT '申请数量',
  `apply_person` varchar(30) NOT NULL COMMENT '申请人，可以是工作人员申请，也可以是部门管理员申请。工作人员申请 对应t_staff表中的staff_id；部门管理员申请对应t_department_manager表中的manager_id',
  `apply_date` datetime DEFAULT NULL COMMENT '申请时间',
  `apply_reason` text COMMENT '申请原因',
  `apply_state` tinyint(1) DEFAULT NULL COMMENT '申请标识；0领用申请，1入库申请（新增），2流动申请，3维修，4报废',
  `asset_in_type` tinyint(1) DEFAULT NULL COMMENT '在apply_state为1 设备入库的时候才有这个字段。asset_in_type=0,为设备添加数量；为1时，是新增设备类型。操作t_assets_info表',
  `approve_person1` varchar(30) DEFAULT NULL COMMENT '审批人1，对应t_staff表中的staff_id;如果申请人是部门管理员，则这一字段不填，为空',
  `approve_person2` varchar(30) DEFAULT NULL COMMENT '审批人2 如果是普通员工申请，则需要两个审批人；一个部门管理员，一个超级管理员。这步是超级管理员审批',
  `approve_date` datetime DEFAULT NULL COMMENT '审批时间',
  `approve_state` tinyint(1) DEFAULT NULL COMMENT '审批状态 0待审核，1部门管理员审批通过，2部门管理员审批不通过，3审批成功（超级管理员审批通过），4审批失败',
  `approve_remark` text COMMENT '审批备注',
  PRIMARY KEY (`main_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='申请表\r\n包括领用申请、入库申请、流动申请等。主要靠apply_state字段标识';

-- ----------------------------
-- Records of t_application
-- ----------------------------
INSERT INTO `t_application` VALUES ('1', '20180403104300', '101', '键盘', '10', 'c-001', '2018-04-03 10:43:52', '个人需要键盘,嗯。', '1', '0', 'd-001', 's-1', '2018-05-31 00:00:00', '1', null);
INSERT INTO `t_application` VALUES ('3', '20180412142114', '100', 'ThinkPad', '15', 'c-001', '2018-04-12 14:21:19', 'sadf', '3', '0', 'd-001', 's-1', '2018-05-31 00:00:00', '3', null);
INSERT INTO `t_application` VALUES ('4', '20180419005053', '100', 'ThinkPad', '20', 'c-001', '2018-04-19 00:50:55', 'fasdfa', '0', '1', 'd-001', 's-1', null, '3', null);
INSERT INTO `t_application` VALUES ('5', '20180419010204', '100', 'ThinkPad', '20', 'c-001', '2018-04-19 01:02:17', 'kkkk', '4', '0', 'd-001', null, null, '3', null);
INSERT INTO `t_application` VALUES ('6', '20180425174156', '101', '键盘', '30', 'c-001', '2018-04-25 17:41:59', 'ooooo', '1', '0', 'd-001', null, null, '0', null);
INSERT INTO `t_application` VALUES ('7', '20180507140657', '101', '键盘', '5', 'c-001', '2018-05-07 00:00:00', '键盘不够用了', '3', '0', 'd-001', '', '2018-05-08 00:00:00', '1', '');

-- ----------------------------
-- Table structure for t_assets_class
-- ----------------------------
DROP TABLE IF EXISTS `t_assets_class`;
CREATE TABLE `t_assets_class` (
  `class_id` int(11) NOT NULL COMMENT '类别ID',
  `class_name` varchar(30) NOT NULL COMMENT '类别名称',
  `class_description` text COMMENT '类别描述',
  PRIMARY KEY (`class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='资产类别信息表';

-- ----------------------------
-- Records of t_assets_class
-- ----------------------------
INSERT INTO `t_assets_class` VALUES ('1001', '电脑', '各种电脑');
INSERT INTO `t_assets_class` VALUES ('1002', '键盘', '键盘呗');
INSERT INTO `t_assets_class` VALUES ('1003', '鼠标', '鼠标呗');
INSERT INTO `t_assets_class` VALUES ('1004', '显示器', '电脑的显示器');
INSERT INTO `t_assets_class` VALUES ('1005', '饮水机', '自带过滤，富氢设备');

-- ----------------------------
-- Table structure for t_assets_info
-- ----------------------------
DROP TABLE IF EXISTS `t_assets_info`;
CREATE TABLE `t_assets_info` (
  `asset_id` int(11) NOT NULL COMMENT '资产id',
  `asset_name` varchar(30) NOT NULL COMMENT '资产名称',
  `manufacture` varchar(50) NOT NULL COMMENT '生产厂家',
  `inventory` int(11) DEFAULT NULL COMMENT '库存数量(剩余数)',
  `in_person` varchar(20) DEFAULT NULL COMMENT '入库人',
  `in_date` datetime DEFAULT NULL COMMENT '入库时间',
  `class_id` int(11) DEFAULT NULL COMMENT '资产类别Id',
  `total_inventory` int(11) DEFAULT NULL COMMENT '总数量（每次入库都在原来数量的基础上增加）',
  `remark` text COMMENT '备注描述',
  PRIMARY KEY (`asset_id`),
  KEY `class_id` (`class_id`),
  CONSTRAINT `class_id` FOREIGN KEY (`class_id`) REFERENCES `t_assets_class` (`class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='资产信息表';

-- ----------------------------
-- Records of t_assets_info
-- ----------------------------
INSERT INTO `t_assets_info` VALUES ('100', 'ThinkPad', '广东联想公司', '100', '张三', '2018-03-05 14:18:46', '1001', '100', '2018-03-05进了10台电脑');
INSERT INTO `t_assets_info` VALUES ('101', '键盘', '浙江温州', '150', 's-1', '2018-05-31 10:41:03', '1002', '150', '');
INSERT INTO `t_assets_info` VALUES ('102', '鼠标', '上海雷神', '90', 's-1', '2018-05-08 09:58:59', '1003', '90', 'sdfweef');
INSERT INTO `t_assets_info` VALUES ('103', '美的', '北京美的', '20', 's-1', '2018-05-08 00:00:00', '1005', '25', '');

-- ----------------------------
-- Table structure for t_depart
-- ----------------------------
DROP TABLE IF EXISTS `t_depart`;
CREATE TABLE `t_depart` (
  `depart_id` int(11) NOT NULL COMMENT '部门id',
  `depart_name` varchar(30) DEFAULT NULL COMMENT '部门名称',
  `depart_description` text COMMENT '部门描述'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='部门表';

-- ----------------------------
-- Records of t_depart
-- ----------------------------
INSERT INTO `t_depart` VALUES ('501', '运维', '系统的运行与维护');
INSERT INTO `t_depart` VALUES ('502', '接口', '开发微信的相关内容');
INSERT INTO `t_depart` VALUES ('503', '测试', '测试应用和系统');

-- ----------------------------
-- Table structure for t_department_manager
-- ----------------------------
DROP TABLE IF EXISTS `t_department_manager`;
CREATE TABLE `t_department_manager` (
  `manager_id` varchar(30) NOT NULL COMMENT '部门管理员id',
  `manager_name` varchar(30) DEFAULT NULL COMMENT '部门管理员姓名',
  `depart_id` int(11) NOT NULL COMMENT '对应的部门id',
  `super_id` varchar(30) NOT NULL COMMENT '超级管理员id',
  `contact_phone` varchar(20) DEFAULT NULL COMMENT '联系方式',
  `email` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`manager_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='部门管理员表';

-- ----------------------------
-- Records of t_department_manager
-- ----------------------------
INSERT INTO `t_department_manager` VALUES ('d-001', '陈红', '501', 's-1', '13283718829', 'chenhong@163.com');
INSERT INTO `t_department_manager` VALUES ('d-002', '李飒', '502', 's-1', '15090696259', 'lisa@sina.cn');
INSERT INTO `t_department_manager` VALUES ('d-003', '龚晨', '503', 's-1', '18733456271', 'gongchen@qq.com');

-- ----------------------------
-- Table structure for t_maintain
-- ----------------------------
DROP TABLE IF EXISTS `t_maintain`;
CREATE TABLE `t_maintain` (
  `maintain_id` varchar(20) NOT NULL COMMENT '维修id',
  `asset_id` int(11) NOT NULL COMMENT '资产id',
  `maintain_reason` text COMMENT '维修原因',
  `in_date` datetime DEFAULT NULL COMMENT '送修时间',
  `maintain_result` varchar(2) DEFAULT NULL COMMENT '维修结果 0修好了，1没修好',
  `maintain_feedback_date` datetime DEFAULT NULL COMMENT '维修反馈时间',
  PRIMARY KEY (`maintain_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='设备维修表';

-- ----------------------------
-- Records of t_maintain
-- ----------------------------

-- ----------------------------
-- Table structure for t_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_menu`;
CREATE TABLE `t_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(30) NOT NULL COMMENT '菜单名称',
  `menu_right_code` varchar(50) NOT NULL COMMENT '菜单权限编码',
  `in_date` datetime DEFAULT NULL COMMENT '录入时间',
  `menu_description` text COMMENT '菜单描述',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单表';

-- ----------------------------
-- Records of t_menu
-- ----------------------------

-- ----------------------------
-- Table structure for t_password
-- ----------------------------
DROP TABLE IF EXISTS `t_password`;
CREATE TABLE `t_password` (
  `auto_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自动增长的表id，字段没什么实际含义',
  `user_id` varchar(30) NOT NULL COMMENT '用户id。对应staff_id,depart_manager_id和super_manager_id',
  `user_name` varchar(30) DEFAULT NULL,
  `user_password` varchar(255) NOT NULL COMMENT '用户密码',
  `contact_phone` varchar(20) DEFAULT NULL COMMENT '联系电话（如果忘记密码可以用来找回密码）',
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`auto_id`),
  KEY `staff_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='员工密码表';

-- ----------------------------
-- Records of t_password
-- ----------------------------
INSERT INTO `t_password` VALUES ('1', 'c-001', '陈思', 'ad07fb25aa2d3a9f96ee12f25e0be902', '18736072766', '3');
INSERT INTO `t_password` VALUES ('3', 'd-002', '李飒', 'dfadc94b33663b2707dff9523e4f1b43', '15090696259', '2');
INSERT INTO `t_password` VALUES ('4', 's-1', null, 'e0eb969fb7f476ee3f6721ec5947d735', '18736072776', '1');
INSERT INTO `t_password` VALUES ('5', 'd-001', '陈红', 'dfadc94b33663b2707dff9523e4f1b43', '13283718829', '2');
INSERT INTO `t_password` VALUES ('6', 'd-003', '龚晨', 'dfadc94b33663b2707dff9523e4f1b43', '18733456271', '2');
INSERT INTO `t_password` VALUES ('7', 'c-003', '张三', 'ad07fb25aa2d3a9f96ee12f25e0be902', '13277658892', '3');

-- ----------------------------
-- Table structure for t_right
-- ----------------------------
DROP TABLE IF EXISTS `t_right`;
CREATE TABLE `t_right` (
  `right_id` tinyint(1) NOT NULL COMMENT '权限id',
  `right_name` varchar(10) NOT NULL COMMENT '权限名称',
  PRIMARY KEY (`right_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='操作权限表\r\n1、入库 \r\n2、领用\r\n3、流动\r\n4、报废\r\n5、维修';

-- ----------------------------
-- Records of t_right
-- ----------------------------
INSERT INTO `t_right` VALUES ('1', '入库');
INSERT INTO `t_right` VALUES ('2', '领用');
INSERT INTO `t_right` VALUES ('3', '流动');
INSERT INTO `t_right` VALUES ('4', '报废');
INSERT INTO `t_right` VALUES ('5', '维修');

-- ----------------------------
-- Table structure for t_role
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) NOT NULL COMMENT '角色姓名',
  `role_description` text COMMENT '角色描述',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='角色表';

-- ----------------------------
-- Records of t_role
-- ----------------------------
INSERT INTO `t_role` VALUES ('1', '超级管理员', '固定资产管理系统的超级管理员，拥有最大的权限，可以查询中心所有的设备信息，审批使用者提交的申请等流程。');
INSERT INTO `t_role` VALUES ('2', '部门管理员', '部门管理员是由超级管理员创建并分发给各个部门负责人，该管理员权限较低，只能管理自己部门中心的设备以及审批部门人员设备申请、设备归还、设备报废维修等请求。');
INSERT INTO `t_role` VALUES ('3', '工作人员', '工作人员：查看个人领用设备信息，无操作权限，可申请领用、归还、报废、维修');

-- ----------------------------
-- Table structure for t_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_role_menu`;
CREATE TABLE `t_role_menu` (
  `role_map_id` int(11) NOT NULL COMMENT '角色ID',
  `menu_map_id` int(11) NOT NULL COMMENT '菜单id',
  PRIMARY KEY (`role_map_id`,`menu_map_id`),
  KEY `menu_map_id` (`menu_map_id`),
  CONSTRAINT `t_role_menu_ibfk_1` FOREIGN KEY (`role_map_id`) REFERENCES `t_role` (`role_id`),
  CONSTRAINT `t_role_menu_ibfk_2` FOREIGN KEY (`menu_map_id`) REFERENCES `t_menu` (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色-菜单映射表';

-- ----------------------------
-- Records of t_role_menu
-- ----------------------------

-- ----------------------------
-- Table structure for t_scrap
-- ----------------------------
DROP TABLE IF EXISTS `t_scrap`;
CREATE TABLE `t_scrap` (
  `scrap_id` varchar(20) NOT NULL COMMENT '报废id',
  `asset_id` int(11) NOT NULL COMMENT '资产id，对应t_assets_info表中的asset_id',
  `maintain_id` int(11) DEFAULT NULL COMMENT '送修id',
  `scrap_time` datetime NOT NULL COMMENT '报废时间',
  `scrap_person` varchar(20) DEFAULT NULL COMMENT '报废处理人',
  PRIMARY KEY (`scrap_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='报废表';

-- ----------------------------
-- Records of t_scrap
-- ----------------------------

-- ----------------------------
-- Table structure for t_staff
-- ----------------------------
DROP TABLE IF EXISTS `t_staff`;
CREATE TABLE `t_staff` (
  `staff_id` varchar(30) NOT NULL COMMENT '员工ID',
  `depart_id` int(11) DEFAULT NULL COMMENT '部门ID',
  `role_id` int(11) NOT NULL COMMENT '对应的角色ID',
  `contact_phone` varchar(20) DEFAULT NULL COMMENT '联系电话',
  `staff_name` varchar(30) DEFAULT NULL COMMENT '员工姓名',
  `higherup_id` varchar(30) NOT NULL COMMENT '上级的员工id（普通员工的上级是部门管理员，部门管理员的上级是超级管理员）',
  `email` varchar(30) DEFAULT NULL COMMENT '邮箱',
  PRIMARY KEY (`staff_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `t_staff_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `t_role` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='工作员工信息表';

-- ----------------------------
-- Records of t_staff
-- ----------------------------
INSERT INTO `t_staff` VALUES ('c-001', '501', '3', '18736072766', '陈思', 'd-001', '157223522@qq.com');
INSERT INTO `t_staff` VALUES ('c-003', '503', '3', '13277658892', '张三', 'd-003', 'zhangsan3@163.com');

-- ----------------------------
-- Table structure for t_super_manager
-- ----------------------------
DROP TABLE IF EXISTS `t_super_manager`;
CREATE TABLE `t_super_manager` (
  `super_id` varchar(30) NOT NULL COMMENT '超级管理员id',
  `contact_phone` varchar(20) DEFAULT NULL COMMENT '联系电话',
  `description` text COMMENT '描述',
  `email` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`super_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='超级管理员表';

-- ----------------------------
-- Records of t_super_manager
-- ----------------------------
INSERT INTO `t_super_manager` VALUES ('s-1', '18736072776', '超级管理员哈哈哈哈哈哈哈', 'hucl5@asiainfo.com');

-- ----------------------------
-- Table structure for t_user_right
-- ----------------------------
DROP TABLE IF EXISTS `t_user_right`;
CREATE TABLE `t_user_right` (
  `user_id` varchar(30) NOT NULL COMMENT '员工id 对应t_staff中的staff_id',
  `right_id` tinyint(1) NOT NULL COMMENT '权限id 对应t_right表中的right_id',
  PRIMARY KEY (`user_id`,`right_id`),
  KEY `right_id` (`right_id`) USING BTREE,
  CONSTRAINT `t_user_right_ibfk_2` FOREIGN KEY (`right_id`) REFERENCES `t_right` (`right_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='员工权限表';

-- ----------------------------
-- Records of t_user_right
-- ----------------------------
INSERT INTO `t_user_right` VALUES ('c-001', '1');
INSERT INTO `t_user_right` VALUES ('d-003', '1');
INSERT INTO `t_user_right` VALUES ('c-001', '2');
INSERT INTO `t_user_right` VALUES ('c-001', '4');
