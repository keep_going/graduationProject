package com.soft.zzti.dao;

import java.util.List;

import com.soft.zzti.entity.TAssetClassBean;

public interface AssetClassDao {
	/**
	 * 查询请全部设备类别
	 * @return
	 */
	List<TAssetClassBean> selectAssetClassList() throws Exception;

}
