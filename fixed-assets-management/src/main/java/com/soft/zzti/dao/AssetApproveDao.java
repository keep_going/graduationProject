package com.soft.zzti.dao;
/**
 * 设备审批
 * @author Administrator
 *
 */

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.soft.zzti.entity.ApplicationAssetBean;
import com.soft.zzti.entity.AssetApproveQueryVo;
import com.soft.zzti.entity.TApplicationBean;
import com.soft.zzti.entity.TAssetsInfoBean;

public interface AssetApproveDao {
	/**
	 * 超级管理员的审核列表
	 * approve_state=1 && approve_person2=该超级管理员
	 * @param superManager
	 * @return
	 * @throws Exception
	 */
	List<ApplicationAssetBean> selectAssetApproveListBySuper(@Param("approveVo") AssetApproveQueryVo approveVo) throws Exception;

	/**
	 * 设备申请的审核
	 * 如果是部门管理员审核，需要传approve_person2、approve_state、mainID字段
	 * 如果是超级管理员审核，需要传approve_date、approve_state、approve_remark、mainID字段
	 * @param mainID
	 * @param approveState
	 * @return
	 * @throws Exception
	 */
	int updateAssetApproveState(@Param("applicaitonInfo") TApplicationBean applicaitonInfo) throws Exception;
}
