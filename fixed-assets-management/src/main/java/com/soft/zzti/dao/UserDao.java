package com.soft.zzti.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.soft.zzti.entity.TDepartManageBean;
import com.soft.zzti.entity.TPassword;
import com.soft.zzti.entity.TStaffBean;
import com.soft.zzti.entity.TSuperManageBean;

public interface UserDao {
	/**
	 * 用户登录检测
	 * 进t_password表中查找
	 * @param user_id
	 * @param user_password
	 * @return
	 */
	TPassword userLoginCheck(@Param("tPassword") TPassword tPassword) throws Exception;
	/**
	 * 修改密码
	 * @param tPassword
	 */
	void updatePassword(@Param("tPassword") TPassword tPassword) throws Exception;
	/**
	 * 查询普通员工的详细信息
	 * @param username
	 * @return
	 */
	TStaffBean selectStaffInfo(String username) throws Exception;
	/**
	 * 查询部门管理员的详细信息
	 * @param username
	 * @return
	 */
	TDepartManageBean selectDepartmentManagerInfo(String username) throws Exception;
	/**
	 * 查询超级管理员的详细信息
	 * @param username
	 * @return
	 */
	TSuperManageBean selectSuperManagerInfo(String username) throws Exception;
	/**
	 * 查询所有用户
	 * @param roleId
	 * @return
	 * @throws Exception
	 */
	List<TPassword> selectAll(@Param("roleId") Integer roleId) throws Exception;
	
	/**
	 * 插入t_password
	 * @param record
	 * @return
	 * @throws Exception
	 */
	int insertSelective(TPassword record) throws Exception;
}
