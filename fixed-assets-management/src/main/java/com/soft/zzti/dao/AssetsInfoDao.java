package com.soft.zzti.dao;
/**
 * 资产信息dao层
 * @author hucl
 *
 */

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.soft.zzti.entity.AssetsInfoQueryVo;
import com.soft.zzti.entity.TAssetsInfoBean;

public interface AssetsInfoDao {
	/**
	 * 根据设备id进行设备查询
	 * @param id
	 * @return
	 * @throws Exception
	 */
	TAssetsInfoBean selectAssetInfoByID(int id) throws Exception;
	/**
	 * 根据设备类别和设备名称进行模糊查询
	 * 查询所有
	 * @return
	 * @throws Exception
	 */
	List<TAssetsInfoBean> selectAssetInfoList(@Param("assetInfo") TAssetsInfoBean assetInfo) throws Exception;
	
	/**
	 * 设备查询需求
	 * 添加上了【待入库数量】
	 * 和剩余数量  
	 * 
	 * 库存数量：t_assets_info表中的 inventory字段
	 * 待入库数量 t_application中的apply_number字段
	 * 剩余数量 = 库存数量 + 待入库数量
	 * @param assetInfo
	 * @return
	 * @throws Exception
	 */
	List<AssetsInfoQueryVo> selectAssetInfoAddedAppyNumberList(@Param("assetInfo")TAssetsInfoBean assetInfo) throws Exception;

	/**
	 * 更新信息
	 * @param assetInfo
	 * @throws Exception
	 */
	void updateAssetInfoByParimayKey(@Param("assetInfo") TAssetsInfoBean assetInfo) throws Exception;
	/**
	 * 通过主键删除
	 * @param assetId
	 * @return
	 */
	int deleteByPrimaryKey(Integer assetId) throws Exception;
	/**
	 * 修改 全部信息
	 * @param assetInfo
	 * @return
	 * @throws Exception
	 */
	int updateByPrimaryKeySelective(TAssetsInfoBean assetInfo) throws Exception;
	/**
	 * 插入
	 * @param assetInfo
	 * @return
	 * @throws Exception
	 */
	int insertSelective(TAssetsInfoBean assetInfo) throws Exception;
	/**
	 * 查询所有
	 * @return
	 * @throws Exception
	 */
	List<TAssetsInfoBean> selectAll() throws Exception;
}
