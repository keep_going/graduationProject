package com.soft.zzti.dao;

import com.soft.zzti.entity.TSuperManageBean;

public interface SuperManagerDao {
	/**
	 * 根据主键修改信息
	 * @param record
	 * @return
	 */
	int updateByPrimaryKeySelective(TSuperManageBean record) throws Exception;
	/**
	 * 根据主键查询
	 * @param superId
	 * @return
	 * @throws Exception
	 */
	TSuperManageBean selectByPrimaryKey(String superId) throws Exception;

}
