package com.soft.zzti.dao;


import java.util.List;

import org.apache.ibatis.annotations.Param;



public interface RightDao {
	int deleteRight(@Param("userId") String userId,@Param("reduceRights") List<String> reduceRights) throws Exception;
	
	int insertRight(@Param("userId") String userId,@Param("increaseRights") List<String> increaseRights) throws Exception;
	
}
