package com.soft.zzti.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.soft.zzti.entity.TStaffBean;

/**
 * 员工t_staff表
 * @author hucl created on 2018/03/16
 *
 */
public interface StaffDao {
	/**
	 * 查询工作人员的操作权限
	 * @param staffID
	 * @return
	 */
	List<Integer> queryStaffRight(@Param("staffID") String staffID) throws Exception;
	/**
	 * 根据staffID查询工作人员的详细信息
	 * @param staffID
	 * @return
	 */
	TStaffBean queryStaffInfo(String staffID) throws Exception;
	/**
	 * 根据工作人员工号修改该人员的详情
	 * @param staffBean
	 * @throws Exception
	 */
	int updateStaffInfo(@Param("staffBean") TStaffBean staffBean) throws Exception;
	/**
	 * 查询所有
	 * @param staffBean
	 * @return
	 * @throws Exception
	 */
	List<TStaffBean> selectAll(TStaffBean staffBean) throws Exception;
	/**
	 * 通过主键删除
	 * @param staffId
	 * @return
	 */
	int deleteByPrimaryKey(String staffId);
	/**
	 * 新增普通员工
	 * @param record
	 * @return
	 */
	int insertSelective(TStaffBean record);
}
