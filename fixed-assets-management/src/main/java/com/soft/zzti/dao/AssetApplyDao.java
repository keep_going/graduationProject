package com.soft.zzti.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.soft.zzti.entity.ApplicationAssetBean;
import com.soft.zzti.entity.DataChartBean;
import com.soft.zzti.entity.TApplicationBean;

/**
 * 设备申请
 * @author hucl
 *
 */
public interface AssetApplyDao {
	/**
	 * 插入t_application新数据
	 * 设备入库申请
	 * @param applicationBean
	 * @throws Exception
	 */
	int insertAddDeviceApplication(@Param("applicationBean") TApplicationBean applicationBean) throws Exception;
	/**
	 * 查询普通员工自己名下的所有申请
	 * 根据时间段和员工工号staffID查询
	 * applicationAssetBean.startTime开始时间
	 * applicationAssetBean.endTime结束时间
	 * @param applicationAssetBean
	 * @return
	 * @throws Exception
	 */
	List<ApplicationAssetBean> selectStaffSelfApplication(@Param("applicationAssetBean") ApplicationAssetBean applicationAssetBean) throws Exception;

	/**
	 * 根据主键查询设备申请
	 * @param mainID
	 * @return
	 * @throws Exception
	 */
	TApplicationBean selectApplicationByPrimaryKey(int mainID) throws Exception;
	/**
	 * 根据主键删除
	 * @param mainId
	 * @return
	 */
	int deleteByPrimaryKey(Integer mainId);
	/**
	 * 查询所有
	 * @param queryCondition
	 * @return
	 * @throws Exception
	 */
	List<ApplicationAssetBean> selectAll(ApplicationAssetBean queryCondition) throws Exception;
	/**
	 * 修改
	 * @param record
	 * @return
	 */
	int updateByPrimaryKeySelective(TApplicationBean record);
	/**
	 * 查询所有的assetid
	 * @return
	 * @throws Exception
	 */
	List<Integer> selectAssetId() throws Exception;
	/**
	 * 根据assetId查询它下面的apply_state
	 * @return
	 * @throws Exception
	 */
	List<DataChartBean> selectApplyStateByAssetId(int assetId) throws Exception;
	/**
	 * 查询设备使用率
	 * @param assetId
	 * @return
	 * @throws Exception
	 */
	List<DataChartBean> selectAssetRate(int assetId) throws Exception;
}
