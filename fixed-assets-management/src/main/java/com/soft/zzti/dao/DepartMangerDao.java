package com.soft.zzti.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.soft.zzti.entity.TDepartManageBean;
import com.soft.zzti.entity.TStaffBean;

public interface DepartMangerDao {
	/**
	 * 根据主键删除
	 * @param managerId
	 * @return
	 * @throws Exception
	 */
	int deleteByPrimaryKey(String managerId) throws Exception;
	
	/**
	 * 查询所有
	 * 可以根据部门管理员id/部门管理员姓名查询
	 * @param dmBean
	 * @return
	 * @throws Exception
	 */
	List<TDepartManageBean> selectAll(TDepartManageBean dmBean) throws Exception;
	/**
	 * 插入
	 * @param record
	 * @return
	 */
	int insertSelective(TDepartManageBean record) throws Exception;
	/**
	 * 更新
	 * @param record
	 * @return
	 */
	int updateByPrimaryKeySelective(TDepartManageBean record) throws Exception;
	
	/**
	 * 根据主键查询
	 * @param managerId
	 * @return
	 * @throws Exception
	 */
	TDepartManageBean selectByPrimaryKey(String managerId) throws Exception;
	/**
	 * 查询部门下的所有普通员工
	 * @param departId
	 * @param staffName
	 * @return
	 * @throws Exception
	 */
	List<TStaffBean> selectStaffByDepartId(@Param("departId")int departId,@Param("staffName")String staffName) throws Exception;

}
