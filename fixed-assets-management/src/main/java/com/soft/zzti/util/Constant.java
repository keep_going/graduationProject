package com.soft.zzti.util;

public class Constant {
	/*
	 * 处理申请的 审批状态 
	 */
	//待审核
	public static final int APPROVE_STATE_WAIT = 0;
	//部门管理员审核通过
	public static final int APPROVE_STATE__DEPART_SUCCESS = 1;
	//部门管理员审核不通过
	public static final int APPROVE_STATE_DEPART_FAIL = 2;
	//超级管理员审核通过（审核成功）
	public static final int APPROVE_SATATE_FINAL_SUCCESS = 3;
	//超级管理员审核不通过（审核失败）
	public static final int APPROVE_STATE_FINAL_FAIL = 4;
	//向前端相应的状态码 成功
	public static final String SUCCESS = "0";
	//失败
	public static final String ERROR = "1";
	//返回的信息节点
	public static final String MSG_FLAG = "msgFlag";
	public static final String ERROR_MSG = "errorMsg";
	public static final String RESULT_JSON = "resultJson";
	
	//设备申请标识 领用申请
	public static final int APPLY_STATE_LINGYONG = 0;
	//设备申请标识 入库申请
	public static final int APPLY_STATE_RUKU = 1;
	//设备申请标识 流动申请
	public static final int APPLY_STATE_LIUDONG = 2;
	//设备申请标识 维修申请
	public static final int APPLY_STATE_WEIXIU = 3;
	//设备申请标识 报废申请
	public static final int APPLY_STATE_BAOFEI = 4;
	
}
