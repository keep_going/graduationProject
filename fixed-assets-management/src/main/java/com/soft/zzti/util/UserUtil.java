package com.soft.zzti.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class UserUtil {
	
	public static int getUserRoleId(String username) {
		/*
    	 * 判断登录人的角色
    	 * 普通工作人员 username第一位是 c对应role_id=3
    	 * 部门管理员 d对应role_id=2
    	 * 超级管理员 s对应role_id=1
    	 */
    	String firstCharacter = username.substring(0, 1);
    	//设置role_id默认值
    	int role_id = 3;
    	switch(firstCharacter) {
    	case "d":
    		role_id = 2;
    		break;
    	case "s":
    		role_id = 1;
    		break;
    	default :
    		break;
    	}
		return role_id;
	}
	/**
	 * 日期转换
	 * @return
	 */
	public static String dateFormat() {
		 Date currentTime = new Date();
		 SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		 String dateString = formatter.format(currentTime);
		 return dateString;
	}
	/**
	 * java.sql.Date 和 java.util.Date相互转换
	 * @return
	 */
	public static java.sql.Date sqlDateFormat() {
		 Date currentTime = new Date();
		 java.sql.Date sqlDate = new java.sql.Date(currentTime.getTime());
		 return sqlDate;
	}
}
