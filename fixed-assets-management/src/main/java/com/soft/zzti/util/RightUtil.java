package com.soft.zzti.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RightUtil {
	/**
	 * 权限减少的数值
	 * @param oldRight
	 * @param newRight
	 * @return
	 */
	public static List<String> rightReduce(String[] oldRight,String[] newRight){
		List<String> newR = Arrays.asList(newRight);
		List<String> reduce = new ArrayList<>();
		
		for(int i=0;i<oldRight.length;i++) {
			if(!newR.contains(oldRight[i])) {
				reduce.add(oldRight[i]);
			}
		}
		return reduce;
	}
	/**
	 * 权限增加的数值
	 * @param oldRight
	 * @param newRight
	 * @return
	 */
	public static List<String> rightIncrease(String[] oldRight,String[] newRight){
		List<String> oldR = Arrays.asList(oldRight);
		List<String> increase = new ArrayList<>();
		
		for(int i=0;i<newRight.length;i++) {
			if(!oldR.contains(newRight[i])) {
				increase.add(newRight[i]);
			}
		}
		return increase;
	}
	
}
