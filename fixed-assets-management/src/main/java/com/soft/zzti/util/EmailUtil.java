package com.soft.zzti.util;

import java.security.GeneralSecurityException;
import java.util.Properties;
import java.util.UUID;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.sun.mail.util.MailSSLSocketFactory;

public class EmailUtil {
	
	/**
	 * @param subject 邮件主题
	 * @param email to邮箱
	 * @param message 邮件信息
	 * @throws GeneralSecurityException
	 * @throws MessagingException
	 */
	public static void sendEmail(String email , String subject, String message) throws GeneralSecurityException, MessagingException{
		//boolean flag = false;
		Properties props = new Properties();

	    // 开启debug调试
	    props.setProperty("mail.debug", "true");
	    // 发送服务器需要身份验证
	    props.setProperty("mail.smtp.auth", "true");
	    // 设置邮件服务器主机名
	    props.setProperty("mail.host", "smtp.qq.com");
	    // 发送邮件协议名称
	    props.setProperty("mail.transport.protocol", "smtp");

	    MailSSLSocketFactory sf = new MailSSLSocketFactory();
	    sf.setTrustAllHosts(true);
	    props.put("mail.smtp.ssl.enable", "true");
	    props.put("mail.smtp.ssl.socketFactory", sf);

	    Session session = Session.getInstance(props);

	    Message msg = new MimeMessage(session);
	    msg.setSubject("来自 亚信科技固定资产管理系统 "+subject);
	    StringBuilder builder = new StringBuilder();
	    builder.append(message);
	    //builder.append("\n页面爬虫错误");
	    //builder.append("\n时间 " + TimeTool.getCurrentTime());
	    msg.setText(builder.toString());
	    msg.setFrom(new InternetAddress("2532823784@qq.com"));

	    Transport transport = session.getTransport();
	    //ppgifvpldrdqdiaa 授权码
	    //transport.connect("smtp.qq.com", "**发送人的邮箱地址**", "**发送人的邮箱密码或者授权码**");
	    transport.connect("smtp.qq.com", "2532823784@qq.com", "ppgifvpldrdqdiaa");
	    //transport.sendMessage(msg, new Address[] { new InternetAddress("**接收人的邮箱地址**") });
	    transport.sendMessage(msg, new Address[] { new InternetAddress(email) });
	    transport.close();
	    
	}
	
	
	//随机生成四位验证码
	public static String getRandomCode(){
		//调用java.util.UUID.randomUUID()方法，随机生验证码
		String logid = UUID.randomUUID().toString(); 
		//System.out.println(logid);
		String code = logid.substring(logid.indexOf("-")+1, logid.indexOf("-")+5);
		//System.out.println(code);
		return code; 
	}
	
//	//随机生成 四位验证码
//	public String randomCode(){
//		String code = "";
//		String str = "0,1,2,3,4,5,6,7,8,9,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z";
//		Random rand = new Random();// 创建Random类的对象rand
//		String str2[] = str.split(",");// 将字符串以,分割
//		int index = 0;
//		code = "";// 清空字符串对象randStr中的值
//		for (int i = 0; i < 4; ++i) {
//			index = rand.nextInt(str2.length - 1);// 在0到str2.length-1生成一个伪随机数赋值给index
//			code += str2[index];// 将对应索引的数组与randStr的变量值相连接
//		}
//		System.out.println("验证码：" + code);// 输出所求的验证码的值
//		return code;
//	}
	

}
