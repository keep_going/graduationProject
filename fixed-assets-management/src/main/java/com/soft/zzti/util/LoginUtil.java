package com.soft.zzti.util;

import javax.servlet.http.HttpSession;

import com.alibaba.fastjson.JSONObject;
/**
 * 登录相关
 * @author hucl created on 2018/04/11
 *
 */
public class LoginUtil {
	/**
	 * 获取登录人保存在session中的信息
	 * @param session
	 * @return
	 */
	public static JSONObject getUserSession(HttpSession session) {
		JSONObject sessionJson = new JSONObject();
		JSONObject userJson = new JSONObject();
		try {
			sessionJson = (JSONObject) session.getAttribute("userSession");
			userJson =  sessionJson.getJSONObject("userInfo");
			System.out.println("-----session中的信息----"+sessionJson.toJSONString());
			System.out.println("-----userInfo中的信息----"+userJson.toString());
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return userJson;
	}

}
