package com.soft.zzti.util;

public class AssetUtil {
	/**
	 * 申请状态转换
	 * @param applyState
	 * @return
	 */
	public static String applyStateString(int applyState) {
		String applyStr = "";
		switch (applyState) {
		case 0:
			applyStr = "领用申请";
			break;
		case 1:
			applyStr = "入库申请";
			break;
		case 2:
			applyStr = "流动申请";
			break;
		case 3:
			applyStr = "维修申请";
			break;
		case 4:
			applyStr = "报废申请";
			break;
		default:
			applyStr = "未知申请";
			break;
		}
		return applyStr;
	}
	/**
	 * 审批状态转换
	 * @param approveState
	 * @return
	 */
	public static String approveStateStr(int approveState) {
		String approveStr = "";
		switch (approveState) {
		case 0:
			approveStr = "待审核";
			break;
		case 1:
			approveStr = "部门管理员审批通过";
			break;
		case 2:
			approveStr = "部门管理员审批不通过";
			break;
		case 3:
			approveStr = "审批成功";
			break;
		case 4:
			approveStr = "审批失败";
			break;
		default:
			break;
		}
		return approveStr;
	}

}
