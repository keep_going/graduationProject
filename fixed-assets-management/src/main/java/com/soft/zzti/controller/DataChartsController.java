package com.soft.zzti.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.soft.zzti.entity.DataChartBean;
import com.soft.zzti.service.AssetApplyService;
import com.soft.zzti.util.Constant;

/**
 * 设备分析图表
 * @author hucl
 *
 */
@Controller
@RequestMapping("/chartsData")
public class DataChartsController {
	@Autowired
	private AssetApplyService assetApplyService;
	
	/**
	 * 获取所有申请的设备id
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/getAllApplyAssetID",method= {RequestMethod.POST})
	public String getAllApplyAssetID() {
		JSONObject responseJson = new JSONObject();
		List<Object> stateList = new ArrayList<>();
		JSONObject resultJson = new JSONObject();
		//设备状态数组
		List<String> stateName = new ArrayList<String>();
		try {
			List<Integer> lists = assetApplyService.getApplyAllAssetId();
			for(int assetId : lists) {
				List<DataChartBean> states = assetApplyService.getApplyStateByAssetId(assetId);
				for(DataChartBean item : states) {
					stateName.add(item.getApplyStateName());
				}
				if(!stateName.contains("领用申请")) {
					DataChartBean lingyong = new DataChartBean();
					lingyong.setApplyNumber(0);
					lingyong.setApplyStateName("领用申请");
					states.add(lingyong);
				}
				if(!stateName.contains("维修申请")) {
					DataChartBean weixiu = new DataChartBean();
					weixiu.setApplyNumber(0);
					weixiu.setApplyStateName("维修申请");
					states.add(weixiu);
				}
				if(!stateName.contains("报废申请")) {
					DataChartBean baofei = new DataChartBean();
					baofei.setApplyNumber(0);
					baofei.setApplyStateName("报废申请");
					states.add(baofei);
				}
				if(!stateName.contains("入库申请")) {
					DataChartBean ruku = new DataChartBean();
					ruku.setApplyNumber(0);
					ruku.setApplyStateName("入库申请");
					states.add(ruku);
				}
				//清空数组
				stateName.clear();
				stateList.add(states);
			}
			resultJson.put("assetIds", lists);
			resultJson.put("states", stateList);
			responseJson.put(Constant.MSG_FLAG, Constant.SUCCESS);
			responseJson.put(Constant.RESULT_JSON, resultJson);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			responseJson.put(Constant.MSG_FLAG, Constant.ERROR);
			responseJson.put(Constant.ERROR_MSG, "获取所有申请的设备id失败："+e.getMessage());
			e.printStackTrace();
		}
		return responseJson.toJSONString();
	}
	
	/**
	 * 根据assetId查询它的所有状态
	 * @param assetId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/getApplyState",method= {RequestMethod.POST})
	public String getApplyStateByAssetId(@RequestParam(value="assetId",required=true) int assetId) {
		JSONObject responseJson = new JSONObject();
		try {
			List<DataChartBean> lists = assetApplyService.getApplyStateByAssetId(assetId);
			responseJson.put(Constant.MSG_FLAG, Constant.SUCCESS);
			responseJson.put(Constant.RESULT_JSON, lists);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			responseJson.put(Constant.MSG_FLAG, Constant.ERROR);
			responseJson.put(Constant.ERROR_MSG, "根据assetId查询它下面的所有状态失败："+e.getMessage());
			e.printStackTrace();
		}
		return responseJson.toJSONString();
	}
	
	/**
	 * 设备使用率
	 * @param assetId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/getAssetRate",method= {RequestMethod.POST})
	public String getAssetRateByAssetId(@RequestParam(value="assetId",required=true) int assetId) {
		JSONObject responseJson = new JSONObject();
		try {
			List<DataChartBean> lists = assetApplyService.getAssetUserRate(assetId);
			responseJson.put(Constant.MSG_FLAG, Constant.SUCCESS);
			responseJson.put(Constant.RESULT_JSON, lists);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			responseJson.put(Constant.MSG_FLAG, Constant.ERROR);
			responseJson.put(Constant.ERROR_MSG, "根据assetId查询设备使用率失败："+e.getMessage());
			e.printStackTrace();
		}
		return responseJson.toJSONString();
	}

}
