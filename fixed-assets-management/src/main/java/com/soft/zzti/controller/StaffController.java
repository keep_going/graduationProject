package com.soft.zzti.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.soft.zzti.entity.BaseBean;
import com.soft.zzti.entity.ResponseJsonBean;
import com.soft.zzti.entity.TStaffBean;
import com.soft.zzti.service.StaffService;
import com.soft.zzti.util.Constant;
import com.soft.zzti.util.LoginUtil;

@Controller
@RequestMapping("/staff")
public class StaffController {
	@Autowired
	private StaffService staffService;
	private static Logger log = LoggerFactory.getLogger(StaffController.class);
	
	/**
	 * 修改工作人员信息
	 * @param staffBean
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/updateStaffInfo",method= {RequestMethod.POST})
	public ResponseJsonBean modifyStaffInfo(TStaffBean staffBean) {
		ResponseJsonBean jsonBean = new ResponseJsonBean();
		String staffID = staffBean.getStaffID();
		
		if(StringUtils.isEmpty(staffID)) {
			jsonBean.setMsgFlag(Constant.ERROR);
			jsonBean.setErrorMsg("员工工号不能为空");
			return jsonBean;
		}
		try {
			int result = staffService.updateStaffInfo(staffBean);
			log.debug("修改工作人员信息---modifyStaffInfo()--执行结果-"+result);
			jsonBean.setMsgFlag(Constant.SUCCESS);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			jsonBean.setMsgFlag(Constant.ERROR);
			jsonBean.setErrorMsg(e.getMessage());
			e.printStackTrace();
		}
		return jsonBean;
	}
	/**
	 * 查询所有普通员工的信息
	 * @param staffInfo
	 * @param baseBean
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/getAllStaffInfo",method= {RequestMethod.POST})
	public String getAllStaff(TStaffBean staffInfo,BaseBean baseBean) {
		JSONObject responseJson = new JSONObject();
		int pageNo = baseBean.getStart() == null?1:baseBean.getStart()+1;
		int pageSize = baseBean.getLength() == null?10:baseBean.getLength();
		log.debug("---pageNo="+pageNo+"---,pageSize="+pageSize);
		//分页设置
		PageInfo<TStaffBean> staffPage = new PageInfo<TStaffBean>();
		try {
			staffPage = staffService.getAllStaffInfo(staffInfo, pageNo, pageSize);
			responseJson.put(Constant.MSG_FLAG, Constant.SUCCESS);
			responseJson.put("resultJson", staffPage.getList());
			responseJson.put("recordsTotal", staffPage.getTotal());
			responseJson.put("recordsFiltered", staffPage.getTotal());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			responseJson.put(Constant.MSG_FLAG, Constant.ERROR);
			responseJson.put(Constant.ERROR_MSG, e.getMessage());
			e.printStackTrace();
		}
		return responseJson.toJSONString();
	}
	/**
	 * 根据普通员工工号删除员工
	 * @param staffID
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/doDeleteStaffInfo",method= {RequestMethod.POST})
	public String doDeleteStaff(@RequestParam(value="staffID",required=true) String staffID) {
		JSONObject responseJson = new JSONObject();
		
		try {
			int result = staffService.deleteStaff(staffID);
			log.debug("doDeleteStaff()----删除结果："+result);
			responseJson.put(Constant.MSG_FLAG, Constant.SUCCESS);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			responseJson.put(Constant.MSG_FLAG, Constant.ERROR);
			responseJson.put(Constant.ERROR_MSG, "普通员工删除失败："+e.getMessage());
			e.printStackTrace();
		}
		return responseJson.toJSONString();
	}
	/**
	 * 查询普通员工个人信息
	 * @param staffId
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/getStaffInfoByStaffId",method= {RequestMethod.GET})
	public String doGetStaffInfo(@RequestParam(value="id",required=true) String staffId,Model model) throws Exception {
		log.debug("查询普通员工个人信息--doGetStaffInfo()---入参-："+staffId);
		TStaffBean staffInfo = staffService.getStaffByStaffId(staffId);
		model.addAttribute("staffInfo",staffInfo);
		return "superManager/userManage/CommonInfoDetail";
	}
	/**
	 * 部门管理员新增本部的员工
	 * @param staffInfo
	 * @param password
	 * @param session
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/addStaffByDM",method= {RequestMethod.POST})
	public String doAddStaff(TStaffBean staffInfo,String password,HttpSession session) {
		log.debug("部门管理员新增本部的员工--doAddStaff()---入参-："+staffInfo.toString()+"---password--"+password);
		JSONObject responseJson = new JSONObject();
		try {
			JSONObject userSession = LoginUtil.getUserSession(session);
			int result = staffService.addStaff(staffInfo, password, userSession);
			log.debug("doAddStaff()----部门管理员新增员工结果："+result);
			responseJson.put(Constant.MSG_FLAG, Constant.SUCCESS);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			responseJson.put(Constant.MSG_FLAG, Constant.ERROR);
			responseJson.put(Constant.ERROR_MSG, "部门管理员新增本部的员工失败："+e.getMessage());
			e.printStackTrace();
		}
		return responseJson.toJSONString();
	}

}
