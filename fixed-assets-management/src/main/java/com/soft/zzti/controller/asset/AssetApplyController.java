package com.soft.zzti.controller.asset;

import java.sql.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.soft.zzti.entity.ApplicationAssetBean;
import com.soft.zzti.entity.ResponseJsonBean;
import com.soft.zzti.entity.TApplicationBean;
import com.soft.zzti.service.AssetApplyService;
import com.soft.zzti.util.Constant;
import com.soft.zzti.util.LoginUtil;

/**
 * 设备行为（入库、领用...）的各种申请
 * @author hucl
 *
 */
@Controller
@RequestMapping("/assetApply")
public class AssetApplyController {
	private static Logger log = LoggerFactory.getLogger(AssetApplyController.class);
	@Autowired
	private AssetApplyService assetApplyService;
	/**
	 * 设备入库申请
	 * @param applicationAssetBean
	 * @return
	 */
	@RequestMapping(value="/addAssetApplication", method= {RequestMethod.POST})
	public @ResponseBody ResponseJsonBean applicationOfAsset(ApplicationAssetBean applicationAssetBean,HttpSession session) {
		//获取登陆人保存在session中的信息
		JSONObject userJson = new JSONObject();
		userJson = LoginUtil.getUserSession(session);
		ResponseJsonBean responseJsonBean = new ResponseJsonBean();
		log.debug(applicationAssetBean.toString());
		
		try {
			assetApplyService.doTheApplicationOfAsset(applicationAssetBean, userJson);
			responseJsonBean.setMsgFlag(Constant.SUCCESS);
		} catch (Exception e) {
			// TODO: handle exception
			responseJsonBean.setMsgFlag(Constant.ERROR);
			responseJsonBean.setErrorMsg("设备入库出错："+e.getMessage());
			e.printStackTrace();
		}
		return responseJsonBean;
	}
	/**
	 * 查寻普通员工自己名下所有的申请
	 * @param applicationAssetBean
	 * @param session
	 * @return
	 */
	@RequestMapping(value="/getStaffSelfAssetApplication",method= {RequestMethod.POST})
	public @ResponseBody String staffSelfAllApplication(ApplicationAssetBean applicationAssetBean,HttpSession session) {
		JSONObject json = new JSONObject();
		Map<String,Object> resultMap = new HashMap<String,Object>();
		
		//对分页的属性进行非空校验 if(空)-初始化为start 1;length 10
		//前台传递的start初始值为0，需要+1 为1
		int start = applicationAssetBean.getStart() == null?1:applicationAssetBean.getStart()+1;
		int length = applicationAssetBean.getLength() == null?10:applicationAssetBean.getLength();
		String staffID = applicationAssetBean.getApplyPerson();
		Date startTime = applicationAssetBean.getStartTime();
		Date endTime = applicationAssetBean.getEndTime();
		System.out.println(start+"--------"+length);
		//非空校验
		if(StringUtils.isEmpty(staffID)) {
			json.put("msgFlag", Constant.ERROR);
			json.put("errorMsg", "员工工号不能为空！");
			return json.toJSONString();
		}
		//分页设置
		PageInfo<ApplicationAssetBean> applicationAssets = new PageInfo<ApplicationAssetBean>();
		try {
			applicationAssets = assetApplyService.doGetAllApplicationOfStaffSelf(staffID, start, length,startTime,endTime);
			resultMap.put("tableData", applicationAssets.getList());
			json.put("resultJson", resultMap);
			json.put("recordsTotal", applicationAssets.getTotal());
			json.put("recordsFiltered", applicationAssets.getTotal());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return json.toString();
	}
	/**
	 * 查询所有
	 * 查询条件1.applyState
	 * 2.applyDate
	 * @param applicationAssetBean
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/getAllAssetApplication",method= {RequestMethod.POST})
	public String getAllApplication(ApplicationAssetBean applicationAssetBean) {
		JSONObject responseJson = new JSONObject();
		//分页设置
		PageInfo<ApplicationAssetBean> applicationAssets = new PageInfo<ApplicationAssetBean>();
		try {
			applicationAssets = assetApplyService.doGetAllApplication(applicationAssetBean);
			responseJson.put(Constant.MSG_FLAG, Constant.SUCCESS);
			responseJson.put(Constant.RESULT_JSON,applicationAssets.getList());
			responseJson.put("recordsTotal", applicationAssets.getTotal());
			responseJson.put("recordsFiltered", applicationAssets.getTotal());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			responseJson.put(Constant.MSG_FLAG, Constant.ERROR);
			responseJson.put(Constant.ERROR_MSG, e.getMessage());
			e.printStackTrace();
		}
		return responseJson.toJSONString();
	}
	/**
	 * 删除设备申请
	 * @param mainId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/deleteOneApplication",method= {RequestMethod.POST})
	public String doDeleteApplicationById(@RequestParam(value="mainID",required=true) int mainId) {
		log.debug("删除设备申请doDeleteApplicationById()---入参："+mainId);
		JSONObject responseJson = new JSONObject();
		try {
			int result = assetApplyService.doDeleteOneApplication(mainId);
			log.debug("doDeleteApplicationById()----删除结果："+result);
			responseJson.put(Constant.MSG_FLAG, Constant.SUCCESS);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			responseJson.put(Constant.MSG_FLAG, Constant.ERROR);
			responseJson.put(Constant.ERROR_MSG, "删除设备申请失败--"+e.getMessage());
			e.printStackTrace();
		}
		return responseJson.toJSONString();
	}
	/**
	 * 根据maiId获取设备申请详情
	 * @param mainId
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/getApplicationDeatil",method= {RequestMethod.GET})
	public String doGetApplyDetail(@RequestParam(value="id",required=true) int mainId,Model model) {
		JSONObject responseJson = new JSONObject();
		try {
			TApplicationBean bean = assetApplyService.getApplicationDetail(mainId);
			responseJson.put("detail", bean);
			model.addAttribute("applicationDetail",bean);
			model.addAttribute("JSONStr", responseJson.toJSONString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		return "superManager/deviceManage/DeviceApplicationDetail";
	}

}
