package com.soft.zzti.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.soft.zzti.entity.TSuperManageBean;
import com.soft.zzti.service.SuperManagerService;
import com.soft.zzti.util.Constant;
/**
 * 超级管理员
 * @author hucl
 *
 */
@Controller
@RequestMapping("/superManager")
public class SuperManagerController {
	private static Logger log = LoggerFactory.getLogger(SuperManagerController.class);
	@Autowired
	private SuperManagerService superManagerService;
	
	/**
	 * 修改信息
	 * @param superManageBean
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/updateSuperManagerInfo",method= {RequestMethod.POST})
	public String modifySuperManagerInfo(TSuperManageBean superManageBean) {
		log.debug("超级管理员修改信息Controller--modifySuperManagerInfo()---入参--"+superManageBean.toString());
		JSONObject responseJson = new JSONObject();
		
		if(StringUtils.isEmpty(superManageBean.getSuperID())) {
			responseJson.put(Constant.MSG_FLAG, Constant.ERROR);
			responseJson.put(Constant.ERROR_MSG, "error:超级管理员工号为空！！");
			return responseJson.toJSONString();
		}
		
		try {
			int result = superManagerService.doUpdateSMByID(superManageBean);
			log.debug("修改超级管理员信息--modifySuperManagerInfo()--影响结果--："+result);
			responseJson.put(Constant.MSG_FLAG, Constant.SUCCESS);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			responseJson.put(Constant.MSG_FLAG, Constant.ERROR);
			responseJson.put(Constant.ERROR_MSG, "修改超级管理员信息失败："+e.getMessage());
			e.printStackTrace();
		}
		return responseJson.toJSONString();
	}

}
