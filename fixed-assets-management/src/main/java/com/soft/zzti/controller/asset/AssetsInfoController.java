package com.soft.zzti.controller.asset;

import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.soft.zzti.entity.AssetsInfoQueryVo;
import com.soft.zzti.entity.TAssetsInfoBean;
import com.soft.zzti.service.AssetsInfoService;
import com.soft.zzti.util.Constant;

/**
 * 资产信息
 * @author hucl
 *
 */
@Controller
@RequestMapping("/assetsInfo")
public class AssetsInfoController {
	//log日志
	private static Logger log = LoggerFactory.getLogger(AssetsInfoController.class);
	@Autowired
	private AssetsInfoService assetsInfoService;
	
	/**
	 * 获取详情
	 * @param assetID
	 * @param response
	 */
	@RequestMapping(value="/getAssetInfoByID",method= {RequestMethod.POST})
	@ResponseBody
	public void doGetAssetInfo(@RequestParam(value="assetID",required=true) int assetID,HttpServletResponse response) {
		JSONObject resultJson = new JSONObject();
		PrintWriter out = null;
		TAssetsInfoBean assetsInfoBean = null;
		try {
			response.setCharacterEncoding("utf-8");
			out = response.getWriter();
			assetsInfoBean = assetsInfoService.getAssetInfoById(assetID);
			resultJson.put("resultJson", assetsInfoBean);
			resultJson.put("msgFlag", "0");
			out.print(resultJson);
			out.flush();
			out.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			resultJson.put("msgFlag", "1");
			resultJson.put("errorMsg",e.getMessage());
			out.print(resultJson);
			out.flush();
			out.close();
			e.printStackTrace();
		}
	}
	
	/**
	 * 设备查询功能
	 * 根据设备名称和设备类型进行模糊查询
	 * 两个参数可以为空
	 * @param response
	 * @param assetInfo
	 */
	@RequestMapping(value="/getAllAssetInfoByClassAndName",method= {RequestMethod.POST})
	public void doGetAllAssetInfo(TAssetsInfoBean assetInfo,HttpServletResponse response) {
		log.debug("AssetsInfoController.doGetAllAssetInfo请求参数-----"+assetInfo.toString());
		//ResponseJsonBean responseJsonBean = new ResponseJsonBean();
		JSONObject resultJson = new JSONObject();
		//Map<String,Object> resultJson = new HashMap<String,Object>();
		try {
			List<AssetsInfoQueryVo> assetInfoQueryLists = assetsInfoService.getAllAssetInfoQueryVo(assetInfo);
			resultJson.put("tableData", assetInfoQueryLists);
			response.setCharacterEncoding("utf-8");
			PrintWriter out = response.getWriter();
			out.print(resultJson);
			out.flush();
			out.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * 超级管理员端 修改设备详情
	 * @param assetID
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/queryAssetInfo",method= {RequestMethod.GET})
	public String getAssetInfoBySM(@RequestParam(value="assetID",required=true) int assetID,Model model) throws Exception{
		JSONObject responseJson = new JSONObject();
		TAssetsInfoBean assetsInfoBean = assetsInfoService.getAssetInfoById(assetID);
		responseJson.put("detail", assetsInfoBean);
		model.addAttribute("assetInfo", assetsInfoBean);
		model.addAttribute("JSONStr", responseJson.toJSONString());
		return "superManager/deviceManage/DeviceInfoDetail";
	}
	/**
	 * 设备信息修改
	 * @param assetInfo
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/updateAssetInfo",method= {RequestMethod.POST})
	public String modifyAssetInfo(TAssetsInfoBean assetInfo) {
		log.debug("设备信息修改--modifyAssetInfo()--入参："+assetInfo.toString());
		JSONObject responseJson = new JSONObject();
		
		if(StringUtils.isEmpty(assetInfo.getAssetID())) {
			responseJson.put(Constant.MSG_FLAG, Constant.ERROR);
			responseJson.put(Constant.ERROR_MSG, "error:设备id为空！！");
			return responseJson.toJSONString();
		}
		try {
			int result = assetsInfoService.modifyAssetInfoDetail(assetInfo);
			log.debug("设备信息修改--modifyDMInfo()--影响结果--："+result);
			responseJson.put(Constant.MSG_FLAG, Constant.SUCCESS);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			responseJson.put(Constant.MSG_FLAG, Constant.ERROR);
			responseJson.put(Constant.ERROR_MSG, "设备信息修改失败："+e.getMessage());
			e.printStackTrace();
		}
		return responseJson.toJSONString();
	}
	/**
	 * 设备新增
	 * @param assetsInfoBean
	 * @return
	 */
	@RequestMapping(value="/addAssetInfo",method = {RequestMethod.POST})
	public @ResponseBody String addAssetInfo(TAssetsInfoBean assetsInfoBean) {
		log.debug("设备新增--addAssetInfo()--入参："+assetsInfoBean.toString());
		JSONObject responseJson = new JSONObject();
		
		if(StringUtils.isEmpty(assetsInfoBean.getAssetID()) && 
				StringUtils.isEmpty(assetsInfoBean.getAssetName())) {
			responseJson.put(Constant.MSG_FLAG, Constant.ERROR);
			responseJson.put(Constant.ERROR_MSG, "error:设备id和设备名称不能为空！！");
			return responseJson.toJSONString();
		}
		if(StringUtils.isEmpty(assetsInfoBean.getInDate()) &&
				StringUtils.isEmpty(assetsInfoBean.getInPerson())) {
			responseJson.put(Constant.MSG_FLAG, Constant.ERROR);
			responseJson.put(Constant.ERROR_MSG, "error:设备入库人和入库日期不能为空！！");
			return responseJson.toJSONString();
		}
		if(StringUtils.isEmpty(assetsInfoBean.getInventory()) &&
				StringUtils.isEmpty(assetsInfoBean.getTotal_inventory())) {
			responseJson.put(Constant.MSG_FLAG, Constant.ERROR);
			responseJson.put(Constant.ERROR_MSG, "error:设备总数量不能为空！！");
			return responseJson.toJSONString();
		}
		if(StringUtils.isEmpty(assetsInfoBean.getClass_id()) && 
				StringUtils.isEmpty(assetsInfoBean.getManufacture())) {
			responseJson.put(Constant.MSG_FLAG, Constant.ERROR);
			responseJson.put(Constant.ERROR_MSG, "error:生产厂家不能为空！！");
			return responseJson.toJSONString();
		}
		try {
			int result = assetsInfoService.addAssetInfo(assetsInfoBean);
			log.debug("设备新增--addAssetInfo()--影响结果--："+result);
			responseJson.put(Constant.MSG_FLAG, Constant.SUCCESS);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			responseJson.put(Constant.MSG_FLAG, Constant.ERROR);
			responseJson.put(Constant.ERROR_MSG, "设备信息修改失败："+e.getMessage());
			e.printStackTrace();
		}
		return responseJson.toJSONString();
	}
	/**
	 * 查询所有 t_assets_info
	 * @return
	 */
	@RequestMapping(value="/getAllAssetsInfo",method = {RequestMethod.POST})
	public @ResponseBody String getAllAssetsInfo() {
		JSONObject responseJson = new JSONObject();
		try {
			List<TAssetsInfoBean> lists = assetsInfoService.getAllAssets();
			responseJson.put(Constant.RESULT_JSON, lists);
			responseJson.put(Constant.MSG_FLAG, Constant.SUCCESS);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			responseJson.put(Constant.MSG_FLAG, Constant.ERROR);
			responseJson.put(Constant.ERROR_MSG, "查询所有设备失败："+e.getMessage());
			e.printStackTrace();
		}
		return responseJson.toJSONString();
	}
}
