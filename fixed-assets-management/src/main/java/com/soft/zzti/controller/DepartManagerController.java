package com.soft.zzti.controller;


import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.soft.zzti.entity.TDepartManageBean;
import com.soft.zzti.entity.TStaffBean;
import com.soft.zzti.service.DepartManagerService;
import com.soft.zzti.util.Constant;

@Controller
@RequestMapping("/departManager")
public class DepartManagerController {
	@Autowired
	private DepartManagerService departManagerService;
	private static Logger log = LoggerFactory.getLogger(DepartManagerController.class);
	
	/**
	 * 修改部门管理员信息
	 * @param staffBean
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/updateDMInfo",method= {RequestMethod.POST})
	public String modifyDMInfo(TDepartManageBean departManageBean) {
		log.debug("修改部门管理员信息--modifyDMInfo()--入参："+departManageBean.toString());
		JSONObject responseJson = new JSONObject();
		
		if(StringUtils.isEmpty(departManageBean.getManagerID())) {
			responseJson.put(Constant.MSG_FLAG, Constant.ERROR);
			responseJson.put(Constant.ERROR_MSG, "error:部门管理员工号为空！！");
			return responseJson.toJSONString();
		}
		try {
			int result = departManagerService.updateDMInfo(departManageBean);
			log.debug("修改部门管理员信息--modifyDMInfo()--影响结果--："+result);
			responseJson.put(Constant.MSG_FLAG, Constant.SUCCESS);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			responseJson.put(Constant.MSG_FLAG, Constant.ERROR);
			responseJson.put(Constant.ERROR_MSG, "修改部门管理员信息失败："+e.getMessage());
			e.printStackTrace();
		}
		return responseJson.toJSONString();
	}
	/**
	 * 获取全部
	 * @param staffInfo
	 * @param baseBean
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/getAllDMInfo",method= {RequestMethod.POST})
	public String getAllDM(TDepartManageBean dmBean) {
		log.debug("获取所有的部门管理员getAllDM()---入参："+dmBean.toString());
		JSONObject responseJson = new JSONObject();
		List<TDepartManageBean> lists = new ArrayList<TDepartManageBean>();
		try {
			lists = departManagerService.getAllDMInfo(dmBean);
			responseJson.put(Constant.MSG_FLAG, Constant.SUCCESS);
			responseJson.put("resultJson", lists);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			responseJson.put(Constant.MSG_FLAG, Constant.ERROR);
			responseJson.put(Constant.ERROR_MSG, e.getMessage());
			e.printStackTrace();
		}
		return responseJson.toJSONString();
	}
	
	/**
	 * 删除
	 * @param managerID
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/doDeleteDM",method= {RequestMethod.POST})
	public String doDeleteDM(@RequestParam(value="managerID",required=true) String deaprtManagerId) {
		log.debug("删除部门管理员doDeleteDM()---入参："+deaprtManagerId);
		JSONObject responseJson = new JSONObject();
		try {
			int result = departManagerService.deleteDepartManager(deaprtManagerId);
			log.debug("doDeleteDM()----删除结果："+result);
			responseJson.put(Constant.MSG_FLAG, Constant.SUCCESS);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			responseJson.put(Constant.MSG_FLAG, Constant.ERROR);
			responseJson.put(Constant.ERROR_MSG, "删除失败："+e.getMessage());
			e.printStackTrace();
		}
		return responseJson.toJSONString();
	}
	/**
	 * 根据主键查询
	 * @param managerID
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/getDMInfoByManagerID",method= {RequestMethod.GET})
	public String doGetOne(@RequestParam(value="id",required=true) String managerID,Model model) throws Exception {
		TDepartManageBean bean = departManagerService.getOne(managerID);
		model.addAttribute("dmBean",bean);
		return "superManager/userManage/DepartManagerInfoDetail";
	}
	
	/**
	 * 根据部门编码查询本部门下的所有普通员工
	 * @param departId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/doQueryThisDepartStaff",method= {RequestMethod.POST})
	public String doQueryThisDMStaff(@RequestParam(value="departId",required=true) int departId,String staffName) {
		log.debug("根据部门编码查询本部门下的所有普通员工--doQueryThisDMStaff()---入参："+departId+staffName);
		JSONObject responseJson = new JSONObject();
		try {
			List<TStaffBean> lists = departManagerService.getStaffByDepartId(departId,staffName);
			responseJson.put(Constant.MSG_FLAG, Constant.SUCCESS);
			responseJson.put(Constant.RESULT_JSON, lists);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			responseJson.put(Constant.MSG_FLAG, Constant.ERROR);
			responseJson.put(Constant.ERROR_MSG, "查询本部门下的所有普通员失败："+e.getMessage());
			e.printStackTrace();
		}
		return responseJson.toJSONString();
	}

}
