package com.soft.zzti.controller.asset;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.soft.zzti.entity.TApplicationBean;
import com.soft.zzti.service.AssetApplyService;
import com.soft.zzti.util.Constant;

@Controller
@RequestMapping("/application")
public class ApplicationController {
	private static Logger log = LoggerFactory.getLogger(ApplicationController.class);
	@Autowired
	private AssetApplyService assetApplyService;
	
	/**
	 * 修改设备申请信息
	 * @param applicationInfo
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/updateApplicationInfo",method= {RequestMethod.POST})
	public String modifyApplicationInfo(TApplicationBean applicationInfo) {
		log.debug("修改设备申请信息--modifyApplicationInfo()--入参："+applicationInfo.toString());
		JSONObject responseJson = new JSONObject();
		
		if(StringUtils.isEmpty(applicationInfo.getMainID())) {
			responseJson.put(Constant.MSG_FLAG, Constant.ERROR);
			responseJson.put(Constant.ERROR_MSG, "error:mainID不能为空！");
			return responseJson.toJSONString();
		}
		try {
			int result = assetApplyService.modifyApplicationInfo(applicationInfo);
			log.debug("修改设备申请信息--modifyApplicationInfo()--影响结果--："+result);
			responseJson.put(Constant.MSG_FLAG, Constant.SUCCESS);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			responseJson.put(Constant.MSG_FLAG, Constant.ERROR);
			responseJson.put(Constant.ERROR_MSG, "修改设备申请信息失败："+e.getMessage());
			e.printStackTrace();
		}
		return responseJson.toJSONString();
	}

}
