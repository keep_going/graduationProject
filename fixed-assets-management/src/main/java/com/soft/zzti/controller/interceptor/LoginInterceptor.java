package com.soft.zzti.controller.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
/**
 * 登录拦截器
 * @author hucl
 *
 */
public class LoginInterceptor implements HandlerInterceptor{

	// 进入 Handler方法之前执行
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object obj) throws Exception {
		// TODO Auto-generated method stub
		String url = request.getRequestURI();
		System.out.println(url);
		// 这里公开地址是登陆提交的地址
		if (url.indexOf("loginCheck.do") >= 0) {
			// 如果进行登录提交，放行
			return true;
		}
		JSONObject sessionJson = (JSONObject) request.getSession().getAttribute("userSession");
		if(sessionJson!=null) {
			return true;
		}
		//执行这里表示用户身份需要认证，跳转登陆页面
		request.getRequestDispatcher("/jsp/login/Login.jsp").forward(request, response);
		return false;
	}
	
	// 进入Handler方法之后，返回modelAndView之前执行
	// 应用场景从modelAndView出发：将公用的模型数据(比如菜单导航)在这里传到视图，也可以在这里统一指定视图
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object obj, ModelAndView model)
			throws Exception {
		// TODO Auto-generated method stub
		System.out.println("LoginInterceptor---postHandle");
	}
	
	// 执行Handler完成执行此方法
	// 应用场景：统一异常处理，统一日志处理
	@Override
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		// TODO Auto-generated method stub
		System.out.println("LoginInterceptor---afterCompletion");
	}

}
