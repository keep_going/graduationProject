package com.soft.zzti.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.soft.zzti.service.RightService;
import com.soft.zzti.service.StaffService;
import com.soft.zzti.util.Constant;
import com.soft.zzti.util.RightUtil;

@Controller
@RequestMapping("/userRight")
public class RightController {
	private static Logger log = LoggerFactory.getLogger(RightController.class);
	@Autowired
    private StaffService staffService;
	@Autowired
	private RightService rightService;
	/**
	 * 获取用户的权限
	 * @param userId
	 * @return
	 */
	@RequestMapping(value="/getUserRight",method= {RequestMethod.GET})
	public String doGetUserRight(@RequestParam(value="userId",required=true) String userId,Model model) {
		JSONObject responseJson = new JSONObject();
		Map<String,Object> resultjson = new HashMap<String,Object>();
		try {
			List<Integer> rightLists = staffService.findStaffRightByStaffID(userId);
			resultjson.put("userId", userId);
			resultjson.put("userRight", rightLists);
			responseJson.put(Constant.RESULT_JSON, resultjson);
			model.addAttribute("userRightInfoStr", responseJson.toJSONString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "superManager/rightManage/UserRightDetail";
	}
	/**
	 * 修改用户权限
	 * @param rights
	 * @param userId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/modifyUserRight",method= {RequestMethod.POST})
	public String doModtifyUserRight(@RequestParam(value="rightsNew") String[] rightsNew, String[] rightsOld,String userId) {
		JSONObject responseJson = new JSONObject();
		//根原权限相比，少的权限 删除
		List<String> reduceList = RightUtil.rightReduce(rightsOld, rightsNew);
		System.out.println(reduceList.toString());
		//根原权限相比，多出的权限 新增
		List<String> increaseList = RightUtil.rightIncrease(rightsOld, rightsNew);
		System.out.println(increaseList.toString());
		
		if(reduceList.size()>0 || increaseList.size()>0) {
			try {
				int result = rightService.updateUserRightByUserId(increaseList, reduceList, userId);
				log.debug("修改用户权限-modifyUserRight()---结果--"+result);
				responseJson.put(Constant.MSG_FLAG, Constant.SUCCESS);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				responseJson.put(Constant.MSG_FLAG, Constant.ERROR);
				responseJson.put(Constant.ERROR_MSG, "修改权限失败"+e.getMessage());
				e.printStackTrace();
			}
		}
		
		return responseJson.toJSONString();
	}
}
