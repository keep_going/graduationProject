package com.soft.zzti.controller.asset;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.soft.zzti.entity.ResponseJsonBean;
import com.soft.zzti.entity.TAssetClassBean;
import com.soft.zzti.service.AssetClassService;
@Controller
@RequestMapping("/assetClass")
public class AssetClassController {
	//log日志
	private static Logger log = LoggerFactory.getLogger(AssetClassController.class);
	@Autowired
	private AssetClassService assetClassService;
	
	/**
	 * 获取所有的设备类型t_assets_class
	 * @return
	 */
	@RequestMapping("/getAssetClassList")
	public @ResponseBody ResponseJsonBean doGetAssetClass() {
		ResponseJsonBean responseJsonBean = new ResponseJsonBean();
		Map<String,Object> resultJson = new HashMap<String,Object>();
		List<TAssetClassBean> assetClassBean = null;
		try {
			assetClassBean = assetClassService.getAssetClassList();
			resultJson.put("assetClass", assetClassBean);
			responseJsonBean.setMsgFlag("0");
			responseJsonBean.setResultJson(resultJson);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			responseJsonBean.setMsgFlag("1");
			responseJsonBean.setErrorMsg("查询设备类型出错："+e.getMessage());
			e.printStackTrace();
		}
		return responseJsonBean;
	}

}
