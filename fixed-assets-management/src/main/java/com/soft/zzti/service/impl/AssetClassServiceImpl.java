package com.soft.zzti.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.soft.zzti.dao.AssetClassDao;
import com.soft.zzti.entity.TAssetClassBean;
import com.soft.zzti.service.AssetClassService;
@Service("assetClassService")
public class AssetClassServiceImpl implements AssetClassService{
	@Resource
	private AssetClassDao assetClassDao;

	@Override
	public List<TAssetClassBean> getAssetClassList() throws Exception{
		// TODO Auto-generated method stub
		List<TAssetClassBean> assetClassBeans = assetClassDao.selectAssetClassList();
		return assetClassBeans;
	}

}
