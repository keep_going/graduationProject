package com.soft.zzti.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.soft.zzti.dao.DepartMangerDao;
import com.soft.zzti.entity.TDepartManageBean;
import com.soft.zzti.entity.TStaffBean;
import com.soft.zzti.service.DepartManagerService;

@Service("departManagerService")
public class DepartManagerServiceImpl implements DepartManagerService{
	@Resource
	private DepartMangerDao dmDao;
	
	@Override
	public int updateDMInfo(TDepartManageBean dmBean) throws Exception {
		// TODO Auto-generated method stub
		return dmDao.updateByPrimaryKeySelective(dmBean);
	}

	@Override
	public List<TDepartManageBean> getAllDMInfo(TDepartManageBean dmBean) throws Exception {
		// TODO Auto-generated method stub
		return dmDao.selectAll(dmBean);
	}

	@Override
	public int deleteDepartManager(String deaprtManagerId) throws Exception {
		// TODO Auto-generated method stub
		return dmDao.deleteByPrimaryKey(deaprtManagerId);
	}

	@Override
	public TDepartManageBean getOne(String managerId) throws Exception {
		// TODO Auto-generated method stub
		return dmDao.selectByPrimaryKey(managerId);
	}

	@Override
	public List<TStaffBean> getStaffByDepartId(int departId,String staffName) throws Exception {
		// TODO Auto-generated method stub
		return dmDao.selectStaffByDepartId(departId,staffName);
	}

}
