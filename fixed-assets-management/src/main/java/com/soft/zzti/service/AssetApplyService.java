package com.soft.zzti.service;


import java.sql.Date;
import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.soft.zzti.entity.ApplicationAssetBean;
import com.soft.zzti.entity.DataChartBean;
import com.soft.zzti.entity.TApplicationBean;

public interface AssetApplyService {
	/**
	 * 处理设备入库的申请
	 * @param applicationAssetBean
	 * @throws Exception
	 */
	void doTheApplicationOfAsset(ApplicationAssetBean applicationAssetBean,JSONObject userInfo) throws Exception;
	/**
	 * 根据员工工号查询自己名下所有的设备申请信息
	 * @param staffID
	 * @param pageNo
	 * @param pageSize
	 * @param startTime
	 * @param endTime
	 * @return
	 * @throws Exception
	 */
	PageInfo<ApplicationAssetBean> doGetAllApplicationOfStaffSelf(String staffID,Integer pageNo,Integer pageSize,Date startTime,Date endTime) throws Exception;
	
	/**
	 * 获取所有的设备申请信息
	 * @param applicationAssetBean
	 * @return
	 * @throws Exception
	 */
	PageInfo<ApplicationAssetBean> doGetAllApplication(ApplicationAssetBean applicationAssetBean) throws Exception;
	/**
	 * 删除申请
	 * @param mainId
	 * @return
	 * @throws Exception
	 */
	int doDeleteOneApplication(int mainId) throws Exception;
	
	/**
	 * 查询详情
	 * @param mainId
	 * @return
	 * @throws Exception
	 */
	TApplicationBean getApplicationDetail(int mainId) throws Exception;
	/**
	 * 修改信息
	 * @param record
	 * @return
	 * @throws Exception
	 */
	int modifyApplicationInfo(TApplicationBean record) throws Exception;
	
	/**
	 * 获取t_application中所有的asset_id
	 * @return
	 * @throws Exception
	 */
	List<Integer> getApplyAllAssetId() throws Exception;
	/**
	 * 根据assetId查询它下面的apply_state
	 * @param assetId
	 * @return
	 * @throws Exception
	 */
	List<DataChartBean> getApplyStateByAssetId(int assetId) throws Exception;
	/**
	 * 根据assetId查询设备使用率
	 * @return
	 * @throws Exception
	 */
	List<DataChartBean> getAssetUserRate(int assetId)throws Exception;
}
