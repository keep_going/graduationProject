package com.soft.zzti.service;


import java.util.List;

import com.soft.zzti.entity.TDepartManageBean;
import com.soft.zzti.entity.TStaffBean;


public interface DepartManagerService {
	/**
	 * 修改部门管理员信息
	 * @param dmBean
	 */
	int updateDMInfo(TDepartManageBean dmBean) throws Exception;
	/**
	 * 查询所有部门管理员
	 * @param dmBean
	 * @param pageNo
	 * @param pageSize
	 * @return
	 * @throws Exception
	 */
	List<TDepartManageBean> getAllDMInfo(TDepartManageBean dmBean) throws Exception;
	/**
	 * 删除
	 * @param deaprtManagerId
	 * @return
	 * @throws Exception
	 */
	int deleteDepartManager(String deaprtManagerId) throws Exception;
	
	/**
	 * 根据主键查询一个
	 * @param managerId
	 * @return
	 * @throws Exception
	 */
	TDepartManageBean getOne(String managerId) throws Exception;
	/**
	 * 查询部门下的所有普通员工
	 * @param departId
	 * @return
	 * @throws Exception
	 */
	List<TStaffBean> getStaffByDepartId(int departId,String staffName) throws Exception;
}
