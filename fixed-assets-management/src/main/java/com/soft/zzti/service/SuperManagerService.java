package com.soft.zzti.service;

import com.soft.zzti.entity.TSuperManageBean;

public interface SuperManagerService {
	/**
	 * 修改
	 * @return
	 * @throws Exception
	 */
	int doUpdateSMByID(TSuperManageBean superManageBean) throws Exception;

}
