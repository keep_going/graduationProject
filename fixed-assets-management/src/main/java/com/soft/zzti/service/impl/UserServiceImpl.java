package com.soft.zzti.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.soft.zzti.controller.exception.CustomException;
import com.soft.zzti.dao.UserDao;
import com.soft.zzti.entity.TDepartManageBean;
import com.soft.zzti.entity.TPassword;
import com.soft.zzti.entity.TStaffBean;
import com.soft.zzti.entity.TSuperManageBean;
import com.soft.zzti.entity.UserBean;
import com.soft.zzti.service.UserService;
import com.soft.zzti.util.MD5Util;

/**
 * Created by hucl on 2018/2/6.
 */
@Service("userService")
public class UserServiceImpl implements UserService{
	@Resource
	private UserDao userDao;
	private static Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

    public boolean test() {
        return false;
    }

	public boolean doLoginCheck(TPassword tPassword) throws Exception {
		// TODO Auto-generated method stub
		TPassword pswBean = userDao.userLoginCheck(tPassword);
		boolean boo = true;
		//根据帐号和role_i判断该帐号在表中是否存在
		if(pswBean != null) {
			/*
			 * 帐号存在得到该帐号对应的密码（都是md5加密后的）
			 * 在与用户属于的密码相比较，
			 * 匹配不成功，密码不正确
			 */
			//从数据库中读出的密码
			String outPsw = pswBean.getUser_password();
			//用户输入的密码 先进行md5加密处理 然后对比
			String inPsw = tPassword.getUser_password();
			inPsw = MD5Util.MD5(inPsw);
			if(!outPsw.equals(inPsw)) {
				boo = false;
				throw new CustomException("密码输入错误，请重新输入！");
			}
		}else {
			boo = false;
			throw new CustomException("该帐号不存在，请重新输入！");
		}
		return boo;
	}

	@Override
	public void doFindPassowrd(TPassword tPassword) throws Exception {
		// TODO Auto-generated method stub
		String password = tPassword.getUser_password();
		log.debug("md5加密前-"+password);
		//Md5加密处理
		password = MD5Util.MD5(password);
		log.debug("md5加密后-"+password);
		tPassword.setUser_password(password);
		
		userDao.updatePassword(tPassword);
		
	}

	@Override
	public UserBean doFindUserInfo(String username, int roleID) throws Exception {
		// TODO Auto-generated method stub
		UserBean userBean = new UserBean();
		if(3 == roleID) {
			TStaffBean staffBean = userDao.selectStaffInfo(username);
			userBean.setStaff(staffBean);
		}else if(2 == roleID) {
			TDepartManageBean dManager = userDao.selectDepartmentManagerInfo(username);
			userBean.setDepartManage(dManager);
		}else if(1 == roleID) {
			TSuperManageBean sManager = userDao.selectSuperManagerInfo(username);
			userBean.setSuperManage(sManager);
		}
		return userBean;
	}

	@Override
	public List<TPassword> doGetAllUser(int roleId) throws Exception {
		// TODO Auto-generated method stub
		return userDao.selectAll(roleId);
	}

	@Override
	public boolean checkOldPassword(String userId, String oldPassword,int role_id) throws Exception {
		// TODO Auto-generated method stub
		boolean boo = true;
		TPassword tPassword = new TPassword();
		tPassword.setRole_id(role_id);
		tPassword.setUser_id(userId);
		TPassword pswBean = userDao.userLoginCheck(tPassword);
		if(pswBean != null) {
			/*
			 * 帐号存在得到该帐号对应的密码（都是md5加密后的）
			 * 在与用户属于的密码相比较，
			 * 匹配不成功，密码不正确
			 */
			//从数据库中读出的密码
			String outPsw = pswBean.getUser_password();
			//用户输入的密码 先进行md5加密处理 然后对比
			oldPassword = MD5Util.MD5(oldPassword);
			if(!outPsw.equals(oldPassword)) {
				boo = false;
			}
		}else {
			boo = false;
		}
		return boo;
	}
}
