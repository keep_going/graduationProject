package com.soft.zzti.service;

import java.util.List;

import com.soft.zzti.entity.AssetsInfoQueryVo;
import com.soft.zzti.entity.TAssetsInfoBean;

/**
 * 资产信息
 * @author hucl
 *
 */
public interface AssetsInfoService {
	/**
	 * 根据主键查询设备
	 * @param id
	 * @return
	 * @throws Exception
	 */
	TAssetsInfoBean getAssetInfoById(int id) throws Exception;
	/**
	 * 根据设备类别和设备名称进行模糊查询
	 * @param assetInfo
	 * @return
	 * @throws Exception
	 */
	List<AssetsInfoQueryVo> getAllAssetInfoQueryVo(TAssetsInfoBean assetInfo) throws Exception;
	/**
	 * 修改
	 * @param assetInfo
	 * @return
	 * @throws Exception
	 */
	int modifyAssetInfoDetail(TAssetsInfoBean assetInfo) throws Exception;
	/**
	 * 插入
	 * @param assetInfo
	 * @return
	 * @throws Exception
	 */
	int addAssetInfo(TAssetsInfoBean assetInfo) throws Exception;
	/**
	 * 获取所有
	 * @return
	 * @throws Exception
	 */
	List<TAssetsInfoBean> getAllAssets() throws Exception;

}
