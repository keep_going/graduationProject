package com.soft.zzti.service.impl;

import java.sql.Date;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.soft.zzti.dao.AssetApplyDao;
import com.soft.zzti.entity.ApplicationAssetBean;
import com.soft.zzti.entity.DataChartBean;
import com.soft.zzti.entity.TApplicationBean;
import com.soft.zzti.service.AssetApplyService;
import com.soft.zzti.util.Constant;
import com.soft.zzti.util.UserUtil;

@Service("assetApplyService")
public class AssetApplyServiceImpl implements AssetApplyService{
	private static Logger log = LoggerFactory.getLogger(AssetApplyService.class);
	@Resource
	private AssetApplyDao assetApplyDao;

	@Override
	public void doTheApplicationOfAsset(ApplicationAssetBean applicationAssetBean,JSONObject userInfo) throws Exception {
		/*
		 * devcieInType
		 * 0 添加数量
		 * 1 新增加设备
		 * 对t_assets_info表操作
		 */
		int devcieInType = applicationAssetBean.getAssetInType();
		if(0 == devcieInType) {
			
		}else if(1 == devcieInType) {
			
		}
		//设备申请标识
		int applyState = applicationAssetBean.getApplyState();
		if(0 == applyState) {
			log.debug("applyState=0:----领用申请");
		}else if(1 == applyState) {
			log.debug("applyState=1:----入库申请（新增）");
		}else if(2 == applyState) {
			log.debug("applyState=2:----流动申请");
		}else if(3 == applyState) {
			log.debug("applyState=3:----维修申请");
		}else if(4 == applyState) {
			log.debug("applyState=4:----报废申请");
		}
		
		TApplicationBean applicationBean = new TApplicationBean();
		applicationBean.setAssetID(applicationAssetBean.getAssetID());
		applicationBean.setAssetName(applicationAssetBean.getAssetName());
		applicationBean.setApplyNumber(applicationAssetBean.getApplyNumber());
		applicationBean.setApplyPerson(applicationAssetBean.getApplyPerson());
		applicationBean.setApplyReason(applicationAssetBean.getApplyReason());
		applicationBean.setApplyID(UserUtil.dateFormat());
		applicationBean.setApplyState(applyState);
		applicationBean.setAssetInType(applicationAssetBean.getAssetInType());
		applicationBean.setApprovePesron1(userInfo.getString("higherupID"));
		applicationBean.setApproveState(Constant.APPROVE_STATE_WAIT);
		
		int mainId = assetApplyDao.insertAddDeviceApplication(applicationBean);
		System.out.println(mainId);
		
	}

	@Override
	public PageInfo<ApplicationAssetBean> doGetAllApplicationOfStaffSelf(String staffID,Integer pageNo,Integer pageSize,Date startTime,Date endTime) throws Exception {
		// TODO Auto-generated method stub
		ApplicationAssetBean queryCondition = new ApplicationAssetBean();
		queryCondition.setApplyPerson(staffID);
		queryCondition.setStart(pageNo);
		queryCondition.setLength(pageSize);
		queryCondition.setStartTime(startTime);
		queryCondition.setEndTime(endTime);
		
		PageHelper.startPage(pageNo, pageSize);
		List<ApplicationAssetBean> list = assetApplyDao.selectStaffSelfApplication(queryCondition);
		//用PageInfo对结果进行包装
		PageInfo<ApplicationAssetBean> page = new PageInfo<ApplicationAssetBean>(list);
		System.out.println(page.getList());
		return page;
	}

	@Override
	public PageInfo<ApplicationAssetBean> doGetAllApplication(ApplicationAssetBean applicationAssetBean)
			throws Exception {
		// TODO Auto-generated method stub
		int pageNo = applicationAssetBean.getStart() == null?1:applicationAssetBean.getStart()+1;
		int pageSize = applicationAssetBean.getLength() == null?10:applicationAssetBean.getLength();
		
		ApplicationAssetBean queryCondition = new ApplicationAssetBean();
		queryCondition.setApplyState(applicationAssetBean.getApplyState());
		queryCondition.setStartTime(applicationAssetBean.getStartTime());
		queryCondition.setEndTime(applicationAssetBean.getEndTime());
		
		PageHelper.startPage(pageNo, pageSize);
		List<ApplicationAssetBean> list = assetApplyDao.selectAll(queryCondition);
		PageInfo<ApplicationAssetBean> page = new PageInfo<ApplicationAssetBean>(list);
		return page;
	}

	@Override
	public int doDeleteOneApplication(int mainId) throws Exception {
		// TODO Auto-generated method stub
		return assetApplyDao.deleteByPrimaryKey(mainId);
	}

	@Override
	public TApplicationBean getApplicationDetail(int mainId) throws Exception {
		// TODO Auto-generated method stub
		return assetApplyDao.selectApplicationByPrimaryKey(mainId);
	}

	@Override
	public int modifyApplicationInfo(TApplicationBean record) throws Exception {
		// TODO Auto-generated method stub
		return assetApplyDao.updateByPrimaryKeySelective(record);
	}

	@Override
	public List<Integer> getApplyAllAssetId() throws Exception {
		// TODO Auto-generated method stub
		return assetApplyDao.selectAssetId();
	}

	@Override
	public List<DataChartBean> getApplyStateByAssetId(int assetId) throws Exception {
		// TODO Auto-generated method stub
		return assetApplyDao.selectApplyStateByAssetId(assetId);
	}

	@Override
	public List<DataChartBean> getAssetUserRate(int assetId) throws Exception {
		// TODO Auto-generated method stub
		List<DataChartBean> lists = assetApplyDao.selectAssetRate(assetId);
		return lists;
	}


	

}
