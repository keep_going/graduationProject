package com.soft.zzti.service;

import java.util.List;

import com.soft.zzti.entity.TPassword;
import com.soft.zzti.entity.UserBean;

/**
 * Created by hucl on 2018/2/6.
 */
public interface UserService {
    boolean test();
    
    /**
	 * 所有使用人员的登录检测
	 * @param tPassword
	 * @return
	 */
	boolean doLoginCheck(TPassword tPassword) throws Exception;
	/**
	 * 找回密码
	 * @param tPassword
	 * @throws Exception
	 */
	void doFindPassowrd(TPassword tPassword) throws Exception;
	/**
	 * 获取登录人的详细信息
	 * 登录人包括 工作人员、部门管理员和超级管理员
	 * @param username
	 * @param roleID
	 * @return
	 * @throws Exception
	 */
	UserBean doFindUserInfo(String username,int roleID) throws Exception;
	
	/**
	 * 查询所有用户
	 * @return
	 * @throws Exception
	 */
	List<TPassword> doGetAllUser(int roleId) throws Exception;
	/**
	 * 密码校验
	 * @param userId
	 * @param oldPassword
	 * @return
	 * @throws Exception
	 */
	boolean checkOldPassword(String userId,String oldPassword,int role_id) throws Exception;
	
}
