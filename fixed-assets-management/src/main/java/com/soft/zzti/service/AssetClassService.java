package com.soft.zzti.service;

import java.util.List;

import com.soft.zzti.entity.TAssetClassBean;

/**
 * 
 * @author hucl
 *
 */
public interface AssetClassService {
	/**
	 * 获取所有的设备类型
	 * @return
	 * @throws Exception
	 */
	List<TAssetClassBean> getAssetClassList() throws Exception;

}
