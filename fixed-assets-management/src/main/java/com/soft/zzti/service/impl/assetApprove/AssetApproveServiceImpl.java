package com.soft.zzti.service.impl.assetApprove;

import java.sql.Date;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.soft.zzti.dao.AssetApplyDao;
import com.soft.zzti.dao.AssetApproveDao;
import com.soft.zzti.dao.AssetsInfoDao;
import com.soft.zzti.entity.ApplicationAssetBean;
import com.soft.zzti.entity.AssetApproveQueryVo;
import com.soft.zzti.entity.TApplicationBean;
import com.soft.zzti.entity.TAssetsInfoBean;
import com.soft.zzti.entity.TSuperManageBean;
import com.soft.zzti.service.assetApprove.AssetApproveService;
import com.soft.zzti.util.Constant;
import com.soft.zzti.util.UserUtil;

@Service("assetApproveService")
public class AssetApproveServiceImpl implements AssetApproveService{
	private static Logger log = LoggerFactory.getLogger(AssetApproveService.class); 
	@Resource
	private AssetApproveDao assetApproveDao;
	@Resource
	private AssetApplyDao assetApplyDao;
	@Resource
	private AssetsInfoDao assetsInfoDao;

	@Override
	public PageInfo<ApplicationAssetBean> getApproveBySuperManager(String superManagerID,Integer pageNo,Integer pageSize,Date startTime, Date endTime)
			throws Exception {
		// TODO Auto-generated method stub
		AssetApproveQueryVo approveQueryVo = new AssetApproveQueryVo();
		TSuperManageBean superManager = new TSuperManageBean();
		superManager.setSuperID(superManagerID);
		approveQueryVo.setStartTime(startTime);
		approveQueryVo.setEndTime(endTime);
		approveQueryVo.setSuperManager(superManager);
		
		PageHelper.startPage(pageNo, pageSize);
		List<ApplicationAssetBean> applicationAssetList = assetApproveDao.selectAssetApproveListBySuper(approveQueryVo);
		//用PageInfo对结果进行包装
		PageInfo<ApplicationAssetBean> page = new PageInfo<ApplicationAssetBean>(applicationAssetList);
		return page;
	}

	@Override
	@Transactional
	public Boolean doHandingAssetApproveBySuperManager(ApplicationAssetBean applicationInfo,int approveState) throws Exception {
		// TODO Auto-generated method stub
		
		boolean boo = false;
		
		int mainID = applicationInfo.getMainID();
		
		ApplicationAssetBean queryBean = new ApplicationAssetBean();
		queryBean.setMainID(mainID);
		queryBean.setApproveState(approveState);
		queryBean.setApproveDate(UserUtil.sqlDateFormat());
		queryBean.setApproveRemork(applicationInfo.getApproveRemork());
		log.debug("设备申请的审核doHandingAssetApproveBySuperManager---dao参数"+queryBean.toString());
		
		//审核 审核通过或不通过
		int result = assetApproveDao.updateAssetApproveState(queryBean);
		System.out.println("超级管理员审批申请 updateAssetApproveState影响的行数---"+result);
		/*
		 * 根据mainID，查询该条记录的审核状态
		 * 若审核成功，根据TApplicationBean 的approveState,applyState以及assetInType进行不同的流程
		 */
		TApplicationBean application = assetApplyDao.selectApplicationByPrimaryKey(mainID);
		int approve_state_query = application.getApproveState();
		int apply_state_query = application.getApplyState();
		//审核通过
		if(Constant.APPROVE_SATATE_FINAL_SUCCESS == approve_state_query) {
			//第一 入库 并且 asset_in_type=0,为设备添加数量 根据设备id，更新数量 
			//asset_in_type=1时，是新增设备类型
			int asset_in_type_query = application.getAssetInType();
			if(Constant.APPLY_STATE_RUKU == apply_state_query) {
				if(0 == asset_in_type_query) {
					int assetID	= application.getAssetID();
					int number = application.getApplyNumber();
					TAssetsInfoBean assetsInfo = new TAssetsInfoBean();
					assetsInfo.setAssetID(assetID);
					assetsInfo.setInventory(number);
					assetsInfoDao.updateAssetInfoByParimayKey(assetsInfo);
					boo = true;
				}else if(1 == asset_in_type_query) {
					//doSomething
//					TAssetsInfoBean assetInfo = new TAssetsInfoBean();
//					assetInfo.setAssetID(application.getAssetID());
//					assetInfo.setAssetName(application.getAssetName());
//					assetInfo.setClass_id(application.get);
					System.out.println("doSomething");
				}
			}
			if(Constant.APPLY_STATE_BAOFEI == approve_state_query) {
				//doSomething
				boo = true;
			}
			if(Constant.APPLY_STATE_LINGYONG == approve_state_query) {
				//doSomething
				boo = true;
			}
			if(Constant.APPLY_STATE_WEIXIU == approve_state_query) {
				//doSomething
				boo = true;
			}
			if(Constant.APPLY_STATE_LIUDONG == approve_state_query) {
				//doSomething
				boo = true;
			}
		}
		
		return boo;
	}

}
