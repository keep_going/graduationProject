package com.soft.zzti.service.assetApprove;


import java.sql.Date;


import com.github.pagehelper.PageInfo;
import com.soft.zzti.entity.ApplicationAssetBean;

/**
 * 设备审批的相关功能
 * @author hucl
 *
 */
public interface AssetApproveService {
	/**
	 * 查询超级管理员应该审核设备申请列表
	 * @param superManagerID
	 * @param startTime
	 * @param endTime
	 * @param pageNo
	 * @param pageSize
	 * @return
	 * @throws Exception
	 */
	PageInfo<ApplicationAssetBean> getApproveBySuperManager(String superManagerID,Integer pageNo,
			Integer pageSize,Date startTime,Date endTime) throws Exception;

	/**
	 * 超级管理员审核
	 * 添加SpringMVC的注解事务操作 @Transactional
	 * 	1.首先要先审核，审核通过后后续步骤；不通过就不说了。
	 * 	2.入库申请，如果asset_in_type为0（添加已有设备的数量），取出applicationInfo中的申请数量，更新t_assets_info表中
	 * 	对应的设备数量。如果asset_in_type为1，则需要得到applicationInfo中的相关信息，插入到t_assets_info表中。
	 * @param applicationInfo
	 * @param approveState
	 * @return
	 * @throws Exception
	 */
	Boolean doHandingAssetApproveBySuperManager(ApplicationAssetBean applicationInfo,int approveState) throws Exception;
}
