package com.soft.zzti.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.soft.zzti.dao.RightDao;
import com.soft.zzti.service.RightService;
@Service("rightService")
public class RightServiceImpl implements RightService{
	@Resource
	private RightDao rightDao;

	@Override
	@Transactional
	public int updateUserRightByUserId(List<String> increase,List<String> reduce, String userId) throws Exception {
		// TODO Auto-generated method stub
		int reduceResult = 0;
		int increaseResult = 0;
		if(reduce!=null && reduce.size()>0) {
			reduceResult = rightDao.deleteRight(userId, reduce);
		}
		if(increase!=null && increase.size()>0) {
			increaseResult = rightDao.insertRight(userId, increase);
		}
		return reduceResult+increaseResult;
	}

}
