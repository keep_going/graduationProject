package com.soft.zzti.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.soft.zzti.dao.AssetsInfoDao;
import com.soft.zzti.entity.AssetsInfoQueryVo;
import com.soft.zzti.entity.TAssetsInfoBean;
import com.soft.zzti.service.AssetsInfoService;

@Service("assetsInfoService")
public class AssetsInfoServiceImpl implements AssetsInfoService{
	
	private static Logger log = LoggerFactory.getLogger(AssetsInfoService.class);
	
	@Resource
	private AssetsInfoDao assetsInfoDao;
	
	public TAssetsInfoBean getAssetInfoById(int id) throws Exception {
		// TODO Auto-generated method stub
		TAssetsInfoBean assetsInfoBean = assetsInfoDao.selectAssetInfoByID(id);
		if(assetsInfoBean == null) {
			assetsInfoBean = new TAssetsInfoBean();
		}
		return assetsInfoBean;
	}

	@Override
	public List<AssetsInfoQueryVo> getAllAssetInfoQueryVo(TAssetsInfoBean assetInfo) throws Exception {
		// TODO Auto-generated method stub
		log.debug("AssetsInfoServiceImpl.getAllAssetInfo参数-----"+assetInfo.toString());
		
		List<AssetsInfoQueryVo> assetInfoVoLists = assetsInfoDao.selectAssetInfoAddedAppyNumberList(assetInfo);
		return assetInfoVoLists;
	}

	@Override
	public int modifyAssetInfoDetail(TAssetsInfoBean assetInfo) throws Exception {
		// TODO Auto-generated method stub
		return assetsInfoDao.updateByPrimaryKeySelective(assetInfo);
	}

	@Override
	public int addAssetInfo(TAssetsInfoBean assetInfo) throws Exception {
		// TODO Auto-generated method stub
		return assetsInfoDao.insertSelective(assetInfo);
	}

	@Override
	public List<TAssetsInfoBean> getAllAssets() throws Exception {
		// TODO Auto-generated method stub
		return assetsInfoDao.selectAll();
	}

}
