package com.soft.zzti.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.soft.zzti.dao.SuperManagerDao;
import com.soft.zzti.entity.TSuperManageBean;
import com.soft.zzti.service.SuperManagerService;

@Service("superManagerService")
public class SuperManagerServiceImpl implements SuperManagerService{
	@Resource
	private SuperManagerDao superManagerDao;

	@Override
	public int doUpdateSMByID(TSuperManageBean superManageBean) throws Exception {
		// TODO Auto-generated method stub
		return superManagerDao.updateByPrimaryKeySelective(superManageBean);
	}

}
