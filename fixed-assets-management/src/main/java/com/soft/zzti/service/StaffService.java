package com.soft.zzti.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.soft.zzti.entity.TStaffBean;


public interface StaffService {
	/**
	 * 查询普通员工的权限
	 * @param staffID
	 * @return
	 */
	List<Integer> findStaffRightByStaffID(String staffID) throws Exception;
	/**
	 * 修改工作人员详情
	 * @param staffInfo
	 */
	int updateStaffInfo(TStaffBean staffInfo) throws Exception;
	/**
	 * 查询所有普通员工
	 * @param staff
	 * @param pageNo
	 * @param pageSize
	 * @return
	 * @throws Exception
	 */
	PageInfo<TStaffBean> getAllStaffInfo(TStaffBean staff,Integer pageNo,Integer pageSize) throws Exception;
	/**
	 * 删除
	 * @param staffId
	 * @return
	 * @throws Exception
	 */
	int deleteStaff(String staffId) throws Exception;
	/**
	 * 根据主键获取普通员工信息
	 * @param staffId
	 * @return
	 * @throws Exception
	 */
	TStaffBean getStaffByStaffId(String staffId) throws Exception;
	/**
	 * 新增普通员工
	 * @param staffInfo
	 * @param password
	 * @return
	 * @throws Exception
	 */
	int addStaff(TStaffBean staffInfo,String password,JSONObject userSession) throws Exception;

}
