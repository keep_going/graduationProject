package com.soft.zzti.service;

import java.util.List;

public interface RightService {
	/**
	 * 修改用户权限
	 * @param increase
	 * @param reduce
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	int updateUserRightByUserId(List<String> increase,List<String> reduce,String userId) throws Exception;

}
