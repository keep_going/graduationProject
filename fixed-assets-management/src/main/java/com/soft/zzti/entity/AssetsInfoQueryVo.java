package com.soft.zzti.entity;
/**
 * TAssetsInfoBean扩展类
 * 设备查询
 * @author Administrator
 *
 */
public class AssetsInfoQueryVo extends TAssetsInfoBean{
	/*
	 * 剩余数量
	 * 剩余数量 = 可申请数量（库存数量inventory） + t_application表中入库申请（apply_state=1 && approve_state in(0,1)）
	 * 				并且入库申请的审批状态为待审核和部门管理员已审核的状态的 数量，暂时称它为【待入库数量】
	 */
	//剩余数量
	private Integer surplusAsset;
	//待入库数量
	private Integer stockPendingAsset;
	public Integer getSurplusAsset() {
		return surplusAsset;
	}
	public void setSurplusAsset(Integer surplusAsset) {
		this.surplusAsset = surplusAsset;
	}
	public Integer getStockPendingAsset() {
		return stockPendingAsset;
	}
	public void setStockPendingAsset(Integer stockPendingAsset) {
		this.stockPendingAsset = stockPendingAsset;
	}

	
}
