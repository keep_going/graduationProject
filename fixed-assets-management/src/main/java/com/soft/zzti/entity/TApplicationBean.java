package com.soft.zzti.entity;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * 对应t_application表 有几个注意点
 * 1.applyState 申请标识
 * 2.approvePesron1 approvePesron2审批人1 2 
 *  如果是普通员工申请，则需要这个个都审核，
 *  如果是部门管理员申请，只需要approvePesron2超级管理员审核
 * 3.当applyState=1时，assetInType=0是为已有设备添加数量，assetInType=1是新增加一个设备
 * 	操作t_assets_info表
 * @author hucl
 *
 */
public class TApplicationBean {
	private Integer mainID;				//t_application自增主键
	private String applyID;				//申请id,时间戳 20180403104300
	private Integer assetID;			//申请的设备id
	private String assetName;			//申请的设备名称
	private Integer applyNumber;		//申请数量
	private String applyPerson;			//申请人id 对应t_staff表中staff_id
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date applyDate;				//申请时间
	private String applyReason;			//申请原因
	private Integer applyState;			//申请标识：0领用申请，1设备入库申请，2流动申请，3维修，4报废
	private Integer assetInType;		//当applyState=1时，assetInType=0是为已有设备添加数量，assetInType=1是新增加一个设备t_assets_info表
	private String approvePesron1;		//审批人 部门管理员审核
	private String approvePesron2;		//审批人 超级管理员审核 
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date approveDate;			//审批时间
	private Integer approveState;		//审批状态 0待审核，1部门管理员审批通过，2审批成功（超级管理员审批通过），3审批失败
	private String approveRemork;		//审批备注
	
	
	public Integer getMainID() {
		return mainID;
	}
	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}
	public String getApplyID() {
		return applyID;
	}
	public void setApplyID(String applyID) {
		this.applyID = applyID;
	}
	public Integer getAssetID() {
		return assetID;
	}
	public void setAssetID(Integer assetID) {
		this.assetID = assetID;
	}
	public Integer getApplyNumber() {
		return applyNumber;
	}
	public void setApplyNumber(Integer applyNumber) {
		this.applyNumber = applyNumber;
	}
	public String getApplyPerson() {
		return applyPerson;
	}
	public void setApplyPerson(String applyPerson) {
		this.applyPerson = applyPerson;
	}
	public Date getApplyDate() {
		return applyDate;
	}
	public void setApplyDate(Date applyDate) {
		this.applyDate = applyDate;
	}
	public String getApplyReason() {
		return applyReason;
	}
	public void setApplyReason(String applyReason) {
		this.applyReason = applyReason;
	}
	public Integer getApplyState() {
		return applyState;
	}
	public void setApplyState(Integer applyState) {
		this.applyState = applyState;
	}
	public String getApprovePesron1() {
		return approvePesron1;
	}
	public void setApprovePesron1(String approvePesron1) {
		this.approvePesron1 = approvePesron1;
	}
	public String getApprovePesron2() {
		return approvePesron2;
	}
	public void setApprovePesron2(String approvePesron2) {
		this.approvePesron2 = approvePesron2;
	}
	public Date getApproveDate() {
		return approveDate;
	}
	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}
	public Integer getApproveState() {
		return approveState;
	}
	public void setApproveState(Integer approveState) {
		this.approveState = approveState;
	}
	public String getApproveRemork() {
		return approveRemork;
	}
	public void setApproveRemork(String approveRemork) {
		this.approveRemork = approveRemork;
	}
	public String getAssetName() {
		return assetName;
	}
	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}
	public Integer getAssetInType() {
		return assetInType;
	}
	public void setAssetInType(Integer assetInType) {
		this.assetInType = assetInType;
	}
	
	
	
}
