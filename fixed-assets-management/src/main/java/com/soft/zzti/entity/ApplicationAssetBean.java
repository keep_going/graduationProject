package com.soft.zzti.entity;
/**
 * 为了系统的可扩展性，对实体类进行扩展
 * TApplicationBean & TAssetsInfoBean
 * @author hucl
 *
 */

import java.sql.Date;

public class ApplicationAssetBean extends TApplicationBean{
	//查询条件中的开始时间和结束时间 开始不能大于结束
	private Date startTime;
	private Date endTime;
	//分页中的开始
	private Integer start;
	//数量
	private Integer length;
	
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public Integer getStart() {
		return start;
	}
	public void setStart(Integer start) {
		this.start = start;
	}
	public Integer getLength() {
		return length;
	}
	public void setLength(Integer length) {
		this.length = length;
	}
	
	

}
