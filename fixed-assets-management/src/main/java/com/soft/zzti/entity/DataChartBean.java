package com.soft.zzti.entity;

public class DataChartBean {
	private String applyStateName;
	private Integer applyNumber;
	private Integer totalInventory;
	private Integer applyState;
	
	public String getApplyStateName() {
		return applyStateName;
	}
	public void setApplyStateName(String applyStateName) {
		this.applyStateName = applyStateName;
	}
	public Integer getApplyNumber() {
		return applyNumber;
	}
	public void setApplyNumber(Integer applyNumber) {
		this.applyNumber = applyNumber;
	}
	public Integer getTotalInventory() {
		return totalInventory;
	}
	public void setTotalInventory(Integer totalInventory) {
		this.totalInventory = totalInventory;
	}
	public Integer getApplyState() {
		return applyState;
	}
	public void setApplyState(Integer applyState) {
		this.applyState = applyState;
	}
	@Override
	public String toString() {
		return "DataChartBean [applyStateName=" + applyStateName + ", applyNumber=" + applyNumber + ", totalInventory="
				+ totalInventory + ", applyState=" + applyState + "]";
	}
	
	

}
