package com.soft.zzti.entity;
/**
 * 资产信息t_assets_info
 * @author hucl
 *
 */

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class TAssetsInfoBean {
	//资产信息id
	private Integer assetID;
	//资产信息名称
	private String assetName;
	//生产厂家
	private String manufacture;
	//库存数量
	private Integer inventory;
	//备注
	private String remark;
	//入库人
	private String inPerson;
	//入库时间
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date inDate;
	//资产类别id
	private Integer class_id;
	//资产总数量
	private Integer total_inventory;
	public Integer getAssetID() {
		return assetID;
	}
	public void setAssetID(Integer assetID) {
		this.assetID = assetID;
	}
	public String getAssetName() {
		return assetName;
	}
	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}
	public String getManufacture() {
		return manufacture;
	}
	public void setManufacture(String manufacture) {
		this.manufacture = manufacture;
	}
	public Integer getInventory() {
		return inventory;
	}
	public void setInventory(Integer inventory) {
		this.inventory = inventory;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getInPerson() {
		return inPerson;
	}
	public void setInPerson(String inPerson) {
		this.inPerson = inPerson;
	}
	public Date getInDate() {
		return inDate;
	}
	public void setInDate(Date inDate) {
		this.inDate = inDate;
	}
	public Integer getClass_id() {
		return class_id;
	}
	public void setClass_id(Integer class_id) {
		this.class_id = class_id;
	}
	public Integer getTotal_inventory() {
		return total_inventory;
	}
	public void setTotal_inventory(Integer total_inventory) {
		this.total_inventory = total_inventory;
	}
	
	@Override
	public String toString() {
		return "TAssetsInfoBean [assetID=" + assetID + ", assetName=" + assetName + ", manufacture=" + manufacture
				+ ", inventory=" + inventory + ", remark=" + remark + ", inPerson=" + inPerson + ", inDate=" + inDate
				+ ", class_id=" + class_id + ", total_inventory=" + total_inventory + "]";
	}
	

}
