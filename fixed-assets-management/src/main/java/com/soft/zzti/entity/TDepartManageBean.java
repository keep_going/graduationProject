package com.soft.zzti.entity;
/**
 * 部门管理员
 * @author hucl
 *
 */
public class TDepartManageBean {
	//部门管理员id
	private String managerID;
	//部门管理员id
	private String managerName;
	//部门id
	private Integer departID;
	//超级管理员id（所属上级）
	private String superID;
	//联系电话
	private String contactPhone;
	//邮箱
	private String email;
	
	public String getManagerID() {
		return managerID;
	}
	public void setManagerID(String managerID) {
		this.managerID = managerID;
	}
	public Integer getDepartID() {
		return departID;
	}
	public void setDepartID(Integer departID) {
		this.departID = departID;
	}
	public String getSuperID() {
		return superID;
	}
	public void setSuperID(String superID) {
		this.superID = superID;
	}
	public String getContactPhone() {
		return contactPhone;
	}
	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}
	public String getManagerName() {
		return managerName;
	}
	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Override
	public String toString() {
		return "TDepartManageBean [managerID=" + managerID + ", managerName=" + managerName + ", departID=" + departID
				+ ", superID=" + superID + ", contactPhone=" + contactPhone + ", email=" + email + "]";
	}
	

}
