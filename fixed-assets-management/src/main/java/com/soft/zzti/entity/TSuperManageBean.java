package com.soft.zzti.entity;
/**
 * 
 * @author 超级管理员
 *
 */
public class TSuperManageBean {
	private String superID;
	private String contactPhone;
	private String description;
	private String email;
	
	public String getSuperID() {
		return superID;
	}
	public void setSuperID(String superID) {
		this.superID = superID;
	}
	public String getContactPhone() {
		return contactPhone;
	}
	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Override
	public String toString() {
		return "TSuperManageBean [superID=" + superID + ", contactPhone=" + contactPhone + ", description="
				+ description + ", email=" + email + "]";
	}
	
	

}
