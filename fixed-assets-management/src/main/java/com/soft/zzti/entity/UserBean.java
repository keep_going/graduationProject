package com.soft.zzti.entity;

/**
 * Created by hucl on 2018/2/7.
 * test
 */
public class UserBean {
	private String username;
	private Integer roleID;
	//普通员工
	private TStaffBean staff;
	//部门管理员
	private TDepartManageBean departManage;
	//超级管理员
	private TSuperManageBean superManage;
	
	public TStaffBean getStaff() {
		return staff;
	}
	public void setStaff(TStaffBean staff) {
		this.staff = staff;
	}
	public TDepartManageBean getDepartManage() {
		return departManage;
	}
	public void setDepartManage(TDepartManageBean departManage) {
		this.departManage = departManage;
	}
	public TSuperManageBean getSuperManage() {
		return superManage;
	}
	public void setSuperManage(TSuperManageBean superManage) {
		this.superManage = superManage;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Integer getRoleID() {
		return roleID;
	}
	public void setRoleID(Integer roleID) {
		this.roleID = roleID;
	}
	
	
}
