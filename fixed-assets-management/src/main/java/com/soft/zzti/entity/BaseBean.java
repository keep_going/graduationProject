package com.soft.zzti.entity;
/**
 * 分页设置
 * @author Administrator
 *
 */
public class BaseBean {
	//分页中的开始
	private Integer start;
	//数量
	private Integer length;
	//第几次请求
	private Integer draw;

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	public Integer getDraw() {
		return draw;
	}

	public void setDraw(Integer draw) {
		this.draw = draw;
	}
	
	
}
