package com.soft.zzti.entity;

import java.util.Map;
/**
 * 响应Json实体类
 * @author hucl
 *
 */
public class ResponseJsonBean {
	
	private String msgFlag;
	private Map<String, Object> resultJson;
	private String errorMsg;
	public String getMsgFlag() {
		return msgFlag;
	}
	public void setMsgFlag(String msgFlag) {
		this.msgFlag = msgFlag;
	}
	
	public Map<String, Object> getResultJson() {
		return resultJson;
	}
	public void setResultJson(Map<String, Object> resultJson) {
		this.resultJson = resultJson;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	
	

}
