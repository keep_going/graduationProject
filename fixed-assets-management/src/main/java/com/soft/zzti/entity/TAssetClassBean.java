package com.soft.zzti.entity;
/**
 * 资产类别
 * @author hucl
 *
 */
public class TAssetClassBean {
	//类别id
	private Integer classID;
	//类别名称
	private String className;
	//描述
	private String classDescription;
	
	public Integer getClassID() {
		return classID;
	}
	public void setClassID(Integer classID) {
		this.classID = classID;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public String getClassDescription() {
		return classDescription;
	}
	public void setClassDescription(String classDescription) {
		this.classDescription = classDescription;
	}
	@Override
	public String toString() {
		return "TAssetClassBean [classID=" + classID + ", className=" + className + ", classDescription="
				+ classDescription + "]";
	}
	
	

}
