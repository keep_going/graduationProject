package com.soft.zzti.entity;
/**
 * 普通员工类
 * @author hucl
 *
 */
public class TStaffBean {
	//员工id
	private String staffID;
	//员工所在的部门id
	private Integer departID;
	//角色id
	private Integer roleID;
	//联系电话
	private String contactPhone;
	//员工姓名
	private String staffName;
	//员工的上级id 也就是部门管理员id
	private String higherupID;
	//邮箱
	private String email;
	
	public String getStaffID() {
		return staffID;
	}
	public void setStaffID(String staffID) {
		this.staffID = staffID;
	}
	public Integer getDepartID() {
		return departID;
	}
	public void setDepartID(Integer departID) {
		this.departID = departID;
	}
	public Integer getRoleID() {
		return roleID;
	}
	public void setRoleID(Integer roleID) {
		this.roleID = roleID;
	}
	public String getContactPhone() {
		return contactPhone;
	}
	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getHigherupID() {
		return higherupID;
	}
	public void setHigherupID(String higherupID) {
		this.higherupID = higherupID;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Override
	public String toString() {
		return "TStaffBean [staffID=" + staffID + ", departID=" + departID + ", roleID=" + roleID + ", contactPhone="
				+ contactPhone + ", staffName=" + staffName + ", higherupID=" + higherupID + ", email=" + email + "]";
	}
	
	

}
