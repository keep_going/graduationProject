package com.soft.zzti.entity;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

public class TPassword {
	//表字段自动增长id
	private Integer auto_id;
	//用户id
	@NotEmpty(message="{TPassword.user_id.IsNULL}")//非空检验
	private String user_id;
	//密码
	@NotBlank(message="{TPassword.user_password.IsNULL}")
	private String user_password;
	private String user_name;
	//联系电话
	private String contact_phone;
	private Integer role_id;
	
	public Integer getAuto_id() {
		return auto_id;
	}
	public void setAuto_id(Integer auto_id) {
		this.auto_id = auto_id;
	}
	
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getUser_password() {
		return user_password;
	}
	public void setUser_password(String user_password) {
		this.user_password = user_password;
	}
	public String getContact_phone() {
		return contact_phone;
	}
	public void setContact_phone(String contact_phone) {
		this.contact_phone = contact_phone;
	}
	public Integer getRole_id() {
		return role_id;
	}
	public void setRole_id(Integer role_id) {
		this.role_id = role_id;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	@Override
	public String toString() {
		return "TPassword [auto_id=" + auto_id + ", user_id=" + user_id + ", user_password=" + user_password
				+ ", user_name=" + user_name + ", contact_phone=" + contact_phone + ", role_id=" + role_id + "]";
	}
	

}
