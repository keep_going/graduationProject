package com.soft.zzti.entity;
/**
 * 设备申请审批：部门管理员审批；超级管理员审批
 * @author hucl
 *
 */

import java.sql.Date;

public class AssetApproveQueryVo {
	
	private TSuperManageBean superManager;
	
	private TDepartManageBean departManager;
	
	private Date startTime;
	
	private Date endTime;

	public TSuperManageBean getSuperManager() {
		return superManager;
	}

	public void setSuperManager(TSuperManageBean superManager) {
		this.superManager = superManager;
	}

	public TDepartManageBean getDepartManager() {
		return departManager;
	}

	public void setDepartManager(TDepartManageBean departManager) {
		this.departManager = departManager;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	
}
