package com.soft.zzti.entity;
/**
 * 普通员工权限 实体类
 * t_staff_right
 * @author hucl
 *
 */
public class TStaffRightBean {
	private String staffID;
	private Integer rightID;
	public String getStaffID() {
		return staffID;
	}
	public void setStaffID(String staffID) {
		this.staffID = staffID;
	}
	public Integer getRightID() {
		return rightID;
	}
	public void setRightID(Integer rightID) {
		this.rightID = rightID;
	}
	@Override
	public String toString() {
		return "TStaffRightBean [staffID=" + staffID + ", rightID=" + rightID + "]";
	}
	
	

}
