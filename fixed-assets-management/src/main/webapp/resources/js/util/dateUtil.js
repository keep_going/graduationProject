/**
 *扩展date类的方法
 */
Date.prototype.getDefaultSpilit=function(){
	return "-";
};
//获取当前日期
Date.prototype.getCurrentDate=function(split){
	return this.getDaysAgoDate(0);
};
//获取一周前的日期
Date.prototype.getWeekAgoDate=function(split){
	return this.getDaysAgoDate(7,split);
};
//获取days前的日期
Date.prototype.getDaysAgoDate=function(days,split){
	if(!split||typeof split=='undefined'){
		split=this.getDefaultSpilit();
	}
    var date=new Date(new Date().getTime() - days * 24 * 3600 * 1000);
	var year = date.getFullYear() ; //年
    var month = date.getMonth()+1<10?"0"+(date.getMonth()+1):date.getMonth()+1 ; //月
    var day = date.getDate()<10?"0"+date.getDate():date.getDate() ; //日期
    return ""+year+split+month+split+day;
};
//格式化日期
Date.prototype.Format = function(fmt) {
    var o = { 
        "M+": this.getMonth() + 1, 
        //月份 
        "d+": this.getDate(), 
        //日 
        "h+": this.getHours(), 
        //小时 
        "m+": this.getMinutes(), 
        //分 
        "s+": this.getSeconds(), 
        //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), 
        //季度 
        "S": this.getMilliseconds() //毫秒 
    }; 
    if (/(y+)/.test(fmt)) { 
        fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length)); 
    } 
    for (var k in o) { 
        if (new RegExp("(" + k + ")").test(fmt)) { 
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length))); 
        } 
    } 
    return fmt; 
} 