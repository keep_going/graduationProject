/**
 * 将一些公共的业务处理放入此对象中 
 */
	var Common = new function(){
		/*调用服务*/
		this._Mobile=new Mobile();
		this.callSvc = function(action,param,callback){
			param = param ? param : new Wade.DataMap();
			var xhr=$.ajax({
				  url:action,
				  type:'post',         //数据发送方式
				  async:true,
				  dataType:'json',     //接受数据格式
				  data:this.parse(param),         //要传递的数据
				 // data:{"staticPayType":[{"payCode":"crmO6008"},{"payCode":"crmO6006"}]},
				  success:callback
				});
        return xhr;   				
		};
		this.parse =function(param){
			var jsonStr={};
			param.eachKey(function(key,value){
				jsonStr[key]=  typeof value =="object"?  value.toString():value;
			});
			return jsonStr;
		}		;						
		this.put = function(key, value) {
			if(!this.checkMapKey(key)){
				return;
			}
			this._Mobile.setMemoryCache(key, value);
			this._Mobile.setOfflineCache(key, value);
		};
		this.get = function(callback, key, value) {
			if(!this.checkArrayKey(key)){
				return;
			}
			this._Mobile.getMemoryCache(callback, key, value);
		};		
		this.clear = function() {
		    this._Mobile.clearMemoryCache();
		};
		this.putLocal = function(key, value) {
			if(!this.checkMapKey(key)){
				return;
			}
			this._Mobile.setOfflineCache(key, value);
		};
		this.getLocal = function(callback, key, value) {
			if(!this.checkArrayKey(key)){
				return;
			}
			this._Mobile.getOfflineCache(callback, key,value);
		};
		this.removeLocal = function(key) {
			if(!this.checkArrayKey(key)){
				return;
			}
			this._Mobile.removeOfflineCache(key);
		};
		this.clearLocal = function() {
			this._Mobile.clearOfflineCache();
		};
		
		
		this.checkMapKey=function(key){
			if (!key || (typeof (key) != "string" && !this._Mobile.isDataMap(key))) {
				alert(key+"参数类型异常");
				return false;
			} else {
				return true;
			}
		};
		this.is_weixin=function(){
			var ua = navigator.userAgent.toLowerCase();
			if(ua.match(/MicroMessenger/i)=="micromessenger") {
				return true;
		 	} else {
				return false;
			}
		};
		this.checkArrayKey=function(key){
			if (!key || (typeof (key) != "string" && !this._Mobile.isArray(key))) {
				alert(key+"参数类型异常");
				return false;
			} else {
				return true;
			}
		};
		/*
		 * 获取url中的参数
		 * */
    	this.getUrlParam= function(){
    		var uri = window.location.search;
    		var re = /\w*\=([^\&\?]*)/ig;
    		var retval= Wade.DataMap() ;
    		while ((arr = re.exec(uri)) != null){       			  
    			var arg = arr[0].split("=");
    			 if(arg.length <= 1){
    				 retval.put(arg[0],"");
    			 }else{
    				 retval.put(arg[0],arg[1]);
    			 } 
    		}       		
    		return retval; 
    	};
    	// 获取当前时间值
    	this.getDateTime=function (){
    		var nowDate = new Date();
    		// 获取年月日时分秒毫秒
    		var year = nowDate.getFullYear();
    		var month = nowDate.getMonth() + 1; // 0~11
    		if(month < 10){
    			month = "0" + month;
    		}
    		var date = nowDate.getDate();	//1~31
    		if(date < 10){
    			date = "0" + date;
    		}
    		var hour = nowDate.getHours();	// 0~59
    		if(hour < 10){
    			hour = "0" + hour;
    		}
    		var minute = nowDate.getMinutes();	// 0~59
    		if(minute < 10){
    			minute = "0" + minute;
    		}
    		var second = nowDate.getSeconds();	// 0~59
    		if(second < 10){
    			second = "0" + second;
    		}
    		var ms = nowDate.getMilliseconds();	// 0~999
    		if(ms < 10){
    			ms = "00" + ms;
    		}else if(ms < 100){
    			ms = "0" + ms;
    		}

    		return (""+year+month+date+hour+minute+second+ms);
    	};

    	// 获取当前日期 格式为：月-日-年  
    	 this.getDatePattern =function(){
    		var nowDate = new Date();
    		// 获取年月日时分秒毫秒
    		var year = nowDate.getFullYear();
    		var month = nowDate.getMonth() + 1; // 0~11
    		if(month < 10){
    			month = "0" + month;
    		}
    		var date = nowDate.getDate();	//1~31
    		if(date < 10){
    			date = "0" + date;
    		}

    		return (year+"-"+month+"-"+date);
    	};
    	// 获取当前日期 格式为：月-日-年  
   	 this.getDatePatternAll =function(){
   		var nowDate = new Date();
   		// 获取年月日时分秒毫秒
   		var year = nowDate.getFullYear();
   		var month = nowDate.getMonth() + 1; // 0~11
   		if(month < 10){
   			month = "0" + month;
   		}
   		var date = nowDate.getDate();	//1~31
   		if(date < 10){
   			date = "0" + date;
   		}
   		var hour = nowDate.getHours();	// 0~59
		if(hour < 10){
			hour = "0" + hour;
		}
		var minute = nowDate.getMinutes();	// 0~59
		if(minute < 10){
			minute = "0" + minute;
		}
		var second = nowDate.getSeconds();	// 0~59
		if(second < 10){
			second = "0" + second;
		}
   		return (year+"-"+month+"-"+date+" "+hour+":"+minute+":"+second);
   	};

	};
	
	
	window.Constant = {
		OPEN_PAGE_KEY : "OPEN_PAGE_KEY",
		STAFF_ID : "STAFF_ID",
		SESSION_ID : "SESSION_ID",
		X_RECORDNUM : "X_RECORDNUM",
		X_RESULTCODE : "X_RESULTCODE",
		X_RESULTINFO : "X_RESULTINFO",
		X_RESULTCAUSE : "X_RESULTCAUSE"
	};
	 window.alert = function(name){  
	        var iframe = document.createElement("IFRAME");  
	        iframe.style.display="none";  
	        iframe.setAttribute("src", 'data:text/plain,');  
	        document.documentElement.appendChild(iframe);  
	        window.frames[0].window.alert(name);  
	        iframe.parentNode.removeChild(iframe);  
	    }  