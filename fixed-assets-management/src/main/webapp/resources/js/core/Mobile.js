
var Mobile =function(){

	/*判断是否是DataMap类型*/
	this.isDataMap = function(key){
		return typeof (key) == "object" && (key instanceof Wade.DataMap);
	};
	/*判断是否是Array类型*/
	this.isArray = function(key){
		return typeof (key) == "object" && key.constructor == Array;
	};
	/*提取DatasetList中的json对象*/
	this.parseList = function(list){
		if(!(list instanceof Wade.DatasetList)){
			return list;
		}
		for(var i=0,len=list.items.length;i<len;i++){
			if(list.items[i] instanceof Wade.DataMap){
				list.items[i] = this.parseData(list.items[i]);
			}else if(list.items[i] instanceof Wade.DatasetList){
				list.items[i] = this.parseList(list.items[i]);
			}
		}
		return list.items;
	};
	/*提取DataMap中的json对象*/
	this.parseData = function (param){
		if(!(param instanceof Wade.DataMap)){
			return param;
		}
		for(var key in param.map){
			if(param.map[key] instanceof Wade.DataMap){
				param.map[key] = this.parseData(param.map[key]);
			}else if(param.map[key] instanceof Wade.DatasetList){
				param.map[key] = this.parseList(param.map[key]);
			}
		}
		return param.map;
	};
	this.StrStartWith=function(str,regStr){     
		  var reg=new RegExp("^"+regStr);     
		  return reg.test(str);        
	};
	this.StrEndWith=function(str,regStr){     
	  var reg=new RegExp(regStr+"$");     
	  return reg.test(str);        
	};
	this.isDataMapString = function(str){
		if(typeof(str) != "string"){
			return false;
		}
		var trimStr = str.trim();
		return StrStartWith(trimStr,"{") && StrEndWith(trimStr,"}");
	};
	this.getDataMap = function(key){
		if(isDataMap(key)){
			return key;
		}else if(isDataMapString(key)){
			return Wade.DataMap(key);
		}else{
			throw "返回数据异常，请尝试重新登陆或联系管理员。(返回的数据为：" + key + ")";
		}
	};
	this.sessionError=function(){
		alert("your browser not support sessionStorage!");
	};
	
	this.localError=function(){
		alert("your browser not support localStorage!");
	};
	 
	/*回退到前一个界面*/
	this.back = function(){
		history.go(-1);
	};
};
		
Mobile.prototype={
        	loadingStart : function(message,title){
        		// 等待开发
        	},loadingStop : function(){
        		// 等待开发
        	},confirm : function(msg){
        		window.confirm(msg);
        	},tip : function(msg,type){
        		// 等待开发
        		alert(msg);
        	},alert : function(msg,title,callback){
        		alert(msg);
        		if(callback){
        			callback();
        		}
        	},setMemoryCache : function(key, value){
        		if (window["sessionStorage"]) {
    				if (this.isDataMap(key)) {
    					for ( var k in key.map) {
    						window["sessionStorage"].setItem(k, key.map[k]);
    					}
    				} else {
    					if (!value) {
    						return
    					}
    					window["sessionStorage"].setItem(key, value);
    				}
    			} else {
    				sessionError();
    			}
        	},getMemoryCache : function(callback,key, value){
        		if (window["sessionStorage"]) {
    				if (this.isArray(key)) {
    					var data = new Wade.DataMap();
    					for ( var i = 0, len = key.length; i < len; i++) {
    						data.put(key[i], window["sessionStorage"].getItem(key[i]));
    					}
    					callback(data);
    					return;
    				} else {
    					if(window["sessionStorage"].getItem(key)&&window["sessionStorage"].getItem(key)!=undefined){
    						callback(window["sessionStorage"].getItem(key));
        					return;
    					}else{
    						callback(window["localStorage"].getItem(key));
        					return;
    					}
    					
    				}
    			} else {
    				sessionError();
    			}
        	},removeMemoryCache : function(key){
        		if (window["sessionStorage"]) {
    				if (this.isArray(key)) {
    					for ( var i = 0, len = key.length; i < len; i++) {
    						window["sessionStorage"].removeItem(key[i]);
    					}
    				} else {
    					window["sessionStorage"].removeItem(key);
    				}
    			} else {
    				sessionError();
    			}
        	},clearMemoryCache : function(){
        		if (window["sessionStorage"]) {
    				window["sessionStorage"].clear();
    			} else {
    				sessionError();
    			}
        	},setOfflineCache : function(key, value){
        		if (window["localStorage"]) {
    				if (this.isDataMap(key)) {
    					for ( var k in key.map) {
    						window["localStorage"].setItem(k, key.map[k]);
    					}
    				} else {
    					if (!value) {
    						return
    					}
    					window["localStorage"].setItem(key, value);
    				}
    			} else {
    				localError();
    			}
        	},getOfflineCache : function(callback, key, value){
        		if (window["localStorage"]) {
    				if (this.isArray(key)) {
    					var data = new Wade.DataMap();
    					for ( var i = 0, len = key.length; i < len; i++) {
    						data.put(key[i], window["localStorage"].getItem(key[i]));
    					}
    					callback(data);
    					return;
    				} else {
    					callback(window["localStorage"].getItem(key));
    					return;
    				}
    			} else {
    				localError();
    			}
        	},removeOfflineCache : function(key){
        		if (window["localStorage"]) {
    				if (this.isArray(key)) {
    					for ( var i = 0, len = key.length; i < len; i++) {
    						window["localStorage"].removeItem(key[i]);
    					}
    				} else {
    					window["localStorage"].removeItem(key);
    				}
    			} else {
    				localError();
    			}
        	},clearOfflineCache : function(){
        		if (window["localStorage"]) {
    				window["localStorage"].clear();
    			} else {
    				localError();
    			}
        	},
																				
			/*
			 * data 参数必须是json格式 
			 */
			openPostWindow : function(name, url, data){
        		var temp = document.createElement("form");
		    	temp.action = url;
		    	temp.method = "post";
 			    temp.style.display = "none";
 			    temp.target = name;
 			    
 			    for (var x in data) {
			    	var ele = document.createElement("input");
			    	ele.type="hidden";  
			    	ele.name = x;
			    	ele.value =  data[x];
			    	temp.appendChild(ele);
			    }
 			   	document.body.appendChild(temp);
 			   	temp.submit();
 			   	temp.parentNode.removeChild(temp);
        	}
        };





