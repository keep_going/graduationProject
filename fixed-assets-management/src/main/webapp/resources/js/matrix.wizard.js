
$(document).ready(function(){
	//表单校验
	$("#form-wizard").validate({
		rules: {
			username: "required",
			password: {
				required: true,
		        minlength: 6
			},
			password2: {
				equalTo: "#password"
			}
		},
		messages: {
			username: "Please enter your name or username",
			password: {
				required: "You must enter the password",
				minlength: "The minimum password length is 6"
			},
			password2: { equalTo: "Password don't match" }
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
		$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
		},
		//按钮提交事件
		submitHandler:function(form){
            alert("提交事件!");   
            alert("click subBtn");
    		var param = new Wade.DataMap();
    		param.put("password",$("#password").val());
    		param.put("username",$("#username").val());
    		console.log(param.toString());
        }  
	});
	
});
