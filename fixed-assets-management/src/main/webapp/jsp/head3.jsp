<%@ page language="java" contentType="text/html;utf-8" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!--<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/js/layui/css/layui.css"/>  -->
<script src="${pageContext.request.contextPath}/resources/js/layui/layui.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#logOut").click(function(){
			if(confirm("确定要退出吗？")){
				window.location.href = "${pageContext.request.contextPath}/user/logout.do"
			}
		});
		var sessionValue = $("#userSession").text();
		sessionValue = Wade.DataMap(sessionValue);
		var superId = sessionValue.get('userInfo').get('superID');
		$("#welcomeUser").text(superId);
	});
	function modifyPsw(){	  
		layui.use('layer', function(){
			layer.open({
			  title: '密码修改',
			  offset: ['100px', '500px'],
			  area: ['400px', '260px'],
			  yes: function(index, layero){
				var oldPassword = $("#oldPassword").val();
				var password1 = $("#password1").val();
				var password2 = $("#password2").val();
				if(password1!='' && oldPassword !='' && password2!=''){
					if(oldPassword == password1){
						alert("新密码不能和旧密码一致");
						return;
					}
					if(password1!=password2){
						alert("两次密码不一致！");
						return;
					}
				}else{
					alert("密码不能为空！");
					return;
				}
				var param = Wade.DataMap();
				param.put("password",password1);
				param.put("username",$("#welcomeUser").text());
				console.log(param.toString());
				Common.callSvc("/fixed-assets-management/user/findPassword.do",param,function(resultData){
					resultData = Wade.DataMap(resultData);
					if("0" == resultData.get("msgFlag")){
						alert("修改成功")
						window.location.href="/fixed-assets-management/jsp/index/SuperIndex.jsp";
					}else{
						alert(resultData.get("errorMsg"));
					}
				});
				
				layer.close(index); //如果设定了yes回调，需进行手工关闭
			  },
			  content: '<table style width="300" border="0"><tr><td style="float: right">旧密码</td><td><input type="password" name="oldPassword" id="oldPassword" style="float: left" autocomplete="off" placeholder="旧密码" class="layui-input"></td></tr><tr><td style="float: right">新密码</td><td><input type="password" name="password1" id="password1" style="float: left" autocomplete="off" placeholder="新密码" class="layui-input"></td></tr><tr><td style="float: right">重复密码</td><td><input type="password" name="password2" id="password2" style="float: left" autocomplete="off" placeholder="重复密码" class="layui-input"></td></tr></table>'
			});
			//旧密码的校验
			$("#oldPassword").blur(function(){
				var oldPassword = $("#oldPassword").val();
				var param = Wade.DataMap();
				param.put("oldPassword",oldPassword);
				param.put("userId",$("#welcomeUser").text());
				param.put("role_id",1);
				console.log(param.toString());
				Common.callSvc("/fixed-assets-management/user/checkOldPassword.do",param,function(resultData){
					resultData = Wade.DataMap(resultData);
					if("0" == resultData.get("msgFlag")){
						return;
					}else{
						$("#oldPassword").val('');
						alert(resultData.get("errorMsg"));
					}
				});
			});
		});
	}
</script>
</head>
<body>
	
	<!--Header-part-->
	<div id="header">
  		<h1><a href="${pageContext.request.contextPath}/jsp/dashboard/dashboard.html">固定资产管理</a></h1>
	</div>
	<span style="display: none" id="userSession"><%=session.getAttribute("userSession")%></span>
	<!--close-Header-part--> 
	<!--top-Header-menu-->
	<div id="user-nav" class="navbar navbar-inverse">
		<ul class="nav">
			<li class="">
				<a title=""><i class="icon icon-user"></i>
					<span class="text">Welcome&nbsp;&nbsp;<span style="color: #fff;" id="welcomeUser"></span></span>
				</a>
			</li>
			<li  class="dropdown" id="profile-messages" ><a title="" href="#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle"><i class="icon icon-user"></i>  <span class="text">Settings</span><b class="caret"></b></a>
      			<ul class="dropdown-menu">
			        <li><a href="${pageContext.request.contextPath}/jsp/userInfo/superManagerInfo.jsp"><i class="icon-user"></i> 个人信息</a></li>
			        <li class="divider"></li>
			        <li><a href="#" id="modifyPassword" onclick="modifyPsw()"><i class="icon-key"></i> 修改密码</a></li>
			   	</ul>
    		</li>
			<li class="">
				<a title="退出">
					<i class="icon icon-share-alt"></i>
					<span class="text" id="logOut">Logout</span>
				</a>
			</li>
		</ul>
	</div>
	<!--close-top-Header-menu-->
	<!--sidebar-menu-->
	<div id="sidebar">
		<ul>
			<li>
				<a href="${pageContext.request.contextPath}/jsp/index/SuperIndex.jsp">
					<i class="icon icon-home"></i> <span>首页</span>
				</a>
			</li>
			<li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>设备管理</span> 
			<span class="label label-important">2</span></a>
     			<ul>
        			<li><a href="${pageContext.request.contextPath}/jsp/superManager/deviceManage/DeviceApplicationList.jsp">设备申请管理</a></li>
        			<li><a href="${pageContext.request.contextPath}/jsp/superManager/deviceManage/DeviceInfoList.jsp">设备信息管理</a></li>
      			</ul>
    		</li>
			<li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>用户管理</span> 
			<span class="label label-important">2</span></a>
     			<ul>
        			<li><a href="${pageContext.request.contextPath}/jsp/superManager/userManage/CommonInfoList.jsp">普通员工</a></li>
        			<li><a href="${pageContext.request.contextPath}/jsp/superManager/userManage/DepartManagerInfoList.jsp">部门管理员</a></li>
      			</ul>
    		</li>
			<li id="">
				<a href="${pageContext.request.contextPath}/jsp/superManager/approveManage/ApproveManage.jsp">
					<i class="icon icon-align-center"></i> <span>审批管理</span>
				</a>
			</li>
			<li id="">
				<a href="${pageContext.request.contextPath}/jsp/superManager/rightManage/UserRightList.jsp">
					<i class="icon icon-sign-blank"></i> <span>权限管理</span>
				</a>
			</li>
			<li id="">
				<a href="${pageContext.request.contextPath}/jsp/superManager/deviceAnalyticStatistics/DeviceAnalytic.jsp">
					<i class="icon icon-pinterest"></i> <span>设备使用情况分析</span>
				</a>
			</li>
			<li>
				<a href="${pageContext.request.contextPath}/jsp/userInfo/SuperManagerInfo.jsp">
					<i class="icon icon-user"></i> <span>个人信息</span>
				</a>
			</li>
		</ul>
	</div>
	<!--sidebar-menu-->
	<div id="modifyPasswordDIV" style="display: none;">
        <label>旧密码：</label>
        <div style="float: right"><input type="password" id="oldPsw"></div>
        <label>新密码：</label>
        <input type="password" id="newPsw">
        <label>重复密码：</label>
        <input type="password" id="reNewPsw">
	</div>
	<table id="modifyPasswordTable" style="width: 300px;border: 0px; display: none;">
		<tr>
			<td style="float: right">旧密码</td>
			<td><input type="text" name="password1"  style="float: left" autocomplete="off" placeholder="旧密码" class="layui-input"></td>
		</tr>
		<tr>
			<td style="float: right">新密码</td>
			<td><input type="text" name="password1"  style="float: left" autocomplete="off" placeholder="新密码" class="layui-input"></td>
		</tr>
		<tr>
			<td style="float: right">重复密码</td>
			<td><input type="text" name="password1"  style="float: left" autocomplete="off" placeholder="重复密码" class="layui-input"></td>
		</tr>
	</table>
</body>
</html>