<%@ page language="java" contentType="text/html;utf-8" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!--<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/js/layui/css/layui.css"/>  -->
<script src="${pageContext.request.contextPath}/resources/js/layui/layui.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#logOut").click(function(){
			if(confirm("确定要退出吗？")){
				window.location.href = "${pageContext.request.contextPath}/user/logout.do"
			}
		});
		$("#modifyPassword").click(function(event){
			event.stopPropagation();//去除父类的事件
			layui.use('layer', function(){
				/* layer.open({
					 type: 1,
				    area: ['600px', '360px'],
				    shadeClose: true, //点击遮罩关闭
					title: '密码修改',
					content: '<table><tr><td style="float: right">旧密码</td><td><input type="text" name="password1"  style="float: left" autocomplete="off" placeholder="旧密码" class="layui-input"></td></tr><tr><td style="float: right">新密码</td><td><input type="text" name="password1"  style="float: left" autocomplete="off" placeholder="新密码" class="layui-input"></td></tr><tr><td style="float: right">重复密码</td><td><input type="text" name="password1"  style="float: left" autocomplete="off" placeholder="重复密码" class="layui-input"></td></tr></table>'
				}); */
				layer.open({
					  type: 1,
					  offset: 'auto',//具体配置参考：http://www.layui.com/doc/modules/layer.html#offset
				      id: 'layerDemo'+'auto', //防止重复弹出
					  skin: 'layui-layer-rim', //加上边框
					  area: ['420px', '240px'], //宽高
					  content: 'html内容'
					});
			});
		
		});
	});
</script>
</head>
<body>
	<!--Header-part-->
	<div id="header">
  		<h1><a href="${pageContext.request.contextPath}/jsp/dashboard/dashboard.html">固定资产管理</a></h1>
	</div>
	<!--close-Header-part--> 
	<!--top-Header-menu-->
	<div id="user-nav" class="navbar navbar-inverse">
		<ul class="nav">
			<li class="">
				<a title=""><i class="icon icon-user"></i>
					<span class="text">Welcome user</span>
				</a>
			</li>
			<li  class="dropdown" id="profile-messages" ><a title="" href="#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle"><i class="icon icon-user"></i>  <span class="text">Settings</span><b class="caret"></b></a>
      			<ul class="dropdown-menu">
			        <li><a href="${pageContext.request.contextPath}/jsp/userInfo/StaffInfo.jsp"><i class="icon-user"></i> 个人信息</a></li>
			        <li class="divider"></li>
			        <li><a href="#" id="modifyPassword"><i class="icon-key"></i> 修改密码</a></li>
			   	</ul>
    		</li>
			<li class="">
				<a title="退出">
					<i class="icon icon-share-alt"></i>
					<span class="text" id="logOut">Logout</span>
				</a>
			</li>
		</ul>
	</div>
	<!--close-top-Header-menu-->
</body>
</html>