
$(document).ready(function(){
	//发送请求的根目录
	var rootDirectory = $("#rootDirectory").val();
	//获取session中的信息 根据员工权限显示 操作按钮
	var sessionValue = $("#sessionInput").text();
	sessionValue = Wade.DataMap(sessionValue);
	if(sessionValue.length<1){
		alert("登录超时，请重新登录");
		window.location.href = rootDirectory+"/jsp/login/Login.jsp";
	}
	//用户信息
	var userInfo = sessionValue.get("userInfo");
	initForm(userInfo);
	
	//表单校验 是根据name属性 不是id
	$("#smInfoForm").validate({
		rules: {
			superId: "required",
			email: {
				required: true,
				email:true
			},
			contactPhone: "required"
		},
		messages: {
			superId: "员工编码不能为空",
			email: {
				required:"邮箱不能为空",
				email:"请输入正确的邮箱格式 "
			},
			contactPhone: "联系方式不能为空"
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
		$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
		},
		//按钮提交事件
		submitHandler:function(form){
    		var param = new Wade.DataMap();
    		param.put("superID",$.trim($("#superId").val()));
    		param.put("contactPhone",$.trim($("#contactPhone").val()));
    		param.put("description",$.trim($("#description").val()));
    		param.put("email",$.trim($("#email").val()));
    		console.log(param.toString());
    		//发送请求
    		Common.callSvc(rootDirectory+"/superManager/updateSuperManagerInfo.do",param,function(resultData){
    			resultData = Wade.DataMap(resultData);
    			console.log(resultData.toString());
    			if("0" == resultData.get("msgFlag")){
    				alert("信息修改成功！");
    				//跳转到登录页面
    				window.location.href = rootDirectory+"/jsp/login/Login.jsp";
    			}else{
    				alert(resultData.get("errorMsg"));
    			}
    		});
        }
	});
});
//填充数据
function initForm(userInfo){
	$("#superId").val(userInfo.get('superID'));
	$("#email").val(userInfo.get('email'));
	$("#contactPhone").val(userInfo.get('contactPhone'));
	$("#description").val(userInfo.get('description'));
}
