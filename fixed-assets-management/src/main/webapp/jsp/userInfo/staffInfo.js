
$(document).ready(function(){
	//发送请求的根目录
	var rootDirectory = $("#rootDirectory").val();
	//获取session中的信息 根据员工权限显示 操作按钮
	var sessionValue = $("#sessionInput").text();
	sessionValue = Wade.DataMap(sessionValue);
	if(sessionValue.length<1){
		alert("登录超时，请重新登录");
		window.location.href = rootDirectory+"/jsp/login/Login.jsp";
	}
	//用户信息
	var userInfo = sessionValue.get("userInfo");
	//员工权限
	var staffRight = sessionValue.get("staffRight");
	if(userInfo == '' || userInfo == null){
		console.error("session信息sessionValue undefined");
	}else{
		/**
		 * staffRight
		 * 1	入库
		 * 2	领用
		 * 3	流动
		 * 4	报废
		 * 5	维修
		 */
		for(var i=0;i<staffRight.length;i++){
			if(1 == staffRight.items[i]){
				$("#addDeviceli").show();
			}
		}
	}
	/*
	 * 表单赋值
	 */
	initForm(userInfo);
	
	//表单校验 是根据name属性 不是id
	$("#staffInfoFrom").validate({
		rules: {
			staffId: "required",
			staffName: "required",
			email: {
				required: true,
				email:true
			},
			phoneNumber: "required",
			departManageID: "required",
			departID: "required",
		},
		messages: {
			staffId: "员工编码不能为空",
			staffName: "员工姓名不能为空",
			email: {
				required:"邮箱不能为空",
				email:"请输入正确的邮箱格式 "
			},
			phoneNumber: "联系方式不能为空",
			departManageID: "部门管理员不能为空",
			departID: "部门不能为空",
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
		$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
		},
		//按钮提交事件
		submitHandler:function(form){
    		var param = new Wade.DataMap();
    		param.put("staffName",$("#staffName").val());
    		param.put("contactPhone",$("#phoneNumber").val());
    		param.put("staffID",$("#staffId").val());
    		param.put("email",$("#email").val());
    		console.log(param.toString());
    		//发送请求
    		Common.callSvc(rootDirectory+"/staff/updateStaffInfo.do",param,function(resultData){
    			resultData = Wade.DataMap(resultData);
    			console.log(resultData.toString());
    			if("0" == resultData.get("msgFlag")){
    				alert("信息修改成功！");
    				//跳转到登录页面
    				window.location.href = rootDirectory+"/jsp/login/Login.jsp";
    			}else{
    				alert(resultData.get("errorMsg"));
    			}
    		});
        }
	});
});
//填充数据
function initForm(userInfo){
	$("#staffId").val(userInfo.get('staffID'));
	$("#staffName").val(userInfo.get('staffName'));
	$("#email").val(userInfo.get('email'));
	$("#phoneNumber").val(userInfo.get('contactPhone'));
	$("#departManageID").val(userInfo.get('higherupID'));
	$("#departID").val(userInfo.get('departID'));
}
