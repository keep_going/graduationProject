<%@ page language="java" import="java.util.*,java.net.*" contentType="text/html;utf-8" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<title>个人信息管理（普通员工）</title>
<meta content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<jsp:include page="../head.jsp"></jsp:include>
<script src="${pageContext.request.contextPath}/jsp/userInfo/staffInfo.js"></script>
</head>
<body>
	<jsp:include page="../head2.jsp"></jsp:include>	
	<input type="hidden" id="rootDirectory" value="${pageContext.request.contextPath}"/>
	<span style="display: none" id="sessionInput"><%=session.getAttribute("userSession")%></span>
	<!--sidebar-menu-->
	<div id="sidebar">
		<ul>
			<li>
				<a href="${pageContext.request.contextPath}/jsp/index/CommonIndex.jsp">
					<i class="icon icon-home"></i> <span>首页</span>
				</a>
			</li>
			<li>
				<a href="${pageContext.request.contextPath}/jsp/device/SelfDeviceManage.jsp">
					<i class="icon icon-pencil"></i> <span>个人设备管理</span>
				</a>
			</li>
			<li>
				<a href="${pageContext.request.contextPath}/jsp/device/QueryDevice.jsp">
					<i class="icon icon-search"></i> <span>设备查询</span>
				</a>
			</li>
			<li id="addDeviceli" style="display: none;">
				<a href="${pageContext.request.contextPath}/jsp/device/AddDevice.jsp">
					<i class="icon icon-plus-sign"></i> <span>设备入库</span>
				</a>
			</li>
			<li>
				<a href="${pageContext.request.contextPath}/jsp/userInfo/StaffInfo.jsp">
					<i class="icon icon-plus-sign"></i> <span>个人信息</span>
				</a>
			</li>
			
		</ul>
	</div>
	<!--sidebar-menu-->

	<!--close-left-menu-stats-sidebar-->

	<div id="content">
		<div id="content-header">
			<div id="breadcrumb">
				<a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> 首页</a> 
				<a href="#" class="current">个人信息</a>
			</div>
			<h1>个人信息</h1>
		</div>
		<div class="container-fluid">
			<hr>
			<div class="row-fluid">
				<div class="span6">
					<div class="widget-box">
						<div class="widget-title">
							<span class="icon"> <i class="icon-align-justify"></i>
							</span>
							<h5>个人信息 &nbsp;&nbsp;&nbsp;<span style="color:red;">注意：修改个人信息成功后，需要重新登录</span></h5>
						</div>
						<div class="widget-content nopadding">
							<form id="staffInfoFrom" class="form-horizontal">
								<div class="control-group">
									<label class="control-label">员工工号 :</label>
									<div class="controls">
										<input type="text" class="span11" id="staffId" name="staffId" disabled="disabled"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">员工姓名 :</label>
									<div class="controls">
										<input type="text" class="span11" id="staffName" name="staffName" placeholder="请输入员工姓名" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">邮箱 :</label>
									<div class="controls">
										<input type="text" class="span11" id="email" name="email" placeholder="请输入邮箱" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">联系电话 :</label>
									<div class="controls">
										<input type="text" class="span11" id="phoneNumber" name="phoneNumber" placeholder="请输入手机号" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">所属部门管理员 :</label>
									<div class="controls">
										<input type="text" class="span11" id="departManageID" name="departManageID" disabled="disabled"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">所属部门 :</label>
									<div class="controls">
										<input type="text" class="span11" id="departID" name="departID" disabled="disabled"/>
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" class="btn btn-success">保存</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--Footer-part-->
	<div class="row-fluid">
		<div id="footer" class="span12"> 2018 &copy; Fixed Assets Management. Brought to you by <a href="http://themedesigner.in/">Themedesigner.in</a> </div>
	</div>
</body>
</html>
