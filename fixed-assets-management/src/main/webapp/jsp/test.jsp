<%@ page language="java" import="java.util.*,java.net.*" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>测试</title>
</head>
<body>
	<c:if test="${lists!=null }">
		<c:forEach items="${lists }" var="list">
			${ list}<br />
		</c:forEach>
	</c:if>    
	<h3>用户保存的session信息：
	<%=
		session.getAttribute("userSession")
	%>
	</h3>
</body>
</html>
