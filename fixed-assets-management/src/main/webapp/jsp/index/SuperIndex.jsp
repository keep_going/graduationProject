<%@ page language="java" import="java.util.*,java.net.*" contentType="text/html;utf-8" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<title>固定资产管理（超级管理员首页）</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<jsp:include page="../head.jsp"></jsp:include>
<script src="${pageContext.request.contextPath}/jsp/index/superIndex.js"></script>
</head>
<body>
	<jsp:include page="../head3.jsp"></jsp:include>
	<span style="display: none" id="sessionInput"><%=session.getAttribute("userSession")%></span>

	<!--main-container-part-->
	<div id="content">
		<!--breadcrumbs-->
		<div id="content-header">
			<div id="breadcrumb">
				<a href="index.html" title="Go to Home" class="tip-bottom"><i
					class="icon-home"></i> 首页</a>
			</div>
		</div>
		<!--End-breadcrumbs-->

		<!--Action boxes-->
		<div class="container-fluid">
			<div class="quick-actions_homepage">
				<ul class="quick-actions">
					<li class="bg_lb"><a href="#"> <i
							class="icon-dashboard"></i> <span class="label label-important">20</span>
							My Dashboard
					</a></li>
					<li class="bg_lg span3"><a href="#"> <i
							class="icon-signal"></i> Charts
					</a></li>
					<li class="bg_ly"><a href="#"> <i
							class="icon-inbox"></i><span class="label label-success">101</span>
							Widgets
					</a></li>
					<li class="bg_lo"><a href="#"> <i
							class="icon-th"></i> Tables
					</a></li>
					<li class="bg_ls"><a href="#"> <i
							class="icon-fullscreen"></i> Full width
					</a></li>
					<li class="bg_lo span3"><a href="#"> <i
							class="icon-th-list"></i> Forms
					</a></li>
					<li class="bg_ls"><a href="#"> <i
							class="icon-tint"></i> Buttons
					</a></li>
					<li class="bg_lb"><a href="#"> <i
							class="icon-pencil"></i>Elements
					</a></li>
					<li class="bg_lg"><a href="#"> <i
							class="icon-calendar"></i> Calendar
					</a></li>
					<li class="bg_lr"><a href="#"> <i
							class="icon-info-sign"></i> Error
					</a></li>

				</ul>
			</div>
			<!--End-Action boxes-->

			<hr />
		</div>
	</div>

	<!--end-main-container-part-->

	<!--Footer-part-->

	<div class="row-fluid">
		<div id="footer" class="span12"> 2018 &copy; Fixed Assets Management. Brought to you by <a href="http://themedesigner.in/">Themedesigner.in</a> </div>
	</div>
</body>
</html>