
$(document).ready(function(){
	//发送请求的根目录
	var rootDirectory = $("#rootDirectory").val();
	var sessionValue = $("#sessionInput").text();
	sessionValue = Wade.DataMap(sessionValue);
	
	var staffRight = sessionValue.get("staffRight");
	if(typeof staffRight == 'undefined'){
		console.error("staffRight == undefined");
	}else {
		/**
		 * staffRight
		 * 1	入库
		 * 2	领用
		 * 3	流动
		 * 4	报废
		 * 5	维修
		 */
		for(var i=0;i<staffRight.length;i++){
			if(1 == staffRight.items[i]){
				$("#addDeviceli").show();
			}
		}
	}
	
});