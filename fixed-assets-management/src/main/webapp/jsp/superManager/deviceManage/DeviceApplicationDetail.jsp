<%@ page language="java" import="java.util.*,java.net.*" contentType="text/html;utf-8" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<jsp:include page="../../head.jsp"></jsp:include>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/uniform.css" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/datatables.css"/>
	<script src="${pageContext.request.contextPath}/resources/js/dataTable/jquery.dataTables.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/util/dateUtil.js"></script>
	<script src="${pageContext.request.contextPath}/jsp/superManager/deviceManage/deviceApplicationDetail.js" ></script>
	<title>设备申请详情</title>
	<style type="text/css">
		.queryClass {
			float: left;
			width: 120px;
			margin-left:5px;
			margin-top:2.7px; 
		}
	</style>
</head>

<body>
	<jsp:include page="../../head3.jsp"></jsp:include>	
	<input type="hidden" id="rootDirectory" value="${pageContext.request.contextPath}"/>
	<span style="display: none" id="detail">${JSONStr }</span>
	<span style="display: none" id="sessionInput"><%=session.getAttribute("userSession")%></span>
	
	<div id="content">
		<div id="content-header">
			<div id="breadcrumb">
				<a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> 首页</a> 
				<a href="#" class="tip-bottom">设备管理</a> 
				<a href="#" class="current">设备申请管理</a>
			</div>
			<h1>设备申请详情</h1>
		</div>
		
		<div class="container-fluid">
			<hr>
			<div class="row-fluid">
				<div class="span6">
					<div class="widget-box">
						<div class="widget-title">
							<span class="icon"> <i class="icon-align-justify"></i>
							</span>
							<h5>设备申请详情</h5>
						</div>
						<div class="widget-content nopadding">
							<form id="applicationForm" class="form-horizontal">
								<div class="control-group" style="display: none;">
									<label class="control-label">主键:</label>
									<div class="controls">
										<input type="text" class="span11" id="mainID" name="mainID" value="${applicationDetail.mainID}" disabled="disabled"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">设备编码 :</label>
									<div class="controls">
										<input type="text" class="span11" id="assetID" name="assetID" value="${applicationDetail.assetID}" disabled="disabled"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">设备名称 :</label>
									<div class="controls">
										<input type="text" class="span11" id="assetName" name="assetName" value="${applicationDetail.assetName}" disabled="disabled" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">申请数量 :</label>
									<div class="controls">
										<input type="number" class="span11" id="applyNumber" name="applyNumber" value="${applicationDetail.applyNumber}" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">申请人 :</label>
									<div class="controls">
										<input type="text" class="span11" id="applyPerson" name="applyPerson" value="${applicationDetail.applyPerson}" placeholder="请输入申请人" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">申请时间 :</label>
									<div class="controls">
										<input type="date" class="span11" id="applyDate" name="applyDate" placeholder="请输入部门管理员" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">申请原因:</label>
									<div class="controls">
										<textarea class="span11" id="applyReason" name="applyReason"></textarea>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">申请操作:</label>
									<div class="controls">
										<select id="applyState" name="applyState">
											<option value="0">领用申请</option>
											<option value="1">入库申请</option>
											<option value="2">流动申请</option>
											<option value="3">维修申请</option>
											<option value="4">报废申请</option>
										</select>
									</div>
								</div>
								<div class="control-group" id="rukuType" style="display: none;">
									<label class="control-label">入库类型 :</label>
									<div class="controls">
										<select id="deviceInType" class="span11">
											<option value="0">添加设备数量</option>
											<option value="1">新增设备类型</option>
										</select>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">审批人1:</label>
									<div class="controls">
										<input type="text" class="span11" id="approvePesron1" name="approvePesron1" value="${applicationDetail.approvePesron1}" placeholder="请输入部门id"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">审理人2:</label>
									<div class="controls">
										<input type="text" class="span11" id="approvePesron2" name="approvePesron2" value="${applicationDetail.approvePesron2}" placeholder="请输入部门id"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">审批时间:</label>
									<div class="controls">
										<input type="date" class="span11" id="approveDate" name="approveDate" placeholder="请输入部门id"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">审批状态:</label>
									<div class="controls">
										<select id="approveState">
											<option value="0">待审核</option>
											<option value="1">部门管理员通过</option>
											<option value="2">部门管理员不通过</option>
											<option value="3">审批成功</option>
											<option value="4">审批失败</option>
										</select>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">审批备注:</label>
									<div class="controls">
										<textarea class="span11" id="approveRemork" name="approveRemork" value="${applicationDetail.approveRemork}" ></textarea>
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" class="btn btn-success">保存</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--Footer-part-->
	<div class="row-fluid">
  		<div id="footer" class="span12"> 2018 &copy; Fixed Assets Management. Brought to you by <a href="http://themedesigner.in/">Themedesigner.in</a> </div>
	</div>
	<!--end-Footer-part-->
</body>
</html>