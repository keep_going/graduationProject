
$(document).ready(function(){
	//发送请求的根目录
	var rootDirectory = $("#rootDirectory").val();
	//获取session中的信息 根据员工权限显示 操作按钮
	var sessionValue = $("#sessionInput").text();
	sessionValue = Wade.DataMap(sessionValue);
	if(sessionValue.length<1){
		alert("登录超时，请重新登录");
		window.location.href = rootDirectory+"/jsp/login/Login.jsp";
		return;
	}
	//获取设备类别
	getDeviceTypeParam(rootDirectory);
	//判断输入的设备编码是否已存在
	$("#assetID").bind("blur",function(){
		var param = new Wade.DataMap();
		param.put("assetID",$("#assetID").val());
		Common.callSvc(rootDirectory+"/assetsInfo/getAssetInfoByID.do",param,function(resultData){
			resultData = Wade.DataMap(resultData);
			console.log(resultData.toString());
			var resultJson = resultData.get('resultJson');
			if("0" == resultData.get("msgFlag")){
				if(resultJson.length>0){
					alert("该设备编码已存在，请重新输入一个");
					$("#assetID").val("");
				}else{
					return;
				}
			}else{
				alert(resultData.get("errorMsg"));
			}
		});
	});
	
	
	//表单校验 是根据name属性 不是id
	$("#deviceInfoForm").validate({
		rules: {
			assetID:"required",
			assetName: "required",
			manufacture: "required",
			inPerson: "required",
			inDate: "required",
			class_id: "required",
			total_inventory: "required"
		},
		messages: {
			assetID:"设备编码不能为空",
			assetName: "设备不能为空",
			manufacture: "生产厂家不能为空",
			inPerson: "入库人不能为空",
			inDate: "入库时间不能为空",
			class_id: "设备类别不能为空",
			total_inventory: "总数量不能为空"
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
		$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
		},
		//按钮提交事件
		submitHandler:function(form){
			$.ajax({
                url: rootDirectory+"/assetsInfo/addAssetInfo.do",
                type: "post",
                data: {
                	"assetID": $("#assetID").val(),
                	"assetName": $("#assetName").val(),
                	"manufacture": $("#manufacture").val(),
                	"remark": $("#remark").val(),
                	"inPerson": $("#inPerson").val(),
                	"class_id": $("#class_id").val(),
                	"inDate": $("#inDate").val(),
                	"total_inventory": $("#total_inventory").val()
                	
                }, success: function (resultData) {
                	resultData = Wade.DataMap(resultData);
        			console.log(resultData.toString());
        			if("0" == resultData.get("msgFlag")){
        				alert("添加成功！");
        				//跳转到登录页面
        				window.location.href = rootDirectory+"/jsp/index/SuperIndex.jsp";
        			}else{
        				alert(resultData.get("errorMsg"));
        			}
                }, error: function () {
                	alert("添加失败");
                }
            });
		}
	});
});

/**
 * 获取设备类型参数
 * @returns
 */
function getDeviceTypeParam(rootDirectory){
	Common.callSvc(rootDirectory+"/assetClass/getAssetClassList.do",null,function(resultData){
		resultData = Wade.DataMap(resultData);
		if("0" === resultData.get("msgFlag")){
			console.log(resultData.toString());
			var resultJson = resultData.get('resultJson');
			var assetClasses = resultJson.get('assetClass');
			
			for(var i=0;i<assetClasses.items.length;i++){
				jQuery("#class_id").append('<option value='+assetClasses.items[i].get("classID")+'> '+assetClasses.items[i].get("className")+'</option>');
			}
		}else{
			alert(resultData.get("errorMsg"));
		}
	});
}
