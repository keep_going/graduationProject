
$(document).ready(function(){
	//发送请求的根目录
	var rootDirectory = $("#rootDirectory").val();
	//初始化设备类别列表
	initAssetClass(rootDirectory);
	//获取session中的信息 根据员工权限显示 操作按钮
	var sessionValue = $("#sessionInput").text();
	sessionValue = Wade.DataMap(sessionValue);
	//搜索按钮
	$("#searchBtn").click(function(){
		var assetName = $.trim($("#queryInput").val());
		var assetClass = $("#deviceType").val();
		//加载表格
		dataStart(assetClass,rootDirectory);
	});
	//新增
	$("#addBtn").click(function(){
		//跳到新增页面
		window.location.href = rootDirectory+"/jsp/superManager/deviceManage/DeviceInfoAdd.jsp";
	});
});
	

/**
 * 初始化 设备类别
 * @param rootDirectory
 * @returns
 */
function initAssetClass(rootDirectory){
	//调用接口为设备类型赋值
	Common.callSvc(rootDirectory+"/assetClass/getAssetClassList.do",null,function(resultData){
		resultData = Wade.DataMap(resultData);
		if("0" === resultData.get("msgFlag")){
			console.log(resultData.toString());
			var resultJson = resultData.get('resultJson');
			var assetClasses = resultJson.get('assetClass');
			
			for(var i=0;i<assetClasses.items.length;i++){
				jQuery("#deviceType").append('<option value='+assetClasses.items[i].get("classID")+'> '+assetClasses.items[i].get("className")+'</option>');
			}
		}else{
			alert(resultData.get("errorMsg"));
		}
	});
}
/**
 * dataTable插件
 * @param assetName
 * @param assetClass
 * @param rootDirectory
 * @returns
 */
function dataStart(assetClass,rootDirectory){
	
	var table = $('.data-table').dataTable({
		"searching":true,//禁用搜索
		"bLengthChange": true,//屏蔽tables的一页展示多少条记录的下拉列表
        "pagingType": "full_numbers",
        // 每次创建是否销毁以前的DataTable,默认false
        "destroy": true,
        // 是否显示情报 就是"当前显示1/100记录"这个信息
        "info": false,
		"ajax": {
			url:rootDirectory+"/assetsInfo/getAllAssetsInfo.do",
			type: "POST",
            data:{
            	class_id:assetClass
            },
            dataSrc: "resultJson"
           	
        },
		"columns":[
			{
				"data":"assetID",
			},{
				"data":"assetName",
			},{
				"data":"manufacture",
			},{
				"data":"inventory",
			},{
				"data":"class_id",
			},{
				"data":"remark",
			},{
				"data":"inPerson",
			},{
				"data":"inDate",
			},{
				"data":"total_inventory",
			},{
				"data":null,
				"render":function(data, type, full, meta) {
					return "<div class='btn-group'>"
        						+"<button data-toggle='dropdown' class='btn btn-info dropdown-toggle'>操作 <span class='caret'></span></button>"
        						+"<ul class='dropdown-menu'>"
        							+"<li class='li1' ><a href='#' onclick='modifyRow(this," + JSON.stringify(full) + ")'>修改</a></li>"
        							+"<li class='li1' ><a href='#' onclick='deleteRow(this," + JSON.stringify(full) + ")'>删除</a></li>"
        							+"<li class='li1' ><a href='#' onclick='showChart(" + JSON.stringify(full) + ")'>使用率</a></li>"
        						+"</ul>"
        					+"</div>";
				}
			}
		],
		
//		//列格式化(只能存在一个)l
//		"columnDefs":[{
//			targets:7,
//			render:function(data, type, row, meta){
//				alert(staffRight);
//				for(var i=0;i<staffRight.length;i++){
//					if(2 == staffRight.items[i]){
//						$(".li2").show();
//					}else if(3 == staffRight.items[i]){
//						$(".li3").show();
//					}else if(4 == staffRight.items[i]){
//						$(".li4").show();
//					}else if(5 == staffRight.items[i]){
//						$(".li5").show();
//					}
//				}
//			}
//		}]
	});
}
//修改
function modifyRow(obj,full){
	var url = "/fixed-assets-management/assetsInfo/queryAssetInfo.do?assetID="+full.assetID;
	window.location.href=url;
}
//删除
function deleteRow(obj,full){
	var param = Wade.DataMap();
	var assetID = full.assetID;
	if(assetID == "" || typeof assetID == 'undefined'){
		alert("mainID不能为空");
		return;
	}
	if(confirm("确定要删除吗？")){
		param.put("assetID",assetID);
//		Common.callSvc("/fixed-assets-management/assetApply/deleteOneApplication.do",param,function(resultData){
//			resultData = Wade.DataMap(resultData);
//			if("0" == resultData.get("msgFlag")){
				$(obj).parents("tr").remove();
				alert("删除成功！");
//				return;
//			}else{
//				alert(resultData.get("errorMsg"));
//			}
//		});
	}
}
//调整到设备使用率的图表
function showChart(full){
	var url = "/fixed-assets-management/jsp/superManager/deviceAnalyticStatistics/DeviceRate.jsp?assetID="+full.assetID;
	window.location.href=url;
}


