<%@ page language="java" import="java.util.*,java.net.*" contentType="text/html;utf-8" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<jsp:include page="../../head.jsp"></jsp:include>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/uniform.css" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/datatables.css"/>
	<script src="${pageContext.request.contextPath}/resources/js/dataTable/jquery.dataTables.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/util/dateUtil.js"></script>
	<script src="${pageContext.request.contextPath}/jsp/superManager/deviceManage/deviceInfoDetail.js" ></script>
	<title>设备管理详情(超管)</title>
	<style type="text/css">
		.queryClass {
			float: left;
			width: 120px;
			margin-left:5px;
			margin-top:2.7px; 
		}
	</style>
</head>

<body>
	<jsp:include page="../../head3.jsp"></jsp:include>	
	<input type="hidden" id="rootDirectory" value="${pageContext.request.contextPath}"/>
	<span style="display: none" id="detail">${JSONStr }</span>
	<span style="display: none" id="sessionInput"><%=session.getAttribute("userSession")%></span>
	
	<div id="content">
		<div id="content-header">
			<div id="breadcrumb">
				<a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> 首页</a> 
				<a href="#" class="tip-bottom">用户管理</a> 
				<a href="#" class="current">设备管理</a>
			</div>
			<h1>设备信息管理</h1>
		</div>
		
		<div class="container-fluid">
			<hr>
			<div class="row-fluid">
				<div class="span6">
					<div class="widget-box">
						<div class="widget-title">
							<span class="icon"> <i class="icon-align-justify"></i>
							</span>
							<h5>设备信息管理</h5>
						</div>
						<div class="widget-content nopadding">
							<form id="deviceInfoForm" class="form-horizontal">
								<div class="control-group">
									<label class="control-label">设备id :</label>
									<div class="controls">
										<input type="text" class="span11" id="assetID" name="assetID" value="${assetInfo.assetID}" disabled="disabled"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">设备名称 :</label>
									<div class="controls">
										<input type="text" class="span11" id="assetName" name="assetName" value="${assetInfo.assetName}" placeholder="设备名称" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">生产厂家 :</label>
									<div class="controls">
										<input type="text" class="span11" id="manufacture" name="manufacture" value="${assetInfo.manufacture}" placeholder="生产厂家" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">库存数量 :</label>
									<div class="controls">
										<input type="number" class="span11" id="inventory" name="inventory" value="${assetInfo.inventory}"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">入库人 :</label>
									<div class="controls">
										<input type="text" class="span11" id="inPerson" name="inPerson" value="${assetInfo.inPerson}" placeholder="入库人" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">入库时间 :</label>
									<div class="controls">
										<input type="date" class="span11" id="inDate" name="inDate" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">设备类别:</label>
									<div class="controls">
										<input type="text" class="span11" id="class_id" name="class_id" value="${assetInfo.class_id}" placeholder="设备类别"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">总数量 :</label>
									<div class="controls">
										<input type="number" class="span11" id="total_inventory" name="total_inventory" value="${assetInfo.total_inventory}" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">备注描述 :</label>
									<div class="controls">
										<input type="number" class="span11" id="remark" name="remark" value="${assetInfo.remark}" placeholder="备注"/>
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" class="btn btn-success">保存</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--Footer-part-->
	<div class="row-fluid">
  		<div id="footer" class="span12"> 2018 &copy; Fixed Assets Management. Brought to you by <a href="http://themedesigner.in/">Themedesigner.in</a> </div>
	</div>
	<!--end-Footer-part-->
</body>
</html>