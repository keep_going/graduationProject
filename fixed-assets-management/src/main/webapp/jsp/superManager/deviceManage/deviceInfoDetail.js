
$(document).ready(function(){
	//发送请求的根目录
	var rootDirectory = $("#rootDirectory").val();
	//获取session中的信息 根据员工权限显示 操作按钮
	var sessionValue = $("#sessionInput").text();
	sessionValue = Wade.DataMap(sessionValue);
	if(sessionValue.length<1){
		alert("登录超时，请重新登录");
		window.location.href = rootDirectory+"/jsp/login/Login.jsp";
		return;
	}
	//获取
	var deviceDetail = $("#detail").text();
	//页面赋值
	initDetailForm(deviceDetail);
	
	
	
	//表单校验 是根据name属性 不是id
	$("#deviceInfoForm").validate({
		rules: {
			assetName: "required",
			manufacture: "required",
			inventory: "required",
			inPerson: "required",
			inDate: "required",
			class_id: "required",
			total_inventory: "required"
		},
		messages: {
			assetName: "设备不能为空",
			manufacture: "生产厂家不能为空",
			inventory: "库存数量不能为空",
			inPerson: "入库人不能为空",
			inDate: "入库时间不能为空",
			class_id: "设备类别不能为空",
			total_inventory: "总数量不能为空"
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
		$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
		},
		//按钮提交事件
		submitHandler:function(form){
			$.ajax({
                url: rootDirectory+"/assetsInfo/updateAssetInfo.do",
                type: "post",
                data: {
                	"assetID": $("#assetID").val(),
                	"assetName": $("#assetName").val(),
                	"manufacture": $("#manufacture").val(),
                	"inventory": $("#inventory").val(),
                	"remark": $("#remark").val(),
                	"inPerson": $("#inPerson").val(),
                	"class_id": $("#class_id").val(),
                	"inDate": $("#inDate").val(),
                	"total_inventory": $("#total_inventory").val()
                	
                }, success: function (resultData) {
                	resultData = Wade.DataMap(resultData);
        			console.log(resultData.toString());
        			if("0" == resultData.get("msgFlag")){
        				alert("信息修改成功！");
        				//跳转到登录页面
        				window.location.href = rootDirectory+"/jsp/index/SuperIndex.jsp";
        			}else{
        				alert(resultData.get("errorMsg"));
        			}
                }, error: function () {
                	alert("信息修改成功");
                }
            });
		}
	});
});
//表单赋值
function initDetailForm(deviceDetail){
	var info = Wade.DataMap(deviceDetail);
	var detail = info.get('detail');
	
	var inDate = detail.get('inDate');
	if(!(typeof inDate == 'undefined' || inDate == null)){
		inDate = new Date(Number(inDate)).Format("yyyy-MM-dd");
	}else {
		inDate = "";
	}
	$("#inDate").val(inDate);
}