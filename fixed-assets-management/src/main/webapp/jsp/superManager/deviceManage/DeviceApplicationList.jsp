<%@ page language="java" import="java.util.*,java.net.*" contentType="text/html;utf-8" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<jsp:include page="../../head.jsp"></jsp:include>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/uniform.css" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/datatables.css"/>
	<script src="${pageContext.request.contextPath}/resources/js/dataTable/jquery.dataTables.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/util/dateUtil.js"></script>
	<script src="${pageContext.request.contextPath}/jsp/superManager/deviceManage/deviceApplicationList.js" ></script>
	<title>设备申请管理(超管)</title>
	<style type="text/css">
		.queryClass {
			float: left;
			width: 120px;
			margin-left:5px;
			margin-top:2.7px; 
		}
	</style>
</head>

<body>
	<jsp:include page="../../head3.jsp"></jsp:include>	
	<input type="hidden" id="rootDirectory" value="${pageContext.request.contextPath}"/>
	<span style="display: none" id="sessionInput"><%=session.getAttribute("userSession")%></span>
	
	<div id="content">
		<div id="content-header">
			<div id="breadcrumb">
				<a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> 首页</a> 
				<a href="#" class="tip-bottom">设备管理</a> 
				<a href="#" class="current">设备申请管理</a>
			</div>
			<h1>设备申请管理</h1>
		</div>
		
		<div class="container-fluid">
			<hr>
			<div class="row-fluid">
				<div class="span12">
       				<div class="widget-box">
       					<!-- 标题 -->
       					<div class="widget-title" style="background-color: lightblue"> <span class="icon"> <i class="icon-th"></i> </span>
            				<h5>Data table</h5>
            				<select id="applyType" class="queryClass">
								<option value="" selected="selected">---申请类别---</option>
								<option value="0">领用申请</option>
								<option value="1">入库申请</option>
								<option value="2">流动申请</option>
								<option value="3">维修申请</option>
								<option value="4">报废申请</option>
							</select>
            				<button class="btn btn-info" style="float: right;margin: 2.7px 5px 0 0;" id="searchBtn">搜索</button>
          				</div>
          				<!-- 内容 -->
		          		<div class="widget-content nopadding">
		            		<table class="table table-bordered data-table">
		              			<thead>
		                			<tr>
		                  				<th>设备编码</th>
		                  				<th>设备名称</th>
		                  				<th>申请数量</th>
		                  				<th>申请人</th>
		                  				<th>申请时间</th>
		                  				<th>申请原因</th>
		                  				<th>申请状态</th>
		                  				<th>审批人1</th>
		                  				<th>审批人2</th>
		                  				<th>审批状态</th>
		                  				<th>操作</th>
		                			</tr>
		              			</thead>
		              		</table>
		              	</div>
       				</div>
       			</div>
			</div>
		</div>
	</div>
	<!--Footer-part-->
	<div class="row-fluid">
  		<div id="footer" class="span12"> 2018 &copy; Fixed Assets Management. Brought to you by <a href="http://themedesigner.in/">Themedesigner.in</a> </div>
	</div>
	<!--end-Footer-part-->
</body>
</html>