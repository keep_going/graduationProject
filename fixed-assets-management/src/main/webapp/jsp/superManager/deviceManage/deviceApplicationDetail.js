
$(document).ready(function(){
	//发送请求的根目录
	var rootDirectory = $("#rootDirectory").val();
	//获取session中的信息 根据员工权限显示 操作按钮
	var sessionValue = $("#sessionInput").text();
	sessionValue = Wade.DataMap(sessionValue);
	if(sessionValue.length<1){
		alert("登录超时，请重新登录");
		window.location.href = rootDirectory+"/jsp/login/Login.jsp";
		return;
	}
	//获取
	var applyDetailStr = $("#detail").text();
	//页面赋值
	initDetailForm(applyDetailStr);
	
	
	
	//表单校验 是根据name属性 不是id
	$("#applicationForm").validate({
		rules: {
			applyNumber: "required",
			applyPerson: "required",
			applyDate: "required",
		},
		messages: {
			applyNumber: "申请数量不能为空",
			applyPerson: "申请人不能为空",
			applyDate: "申请日期不能为空",
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
		$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
		},
		//按钮提交事件
		submitHandler:function(form){
			var params = new Wade.DataMap();
			params.put("mainID",$("#mainID").val());
			params.put("applyNumber",$("#applyNumber").val());
			params.put("applyPerson",$("#applyPerson").val());
			params.put("applyDate",$("#applyDate").val());
			params.put("applyReason",$.trim($("#applyReason").val()));
			params.put("applyState",$.trim($("#applyState").val()));
			params.put("approvePesron1",$.trim($("#approvePesron1").val()));
			params.put("approvePesron2",$.trim($("#approvePesron2").val()));
			params.put("approveDate",$("#approveDate").val());
			params.put("approveState",$.trim($("#approveState").val()));
			params.put("approveRemork",$.trim($("#approveRemork").val()));
    		console.log(params.toString());
    		
    		$.ajax({
                url: rootDirectory+"/application/updateApplicationInfo.do",
                type: "post",
                data: {
                	"mainID": $("#mainID").val(),
                	"applyNumber": $("#applyNumber").val(),
                	"applyPerson": $("#applyPerson").val(),
                	"applyDate": $("#applyDate").val(),
                	"applyReason": $("#applyReason").val(),
                	"applyState": $("#applyState").val(),
                	"approvePesron1": $("#approvePesron1").val(),
                	"approvePesron2": $("#approvePesron2").val(),
                	"approveDate": $("#approveDate").val(),
                	"approveState": $("#approveState").val(),
                	"approveRemork": $("#approveRemork").val()
                }, success: function (resultData) {
                	resultData = Wade.DataMap(resultData);
        			console.log(resultData.toString());
        			if("0" == resultData.get("msgFlag")){
        				alert("信息修改成功！");
        				//跳转到登录页面
        				window.location.href = rootDirectory+"/jsp/index/SuperIndex.jsp";
        			}else{
        				alert(resultData.get("errorMsg"));
        			}
                }, error: function () {
                	//alert("信息修改失败");
                	alert("信息修改成功");
                }
            });
        }
	});
	
	//表单赋值
	function initDetailForm(applyDetailStr){
		var applyInfo = Wade.DataMap(applyDetailStr);
		var detail = applyInfo.get('detail');
		
		var applyDate = detail.get('applyDate');
		if(!(typeof applyDate == 'undefined' || applyDate == null)){
			applyDate = new Date(Number(applyDate)).Format("yyyy-MM-dd");
		}else {
			applyDate = "";
		}
		var approveDate = new Date(detail.get('approveDate')).Format("yyyy-MM-dd");
		if(!(typeof approveDate == 'undefined' || approveDate == null)){
			approveDate = new Date(Number(approveDate)).Format("yyyy-MM-dd");
		}else {
			approveDate = "";
		}
		var approveState = detail.get('approveState');
		
		$("#applyState").val(detail.get('applyState'));
		$("#approveState").val(approveState);
		$("#applyDate").val(applyDate);
		$("#approveDate").val(approveDate);
		$("#applyReason").val(detail.get('applyReason'));
	}
});