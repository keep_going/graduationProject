
$(document).ready(function(){
	//发送请求的根目录
	var rootDirectory = $("#rootDirectory").val();
	var sessionValue = $("#sessionInput").text();
	sessionValue = Wade.DataMap(sessionValue);
	if(sessionValue.length<1){
		alert("登录超时，请重新登录");
		window.location.href = rootDirectory+"/jsp/login/Login.jsp";
	}
	//用户信息
	var userInfo = sessionValue.get("userInfo");
	
	
	//查询
	$("#searchBtn").click(function(){
		var applyType = $("#applyType").val();
		//生成表单
		dataTableInit(applyType,rootDirectory);
	});
});
function dataTableInit(applyType,rootDirectory){
	var table = $('.data-table').dataTable({
		"pagingType": "full_numbers",
		"aLengthMenu": [5, 10, 15, 20],//设置一页展示5条记录
		"bServerSide": true,//这个用来指明是通过服务端来取数据
		"info": true,
		// 每次创建是否销毁以前的DataTable,默认false
        "destroy": true,
		"ajax": {
			url:rootDirectory+"/assetApply/getAllAssetApplication.do",
			type: "POST",
			data:{
//				startTime:startTime,
//				endTime:endTime,
				applyState:applyType
			},
            dataSrc: "resultJson"
        },
        "columns":[
        	{
        		"data":"assetID",
        	},{
        		"data":"assetName",
        	},{
        		"data":"applyNumber",
        	},{
        		"data":"applyPerson",
        	},{
        		"data":"applyDate",
        		"render":function(data,type,full,meta){
        			data = new Date(data).Format("yyyy-MM-dd");
        			return data;
        		}
        	},{
        		"data":"applyReason",
        	},{
        		"data":"applyState",
        		"render":function(data, type, full, meta){
        			if(data == 0){
        				data = "领用申请";
        			}else if(data == 1){
        				data = "入库申请";
        			}else if(data == 2){
        				data = "流动申请";
        			}else if(data == 3){
        				data = "维修申请";
        			}else if(data == 4){
        				data = "报废申请";
        			}else{
        				data = "无";
        			}
        			return data;
        		}
        	},{
        		"data":"approvePesron1"
        	},{
        		"data":"approvePesron2",
        		"render":function(data, type, full, meta){
        			if(!data || typeof data == 'underfined'){
        				data = "";
        			}
        			return data;
        		}
        	},{
        		"data":"approveState",
        		"render":function(data, type, full, meta){
        			if(data == 0){
        				data = "待审核";
        			}else if(data == 1){
        				data = "部门管理员通过";
        			}else if(data == 2){
        				data = "部门管理员不通过";
        			}else if(data == 3){
        				data = "审批成功";
        			}else if(data == 4){
        				data = "审批失败";
        			}else{
        				data = "无";
        			}
        			return data;
        		}
        	},{
				"data":null,
				"render":function(data, type, full, meta) {
					return "<div class='btn-group'>"
		            			+"<button data-toggle='dropdown' class='btn btn-info dropdown-toggle'>操作 <span class='caret'></span></button>"
		            			+"<ul class='dropdown-menu'>"
		            				+"<li class='li1' ><a href='#' onclick='modifyRow(this," + JSON.stringify(full) + ")'>修改</a></li>"
		            				+"<li class='li1' ><a href='#' onclick='deleteRow(this," + JSON.stringify(full) + ")'>删除</a></li>"
		            			+"</ul>"
		            		+"</div>";
				}
        	}
        ]
	});
}
//比较两个日期的大小
function CompareDate(d1,d2){
  return ((new Date(d1.replace(/-/g,"\/"))) <= (new Date(d2.replace(/-/g,"\/"))));
}
//修改
function modifyRow(obj,full){
	var url = "/fixed-assets-management/assetApply/getApplicationDeatil.do?id="+full.mainID;
	window.location.href=url;
}
//删除
function deleteRow(obj,full){
	var param = Wade.DataMap();
	var mainID = full.mainID;
	if(mainID == "" || typeof mainID == 'undefined'){
		alert("mainID不能为空");
		return;
	}
	if(confirm("确定要删除吗？")){
		param.put("mainID",mainID);
		Common.callSvc("/fixed-assets-management/assetApply/deleteOneApplication.do",param,function(resultData){
			resultData = Wade.DataMap(resultData);
			if("0" == resultData.get("msgFlag")){
				$(obj).parents("tr").remove();
				alert("删除成功！");
				return;
			}else{
				alert(resultData.get("errorMsg"));
			}
		});
	}
}
