
$(document).ready(function(){
	//发送请求的根目录
	var rootDirectory = $("#rootDirectory").val();
	//获取session中的信息 根据员工权限显示 操作按钮
	var sessionValue = $("#sessionInput").text();
	sessionValue = Wade.DataMap(sessionValue);
	if(sessionValue.length<1){
		alert("登录超时，请重新登录");
		window.location.href = rootDirectory+"/jsp/login/Login.jsp";
	}
	
	//表单校验 是根据name属性 不是id
	$("#dmForm").validate({
		rules: {
			managerId: "required",
			managerName: "required",
			departId: "required",
			email: "required",
			superId: "required",
		},
		messages: {
			managerId: "部门管理员工号不能为空",
			managerName: "部门管理员姓名不能为空",
			departId: "部门编码不能为空",
			email: "邮箱不能为空",
			superId: "上级工号不能为空",
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
		$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
		},
		//按钮提交事件
		submitHandler:function(form){
    		var param = new Wade.DataMap();
    		param.put("managerID",$.trim($("#managerId").val()));
    		param.put("managerName",$.trim($("#managerName").val()));
    		param.put("departID",$.trim($("#departId").val()));
    		param.put("contactPhone",$.trim($("#contactPhone").val()));
    		param.put("email",$.trim($("#email").val()));
    		param.put("superID",$.trim($("#superId").val()));
    		console.log(param.toString());
    		//发送请求
    		Common.callSvc(rootDirectory+"/departManager/updateDMInfo.do",param,function(resultData){
    			resultData = Wade.DataMap(resultData);
    			console.log(resultData.toString());
    			if("0" === resultData.get("msgFlag")){
    				alert("信息修改成功！");
    				//跳转到首页
    				window.location.href = rootDirectory+"/jsp/index/SuperIndex.jsp";
    			}else{
    				alert(resultData.get("errorMsg"));
    			}
    		});
        }
	});
	
});


