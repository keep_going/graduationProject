<%@ page language="java" import="java.util.*,java.net.*" contentType="text/html;utf-8" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<jsp:include page="../../head.jsp"></jsp:include>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/uniform.css" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/datatables.css"/>
	<script src="${pageContext.request.contextPath}/resources/js/dataTable/jquery.dataTables.min.js"></script>
	<script src="${pageContext.request.contextPath}/jsp/superManager/userManage/departManagerInfoDetail.js" ></script>
	<title>部门管理员信息管理</title>
	<style type="text/css">
		.queryClass {
			float: left;
			width: 120px;
			margin-left:5px;
			margin-top:2.7px; 
		}
	</style>
</head>

<body>
	<jsp:include page="../../head3.jsp"></jsp:include>	
	<input type="hidden" id="rootDirectory" value="${pageContext.request.contextPath}"/>
	<span style="display: none" id="sessionInput"><%=session.getAttribute("userSession")%></span>
	
	<div id="content">
		<div id="content-header">
			<div id="breadcrumb">
				<a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> 首页</a> 
				<a href="#" class="tip-bottom">用户管理</a> 
				<a href="#" class="current">部门管理员信息管理</a>
			</div>
			<h1>部门管理员信息管理</h1>
		</div>
		
		<div class="container-fluid">
			<hr>
			<div class="row-fluid">
				<div class="span6">
					<div class="widget-box">
						<div class="widget-title">
							<span class="icon"> <i class="icon-align-justify"></i>
							</span>
							<h5>部门管理员详情</h5>
						</div>
						<div class="widget-content nopadding">
							<form id="dmForm" class="form-horizontal">
								<div class="control-group">
									<label class="control-label">部门管理员工号 :</label>
									<div class="controls">
										<input type="text" class="span11" id="managerId" name="managerId" value="${dmBean.managerID}" disabled="disabled"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">部门管理员姓名 :</label>
									<div class="controls">
										<input type="text" class="span11" id="managerName" name="managerName" value="${dmBean.managerName}" placeholder="请输入部门管理员姓名" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">部门ID :</label>
									<div class="controls">
										<input type="text" class="span11" id="departId" name="departId" value="${dmBean.departID}" placeholder="请输入部门ID" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">联系电话 :</label>
									<div class="controls">
										<input type="text" class="span11" id="contactPhone" name="contactPhone" value="${dmBean.contactPhone}" placeholder="请输入部门ID" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">邮箱 :</label>
									<div class="controls">
										<input type="text" class="span11" id="email" name="email" value="${dmBean.email}" placeholder="请输入邮箱" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">所属上级 :</label>
									<div class="controls">
										<input type="text" class="span11" id="superId" name="superId" value="${dmBean.superID}" disabled="disabled"/>
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" class="btn btn-success">提交</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--Footer-part-->
	<div class="row-fluid">
  		<div id="footer" class="span12"> 2018 &copy; Fixed Assets Management. Brought to you by <a href="http://themedesigner.in/">Themedesigner.in</a> </div>
	</div>
	<!--end-Footer-part-->
</body>
</html>