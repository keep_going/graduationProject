
$(document).ready(function(){
	//发送请求的根目录
	var rootDirectory = $("#rootDirectory").val();
	
	//获取session中的信息 根据员工权限显示 操作按钮
	var sessionValue = $("#sessionInput").text();
	sessionValue = Wade.DataMap(sessionValue);
	if(sessionValue.length<1){
		alert("登录超时，请重新登录");
		window.location.href = rootDirectory+"/jsp/login/Login.jsp";
	}
	
	//搜索按钮
	$("#searchBtn").click(function(){
		var commonName = $.trim($("#queryInput").val());
		//加载表格
		dataStart(commonName,rootDirectory);
	});
});
	
/**
 * dataTable插件
 * @param commonName
 * @param rootDirectory
 * @returns
 */
function dataStart(commonName,rootDirectory){
	
	var table = $('.data-table').dataTable({
		"searching":true,//禁用搜索
		"bLengthChange": true,//屏蔽tables的一页展示多少条记录的下拉列表
        "pagingType": "full_numbers",
        // 每次创建是否销毁以前的DataTable,默认false
        "destroy": true,
        // 是否显示情报 就是"当前显示1/100记录"这个信息
        "info": false,
		"ajax": {
			url:rootDirectory+"/staff/getAllStaffInfo.do",
			type: "POST",
            data:{
            	staffName:commonName
            },
            dataSrc: "resultJson"
           	
        },
		"columns":[
			{
				"data":"staffID",
			},{
				"data":"staffName",
			},{
				"data":"email",
			},{
				"data":"contactPhone",
			},{
				"data":"departID",
			},{
				"data":"roleID",
			},{
				"data":"higherupID",
			},{
				"data":null,
				"render":function(data, type, full, meta) {
					return "<div class='btn-group'>"
        						+"<button data-toggle='dropdown' class='btn btn-info dropdown-toggle'>操作 <span class='caret'></span></button>"
        						+"<ul class='dropdown-menu'>"
        							+"<li class='li1' ><a href='#' onclick='deleteCommon(this," + JSON.stringify(full) + ")'>删除</a></li>"
        							+"<li class='li1' ><a href='#' onclick='modifyStaffInfo(" + JSON.stringify(full) + ")'>修改</a></li>"
        						+"</ul>"
        					+"</div>";
				}
			}
		],
		
//		//列格式化(只能存在一个)l
//		"columnDefs":[{
//			targets:7,
//			render:function(data, type, row, meta){
//				alert(staffRight);
//				for(var i=0;i<staffRight.length;i++){
//					if(2 == staffRight.items[i]){
//						$(".li2").show();
//					}else if(3 == staffRight.items[i]){
//						$(".li3").show();
//					}else if(4 == staffRight.items[i]){
//						$(".li4").show();
//					}else if(5 == staffRight.items[i]){
//						$(".li5").show();
//					}
//				}
//			}
//		}]
	});
}
//删除员工
function deleteCommon(obj,full){
	var param = Wade.DataMap();
	var staffId = $.trim(full.staffID);
	console.log(staffId);
	if(staffId == "" || typeof staffId == 'undefined'){
		alert("普通员工工号不能为空");
		return;
	}
	if(confirm("确定要删除吗？")){
		param.put("staffID",staffId);
		Common.callSvc("/fixed-assets-management/staff/doDeleteStaffInfo.do",param,function(resultData){
			if("0" == resultData.msgFlag){
				$(obj).parents("tr").remove();
				alert("删除成功！");
				return;
			}else{
				alert(resultData.errorMsg);
			}
		});
	}
}

function modifyStaffInfo(full){
	var url = "/fixed-assets-management/staff/getStaffInfoByStaffId.do?id="+full.staffID;
	window.location.href=url;
}
