
$(document).ready(function(){
	//发送请求的根目录
	var rootDirectory = $("#rootDirectory").val();
	
	//定义各个状态的数量
	var lingyong = 0;
	var weixiu = 0;
	var baofei = 0;
	var totalNumber = 0;
	//定义各个状态的利用率
	var lyRate = 0.0;
	var wxRate = 0.0;
	var bfRate = 0.0;
	var otherRate = 0.0;
	
	var param = Wade.DataMap();
	var assetId = $("#ASSETID").val();
	param.put("assetId",assetId);
	Common.callSvc(rootDirectory+"/chartsData/getAssetRate.do",param,function(resultData){
		resultData = Wade.DataMap(resultData);
		if("0" == resultData.get("msgFlag")){
			var resultJson = resultData.get("resultJson");
			resultJson.each(function(value,index){
				totalNumber = value.get('totalInventory');
				if("0" == value.get('applyState')){
					lingyong += value.get('applyNumber');
				}
				if("3" == value.get('applyState')){
					weixiu += value.get('applyNumber');
				}
				if("4" == value.get('applyState')){
					bfRate += value.get('applyNumber');
				}
				//计算状态利用率
				lyRate = lingyong/totalNumber;
				wxRate = weixiu/totalNumber;
				bfRate = bfRate/totalNumber;
				otherRate = (totalNumber-bfRate-weixiu-lingyong)/totalNumber;
				
				initChart(lyRate,wxRate,bfRate,otherRate);
			});
		}else{
			alert(resultData.get("errorMsg"));
		}
	});
	/**
	 * @param lyRate 领用率
	 * @param wxRate 维修率
	 * @param bfRate 报废率
	 * @param otherRate 其他
	 */
	function initChart(lyRate,wxRate,bfRate,otherRate){
		var chart = Highcharts.chart('container', {
			title: {
				text: '设备使用率'
			},
			tooltip: {
				headerFormat: '{series.name}<br>',
				pointFormat: '{point.name}: <b>{point.percentage:.1f}%</b>'
			},
			credits: { 
		    	enabled: false 
		    },
			plotOptions: {
				pie: {
					allowPointSelect: true,  // 可以被选择
					cursor: 'pointer',       // 鼠标样式
					dataLabels: {
						enabled: true,
						format: '<b>{point.name}</b>: {point.percentage:.1f} %',
						style: {
								color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
							}
					}
				}
			},
			series: [{
				type: 'pie',
				name: '使用率占比',
				data: [
					['领用',   lyRate],
					['维修',       wxRate],
					{
						name: '其他',
						y: otherRate,
						sliced: true,  // 默认突出
						selected: true // 默认选中 
					},
					['报废',    bfRate]
				]
			}]
		});
	}
});

