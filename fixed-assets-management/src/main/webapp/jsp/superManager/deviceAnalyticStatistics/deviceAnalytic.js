
$(document).ready(function(){
	//发送请求的根目录
	var rootDirectory = $("#rootDirectory").val();
	/*
	 * 定于数组 接收数据
	 */
	//设备id
	var assetIDs = [];
	//领用申请的数量
	var lingyong = [];
	//入库申请的数量
	var ruku = [];
	//维修申请的数量
	var weixiu = [];
	//报废申请的数量
	var baofei = [];
	//其他申请的数量
	var other = [];
	
	var param = Wade.DataMap();
	Common.callSvc(rootDirectory+"/chartsData/getAllApplyAssetID.do",param,function(resultData){
		resultData = Wade.DataMap(resultData);
		if("0" == resultData.get("msgFlag")){
			var resultJson = resultData.get('resultJson');
			var assetIds = resultJson.get('assetIds');
			var states = resultJson.get('states');
			assetIds.each(function(value1,index){
				assetIDs.push(value1);
			});
			states.each(function(value,index){
				value.each(function(val,ind){
					if("领用申请" == val.get('applyStateName')){
						lingyong.push(val.get('applyNumber'));
					}else if("入库申请" == val.get('applyStateName')){
						ruku.push(val.get('applyNumber'));
					}else if("维修申请" == val.get('applyStateName')){
						weixiu.push(val.get('applyNumber'));
					}else if("报废申请" == val.get('applyStateName')){
						baofei.push(val.get('applyNumber'));
					}else{
						other.push(val.get('applyNumber'));
					}
				});
			});
			productChart();
		}else{
			alert(resultData.get("errorMsg"));
		}
	});
	
	function productChart(){
		var chart = Highcharts.chart('container',{
		    chart: {
		        type: 'column'
		    },
		    title: {
		        text: '设备状态统计'
		    },
		    xAxis: {
		    	title: {
		            text: '设备编码'
		        },
		    	categories: assetIDs,
				crosshair: true
		    }, 
		    credits: { 
		    	enabled: false 
		    },
		    yAxis: {
		        min: 0,
		        title: {
		            text: '数量(个)'
		        }
		    },
		    tooltip: {
		        // head + 每个 point + footer 拼接成完整的 table
		        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
		        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
		        '<td style="padding:0"><b>{point.y:.1f} 个</b></td></tr>',
		        footerFormat: '</table>',
		        shared: true,
		        useHTML: true
		    },
		    plotOptions: {
		        column: {
		            borderWidth: 0
		        }
		    },
		    series:  [{
				name: '领用申请',
				data: lingyong
		    }, {
				name: '入库申请',
				data: ruku
		    }, {
				name: '维修申请',
				data: weixiu
		    }, {
				name: '报废申请',
				data: baofei
		    }, {
				name: '其他',
				data: other
		    }]
		});
	}
});

