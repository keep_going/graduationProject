
$(document).ready(function(){
	
	//发送请求的根目录
	var rootDirectory = $("#rootDirectory").val();
	//获取session中的信息 根据员工权限显示 操作按钮
	var sessionValue = $("#sessionInput").text();
	sessionValue = Wade.DataMap(sessionValue);
	if(sessionValue.length<1){
		alert("登录超时，请重新登录");
		window.location.href = rootDirectory+"/jsp/login/Login.jsp";
		return;
	}
	//获取
	var rightDetail = $("#detail").text();
	//页面赋值
	var arrayOld = initPage(rightDetail);
	
	
	
	//表单校验 是根据name属性 不是id
	$("#deviceInfoForm").validate({
		rules: {
			rights: "required"
		},
		messages: {
			rights: "不能为空"
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
		$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
		},
		//按钮提交事件
		submitHandler:function(form){
			var array = new Array();
			$.each($("input[type=checkbox]:checked"),function(){
				array.push($(this).val());
			});
			var param = Wade.DataMap();
			param.put("userId",$("#userId").val());
			param.put("rightsNew",array);
			param.put("rightsOld",arrayOld);
			
			Common.callSvc(rootDirectory+"/userRight/modifyUserRight.do",param,function(resultData){
				resultData = Wade.DataMap(resultData);
				console.log(resultData.toString());
				var resultJson = resultData.get('resultJson');
				if("0" == resultData.get("msgFlag")){
					alert("修改成功");
					window.location.href = rootDirectory+"/jsp/index/SuperIndex.jsp";
				}else{
					alert(resultData.get("errorMsg"));
				}
			});
		}
	});
});
//表单赋值
function initPage(rightDetail){
	var info = Wade.DataMap(rightDetail);
	var resultJson = info.get('resultJson');
	console.log(info.toString());
	
	$("#userId").val(resultJson.get('userId'));
	var userRight = resultJson.get('userRight');
	var arrayOld = new Array();
	
	for(var i=0;i<userRight.items.length;i++){
		$("input[type=checkbox]").each(function(){  
            if($(this).val()==userRight.items[i]){  
                $(this).attr("checked",true);  
            }  
        });
		arrayOld.push(userRight.items[i]);
	}
	return arrayOld;
}