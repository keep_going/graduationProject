
$(document).ready(function(){
	//发送请求的根目录
	var rootDirectory = $("#rootDirectory").val();
	//获取session中的信息 根据员工权限显示 操作按钮
	var sessionValue = $("#sessionInput").text();
	sessionValue = Wade.DataMap(sessionValue);
	//搜索按钮
	$("#searchBtn").click(function(){
		var roleId = $("#roleType").val();
		//加载表格
		dataStart(roleId,rootDirectory);
	});
});
/**
 * dataTable插件
 * @param roleId
 * @param rootDirectory
 * @returns
 */
function dataStart(roleId,rootDirectory){
	
	var table = $('.data-table').dataTable({
		"searching":true,//禁用搜索
		"bLengthChange": true,//屏蔽tables的一页展示多少条记录的下拉列表
        "pagingType": "full_numbers",
        // 每次创建是否销毁以前的DataTable,默认false
        "destroy": true,
        // 是否显示情报 就是"当前显示1/100记录"这个信息
        "info": false,
		"ajax": {
			url:rootDirectory+"/user/getAllUser.do",
			type: "POST",
            data:{
            	roleID:roleId
            },
            dataSrc: "resultJson"
           	
        },
		"columns":[
			{
				"data":"user_id",
			},{
				"data":"user_name",
			},{
				"data":"contact_phone",
			},{
				"data":"role_id",
				"render":function(data, type, full, meta){
					if(data == 2){
						data = "<p style='color:red;'>部门管理员</p>"
					}else if(data == 3){
						data = "<p style='color:red;'>普通员工</p>"
					}else{
						data = "<p style='color:red;'>暂未角色</p>"
					}
					return data;
				}
			},{
				"data":null,
				"render":function(data, type, full, meta) {
					return "<button class='btn btn-info' onclick='userRightDetial(" + JSON.stringify(full) + ")'>权限详情</button>"
				}
			}
		],
		
//		//列格式化(只能存在一个)l
//		"columnDefs":[{
//			targets:7,
//			render:function(data, type, row, meta){
//				alert(staffRight);
//				for(var i=0;i<staffRight.length;i++){
//					if(2 == staffRight.items[i]){
//						$(".li2").show();
//					}else if(3 == staffRight.items[i]){
//						$(".li3").show();
//					}else if(4 == staffRight.items[i]){
//						$(".li4").show();
//					}else if(5 == staffRight.items[i]){
//						$(".li5").show();
//					}
//				}
//			}
//		}]
	});
}
//修改
function userRightDetial(full){
	var url = "/fixed-assets-management/userRight/getUserRight.do?userId="+full.user_id;
	window.location.href=url;
}


