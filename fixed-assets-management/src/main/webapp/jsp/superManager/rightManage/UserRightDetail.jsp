<%@ page language="java" import="java.util.*,java.net.*" contentType="text/html;utf-8" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<jsp:include page="../../head.jsp"></jsp:include>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/select2.css" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/uniform.css" />
	<script src="${pageContext.request.contextPath}/resources/js/select2.min.js"></script>
	<script src="${pageContext.request.contextPath}/jsp/superManager/rightManage/userRightDetail.js" ></script>
	<title>权限管理详情(超管)</title>
	<style type="text/css">
		.queryClass {
			float: left;
			width: 120px;
			margin-left:5px;
			margin-top:2.7px; 
		}
	</style>
</head>

<body>
	<jsp:include page="../../head3.jsp"></jsp:include>	
	<input type="hidden" id="rootDirectory" value="${pageContext.request.contextPath}"/>
	<span style="display: none" id="detail">${userRightInfoStr }</span>
	<span style="display: none" id="sessionInput"><%=session.getAttribute("userSession")%></span>
	
	<div id="content">
		<div id="content-header">
			<div id="breadcrumb">
				<a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> 首页</a> 
				<a href="#" class="current">权限管理</a>
			</div>
			<h1>权限管理详情</h1>
		</div>
		
		<div class="container-fluid">
			<hr>
			<div class="row-fluid">
				<div class="span6">
					<div class="widget-box">
						<div class="widget-title">
							<span class="icon"> <i class="icon-align-justify"></i>
							</span>
							<h5>权限管理详情</h5>
						</div>
						<div class="widget-content nopadding">
							<form id="deviceInfoForm" class="form-horizontal">
								<div class="control-group">
									<label class="control-label">用户工号 :</label>
									<div class="controls">
										<input type="text" class="span11" id="userId" name="userId" disabled="disabled"/>
									</div>
								</div>
								
								<div class="control-group">
              						<label class="control-label">个人权限 :</label>
             						<div class="controls">
						                <label>
						                  <input type="checkbox" name="rights" value=1 />
						                  	入库</label>
						                <label>
						                  <input type="checkbox" name="rights" value=2 />
						                 	领用</label>
						                <label>
						                  <input type="checkbox" name="rights" value=3 />
						                 	流动</label>
						                <label>
						                  <input type="checkbox" name="rights" value=4 />
						                  	报废</label>
						                <label>
						                  <input type="checkbox" name="rights" value=5 />
						                  	维修</label>
						            </div>
						        </div>
								<div class="form-actions">
									<button type="submit" class="btn btn-success">修改</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--Footer-part-->
	<div class="row-fluid">
  		<div id="footer" class="span12"> 2018 &copy; Fixed Assets Management. Brought to you by <a href="http://themedesigner.in/">Themedesigner.in</a> </div>
	</div>
	<!--end-Footer-part-->
</body>
</html>