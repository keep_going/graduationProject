<%@ page language="java" contentType="text/html;utf-8" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
	<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap-responsive.min.css" />
	<link href="${pageContext.request.contextPath}/resources/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/matrix-style.css" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/matrix-media.css" />
	<script src="${pageContext.request.contextPath}/resources/js/jquery-3.2.1.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/jquery.ui.custom.js"></script> 
	<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script> 
	<script src="${pageContext.request.contextPath}/resources/js/jquery.validate.js"></script> 
	<script src="${pageContext.request.contextPath}/resources/js/jquery.wizard.js"></script> 
	<script src="${pageContext.request.contextPath}/resources/js/matrix.js"></script> 
	<script src="${pageContext.request.contextPath}/resources/js/wysihtml5-0.3.0.js"></script> 
	<!-- asiainfo内部commonJS  注意先后顺序，先Mobile.js 在WadeData.js 最后common.js-->
	<script src="${pageContext.request.contextPath}/resources/js/core/Mobile.js"></script> 
	<script src="${pageContext.request.contextPath}/resources/js/core/WadeData.js"></script> 
	<script src="${pageContext.request.contextPath}/resources/js/core/common.js"></script> 

