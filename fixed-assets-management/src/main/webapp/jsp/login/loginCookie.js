$(function() {
	//页面初始化给input框赋值
	if (getCookie('username') && getCookie('password')) {
		$('#user_id').val(getCookie('username'));
		$('#user_password').val(getCookie('password'));
		$('#autologin').prop('checked', 'checked');
	} else {
		$('#user_id').val('');
		$('#user_password').val('');
	}
	//登录
	$('#subBtn').click(function() {
		if ($('#autologin').prop('checked')) {
			var username = $('#user_id').val();
			var password = $('#user_password').val();
			setCookie("username", username);
			setCookie("password", password);
		} else {
			delCookie('username');
			delCookie('password');
		}
	});
	//主要函数 设置cookie
	function setCookie(name, value){
		var Days = 7;
		var exp = new Date();
		exp.setTime(exp.getTime() + Days * 24 * 60 * 60 * 1000);
		document.cookie = name + "=" + escape(value) + ";expires="
				+ exp.toGMTString();
	}
	//拿到cookie
	function getCookie(name){
		var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
		if (arr = document.cookie.match(reg)){
			return unescape(arr[2]);
		}else{
			return null;
		}
	}
	//删除cookie
	function delCookie(name){
		var exp = new Date();
		exp.setTime(exp.getTime() - 1);
		var cval = getCookie(name);
		if (cval != null){
			document.cookie = name + "=" + cval + ";expires=" + exp.toGMTString();
		}
	}
});





