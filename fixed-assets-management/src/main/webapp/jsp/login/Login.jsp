<%@ page language="java" import="java.util.*,java.net.*" contentType="text/html;utf-8" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
	<title>Fixed Assets Management</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jsp:include page="../head.jsp"></jsp:include>
	<script src="${pageContext.request.contextPath}/jsp/login/login.js"></script>
	<script src="${pageContext.request.contextPath}/jsp/login/loginCookie.js"></script>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/matrix-login.css" />
</head>
<body>
	<input type="hidden" id="rootDirectory" value="${pageContext.request.contextPath}"/>
	<div id="loginbox">
		<!-- 登录 -->
		<form id="loginform" class="form-vertical" action="${pageContext.request.contextPath}/user/loginCheck.do"  method="post">
			<div class="control-group normal_text">
				<h3><img src="${pageContext.request.contextPath}/resources/img/logo.png" alt="Logo" /></h3>
			</div>
			<div class="control-group">
				<div class="controls">
					<div class="main_input_box">
						<span class="add-on bg_lg"><i class="icon-user"></i></span><input type="text" id="user_id" name="user_id" placeholder="Username" />
					</div>
				</div>
			</div>
			<div class="control-group">
				<div class="controls">
					<div class="main_input_box">
						<span class="add-on bg_ly"><i class="icon-lock"></i></span><input type="password" id="user_password" name="user_password" placeholder="Password" />
					</div>
				</div>
			</div>
			<!-- 记住密码 -->
			<div class="checkbox">
        		<label><input type="checkbox" id="autologin" name="autologin">记住密码</label>
    		</div>
    		
			<div class="form-actions">
				<span class="pull-left"><a href="#" class="flip-link btn btn-info" id="to-recover">忘记密码?</a></span>
				<span class="pull-right"><button id="subBtn" type="submit" class="btn btn-success">登录</button></span>
			</div>
		</form>
		
		<!-- 忘记密码 -->
		<form id="recoverform" action="#" class="form-vertical">
			<p class="normal_text">Enter your e-mail address below and we will send you instructions how to recover a password.</p>
			<div class="controls">
				<div class="main_input_box">
					<span class="add-on bg_lo"><i class="icon-envelope"></i></span>
					<input type="email" id="emailInput" name="emailInput" placeholder="邮箱地址" />
				</div>
			</div>
			<div class="form-actions">
				<span class="pull-right"><button class="btn btn-info" id="sendEmailBtn">发送</button></span>
			</div>
			<div class="controls">
				<div class="main_input_box">
					<span class="add-on bg_lo"><i class="icon-key"></i></span>
					<input type="text" id="emailCode" name="emailCode" placeholder="验证码" />
				</div>
			</div>

			<div class="form-actions">
				<span class="pull-left"><a href="#" class="flip-link btn btn-success" id="to-login">&laquo; 返回登录</a></span>
				<span class="pull-right"><a class="btn btn-info" id="nextBtn">下一步</a></span>
			</div>
		</form>
	</div>
</body>
</html>