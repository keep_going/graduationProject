
$(document).ready(function(){
	//发送请求的根目录
	var rootDirectory = $("#rootDirectory").val();
	var InterValObj; //timer变量，控制时间 
	var count = 60; //间隔函数，1秒执行 
	var curCount;//当前剩余秒数 

	//表单验证
	$("#loginform").validate({
		rules:{
			user_id:"required",
			user_password:"required",
			emailInput:{
				required: true,
				email:true
			}
		},
		messages:{
			user_id:"Please enter your username",
			user_password:"Please enter your password",
			emailInput:{
				required:"Please enter your email",
				email:"Please enter the correct email format "
			}
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
		$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
		},
//		//登录按钮
//		submitHandler:function(form){
//    		var param = new Wade.DataMap();
//    		var username = $.trim($("#user_id").val());
//    		var password = $.trim($("#user_password").val());
//    		param.put("user_id",username);
//    		param.put("user_password",password);
//    		console.log(param.toString());
//    		//发送请求
//    		Common.callSvc(rootDirectory+"/user/loginCheck.do",param,function(resultData){});
//        }  
	});
	
	//发送邮件
	$("#sendEmailBtn").click(function(){
		curCount = count;
		$("#sendEmailBtn").attr("disabled","true");
		$("#sendEmailBtn").html(curCount + "秒后可重新发送");
		InterValObj = window.setInterval(SetRemainTime, 1000); //启动计时器，1秒执行一次 
		
		var email = $.trim($("#emailInput").val());
		if(email != "" || email != null){
			var isEmail = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/;
			if(!isEmail.test(email)){
				alert("请输入正确的邮箱格式！");
				return false;
			}
			var param = new Wade.DataMap();
			param.put("email",email);
			//发送请求
			Common.callSvc(rootDirectory+"/sendEmail.do",param,function(resultData){
				resultData = Wade.DataMap(resultData);
				if("0" == resultData.get("msgFlag")){
					//触发codeCheck事件
					$("#emailCode").trigger("codeCheck");
				}else{
					alert(resultData.get("errorMsg"));
				}
			});
			
		}else{
			alert("邮箱地址不能为空!");
			return false;
		}
	});
	
	$("#emailCode").bind("codeCheck",function(){
		// input框失焦事件 验证码比较是否正确
		$("#emailCode").bind("blur",function(){
			// 验证码
			var verificationCode = $.trim($("#emailCode").val());
			if(verificationCode == "" ||  verificationCode == null){
				alert("验证码不能为空！");
				return;
			}
			var param = new Wade.DataMap();
			param.put("verificationCode",verificationCode);
			
			Common.callSvc(rootDirectory+"/emailCodeCheck.do",param,function(resultData){
				resultData = Wade.DataMap(resultData);
				if("0" == resultData.get("msgFlag")){
					$('#nextBtn').attr("isClick","0"); 
				}else{
					alert(resultData.get("errorMsg"));
				}
			});
		});
	});
	//下一步
	$("#nextBtn").click(function(){
		var isClick_ = $('#nextBtn').attr("isClick");
		if("0" == isClick_){
			window.location.href = rootDirectory+"/jsp/findPassword/findPassword.jsp";
		}else{
			return;
		}
	});
	
	//timer处理函数 
	function SetRemainTime() {
		if(curCount == 0) {
			window.clearInterval(InterValObj); //停止计时器 
			$("#sendEmailBtn").removeAttr("disabled"); //启用按钮 
			$("#sendEmailBtn").html("重新发送验证码");
		} else {
			curCount--;
			$("#sendEmailBtn").html(curCount + "秒后可重新发送");
		}
	}
	
	$('#to-recover').click(function(){
		$("#loginform").slideUp();
		$("#recoverform").fadeIn();
	});
	$('#to-login').click(function(){
		$("#recoverform").hide();
		$("#loginform").fadeIn();
	});
});