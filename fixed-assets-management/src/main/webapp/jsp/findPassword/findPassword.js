
$(document).ready(function(){
	//发送请求的根目录
	var rootDirectory = $("#rootDirectory").val();
	
	//表单校验
	$("#form-wizard").validate({
		rules: {
			username: "required",
			password: {
				required: true,
		        minlength: 6
			},
			password2: {
				equalTo: "#password"
			}
		},
		messages: {
			username: "Please enter your name or username",
			password: {
				required: "You must enter the password",
				minlength: "The minimum password length is 6"
			},
			password2: { equalTo: "Password don't match" }
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
		$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
		},
		//按钮提交事件
		submitHandler:function(form){
    		var param = new Wade.DataMap();
    		param.put("password",$("#password").val());
    		param.put("username",$("#username").val());
    		console.log(param.toString());
    		//发送请求
    		Common.callSvc(rootDirectory+"/user/findPassword.do",param,function(resultData){
    			resultData = Wade.DataMap(resultData);
    			console.log(resultData.toString());
    			if("0" === resultData.get("msgFlag")){
    				//重定向到登录界面
    				alert("密码找回成功！");
    				window.location.href = rootDirectory+"/jsp/login/Login.jsp";
    			}else{
    				alert(resultData.get("errorMsg"));
    			}
    		});
        }  
	});
	
});
