<%@ page language="java" contentType="text/html;utf-8" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
	<title>Fixed Assets Management</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jsp:include page="../head.jsp"></jsp:include>
	<script src="${pageContext.request.contextPath}/jsp/findPassword/findPassword.js"></script>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/matrix-login.css" />
</head>
<body>
	<input type="hidden" id="rootDirectory" value="${pageContext.request.contextPath}"/>
	<div id="loginbox">
		<form id="form-wizard" class="form-horizontal" method="post">
			<div id="form-wizard-1" class="step" style="border-top: 1px solid #3f4954;">
				<div class="control-group">
					<label class="control-label" style="color: #fff">Username</label>
					<div class="controls">
						<input id="username" type="text" name="username" />
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" style="color: #fff">Password</label>
					<div class="controls">
						<input id="password" type="password" name="password" />
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" style="color: #fff">Confirm Password</label>
					<div class="controls">
						<input id="password2" type="password" name="password2" />
					</div>
				</div>
			</div>
			<!-- <div id="form-wizard-2" class="step">
				<div class="control-group">
					<label class="control-label">Email</label>
					<div class="controls">
						<input id="email" type="text" name="email" />
					</div>
				</div>
			</div> -->
			<div class="form-actions">
				<input id="subBtn" class="btn btn-primary" type="submit" value="Submit" />
			</div>
		</form>
	</div>
</body>
</html>