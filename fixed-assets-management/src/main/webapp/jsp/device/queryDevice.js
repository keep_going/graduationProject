
$(document).ready(function(){
	//发送请求的根目录
	var rootDirectory = $("#rootDirectory").val();
	//初始化设备类别列表
	initAssetClass(rootDirectory);
	//获取session中的信息 根据员工权限显示 操作按钮
	var sessionValue = $("#sessionInput").text();
	sessionValue = Wade.DataMap(sessionValue);
	var staffRight = sessionValue.get("staffRight");
	
	if(typeof staffRight == 'undefined'){
		console.error("staffRight == undefined");
	}else {
		/**
		 * staffRight
		 * 1	入库
		 * 2	领用
		 * 3	流动
		 * 4	报废
		 * 5	维修
		 */
		for(var i=0;i<staffRight.length;i++){
			if(1 == staffRight.items[i]){
				$("#addDeviceli").show();
			}
		}
	}
	
	
	//搜索按钮
	$("#searchBtn").click(function(){
		var assetName = $.trim($("#queryInput").val());
		var assetClass = $("#deviceType").val();
		
		var param = Wade.DataMap();
		if(assetName != "" || assetName != null){
			param.put("assetName",assetName);
		}
		if(assetClass != "" || assetClass != null){
			param.put("class_id",assetClass);
		}
		//加载表格
		dataStart(assetClass,assetName,rootDirectory,staffRight);
	
//		if(typeof staffRight == 'undefined'){
//			console.error("staffRight == undefined");
//		}else{
//			/**
//			 * staffRight
//			 * 2	领用
//			 * 3	流动
//			 * 4	报废
//			 * 5	维修
//			 */
//			for(var i=0;i<staffRight.length;i++){
//				if(2 == staffRight.items[i]){
//					$(".li2").show();
//				}else if(3 == staffRight.items[i]){
//					$(".li3").show();
//				}else if(4 == staffRight.items[i]){
//					$(".li4").show();
//				}else if(5 == staffRight.items[i]){
//					$(".li5").show();
//				}
//			}
//		}
	});
});
	

/**
 * 初始化 设备类别
 * @param rootDirectory
 * @returns
 */
function initAssetClass(rootDirectory){
	//调用接口为设备类型赋值
	Common.callSvc(rootDirectory+"/assetClass/getAssetClassList.do",null,function(resultData){
		resultData = Wade.DataMap(resultData);
		if("0" === resultData.get("msgFlag")){
			console.log(resultData.toString());
			var resultJson = resultData.get('resultJson');
			var assetClasses = resultJson.get('assetClass');
			
			for(var i=0;i<assetClasses.items.length;i++){
				jQuery("#deviceType").append('<option value='+assetClasses.items[i].get("classID")+'> '+assetClasses.items[i].get("className")+'</option>');
			}
		}else{
			alert(resultData.get("errorMsg"));
		}
	});
}
/**
 * dataTable插件
 * @param assetName
 * @param assetClass
 * @param rootDirectory
 * @returns
 */
function dataStart(assetClass,assetName,rootDirectory,staffRight){
	
	var table = $('.data-table').dataTable({
		"searching":true,//禁用搜索
		"bLengthChange": true,//屏蔽tables的一页展示多少条记录的下拉列表
        "pagingType": "full_numbers",
        // 每次创建是否销毁以前的DataTable,默认false
        "destroy": true,
        // 是否显示情报 就是"当前显示1/100记录"这个信息
        "info": false,
		"ajax": {
			url:rootDirectory+"/assetsInfo/getAllAssetInfoByClassAndName.do",
			type: "POST",
            data:{
            	class_id:assetClass,
            	assetName:assetName
            },
            dataSrc: "tableData"
           	
        },
		"columns":[
			{
				"data":"assetID",
			},{
				"data":"assetName",
			},{
				"data":"manufacture",
			},{
				"data":"class_id",
			},{
				"data":"remark",
			},{
				"data":"surplusAsset",
			},{
				"data":"inventory",
			},{
				"data":null,
				"render":function(data, type, full, meta) {
					return '<div class="btn-group">'
		            			+'<button data-toggle="dropdown" class="btn btn-info dropdown-toggle">操作 <span class="caret"></span></button>'
		            			+'<ul class="dropdown-menu">'
		            				+'<li class="li2" ><a href="#">领用</a></li>'
		            				+'<li class="li3" ><a href="#">流动</a></li>'
		            				+'<li class="li4" ><a href="#">报废</a></li>'
		            				+'<li class="li5" style="display:none;"><a href="#">维修</a></li>'
		            			+'</ul>'
		            		+'</div>';
				}
			}
		],
		
//		//列格式化(只能存在一个)l
//		"columnDefs":[{
//			targets:7,
//			render:function(data, type, row, meta){
//				alert(staffRight);
//				for(var i=0;i<staffRight.length;i++){
//					if(2 == staffRight.items[i]){
//						$(".li2").show();
//					}else if(3 == staffRight.items[i]){
//						$(".li3").show();
//					}else if(4 == staffRight.items[i]){
//						$(".li4").show();
//					}else if(5 == staffRight.items[i]){
//						$(".li5").show();
//					}
//				}
//			}
//		}]
	});
	
}


