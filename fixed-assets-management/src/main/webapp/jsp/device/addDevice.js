
$(document).ready(function(){
	//发送请求的根目录
	var rootDirectory = $("#rootDirectory").val();
	//调用接口为设备类型赋值
	getDeviceTypeParam(rootDirectory);
	
	//获取session中的信息 根据员工权限显示 操作按钮
	var sessionValue = $("#sessionInput").text();
	sessionValue = Wade.DataMap(sessionValue);
	if(sessionValue.length<1){
		alert("登录超时，请重新登录");
		window.location.href = rootDirectory+"/jsp/login/Login.jsp";
	}
	//用户信息
	var userInfo = sessionValue.get("userInfo");
	//员工权限
	var staffRight = sessionValue.get("staffRight");
	if(userInfo == '' || userInfo == null){
		console.error("session信息sessionValue undefined");
	}else{
		/**
		 * staffRight
		 * 1	入库
		 * 2	领用
		 * 3	流动
		 * 4	报废
		 * 5	维修
		 */
		for(var i=0;i<staffRight.length;i++){
			if(1 == staffRight.items[i]){
				$("#addDeviceli").show();
			}
		}
		//员工工号
		var staffID = userInfo.get('staffID');
		$("#applyPerson").val(staffID);
		$("#applyPerson").attr("disabled",true);
	}
	
	/*
	 * 填写设备编码input的失焦事件
	 * 如果deviceType 为添加设备数量
	 * 		则填写玩设备编码后到后台查询表中是否有该设备，
	 * 		有  则把该设备的相关信息带出来
	 * 		无  则提示没找到对应的设备信息
	 * 否则不执行操作
	 */
	
	$("#deviceId").bind("blur",function(){
		var assetType = $("#deviceInType").val();
		if("0" == assetType){
			var param = new Wade.DataMap();
			param.put("assetID",$("#deviceId").val());
			Common.callSvc(rootDirectory+"/assetsInfo/getAssetInfoByID.do",param,function(resultData){
				resultData = Wade.DataMap(resultData);
				console.log(resultData.toString());
				var resultJson = resultData.get('resultJson');
				if("0" == resultData.get('msgFlag')){
					//赋值
					$("#deviceId").val(resultJson.get('assetID'));
					$("#deviceId").attr("disabled",true);
					$("#deviceName").val(resultJson.get('assetName'));
					$("#deviceName").attr("disabled",true);
					$("#deviceType").val(resultJson.get('class_id'));
					$("#deviceType").attr("disabled",true);
				}else{
					alert(resultData.get('errorMsg'));
				}
			});
		}
	});
	
	
	//表单校验 是根据name属性 不是id
	$("#addDeviceForm").validate({
		rules: {
			deviceId: "required",
			deviceName: "required",
			applyNumber: "required",
			applyPerson: "required",
			applyReason: "required",
			deviceType: "required",
		},
		messages: {
			deviceId: "请输入设备编码",
			deviceName: "请输入设备名称",
			applyNumber: "请填入设备入库的数量",
			applyPerson: "请输入申请人工号",
			applyReason: "请输入申请原因",
			deviceType: "请选择设备类别",
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
		$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
		},
		//按钮提交事件
		submitHandler:function(form){
    		var param = new Wade.DataMap();
    		param.put("assetID",$("#deviceId").val());
    		param.put("assetName",$("#deviceName").val());
    		param.put("applyNumber",$("#applyNumber").val());
    		param.put("applyPerson",$("#applyPerson").val());
    		param.put("assetType",$("#deviceType").val());
    		param.put("applyReason",$("#applyReason").val());
    		param.put("assetInType",$("#deviceInType").val());
    		//applyState 申请标识 设备入库
    		param.put("applyState",1);
    		console.log(param.toString());
    		//发送请求
    		Common.callSvc(rootDirectory+"/assetApply/addAssetApplication.do",param,function(resultData){
    			resultData = Wade.DataMap(resultData);
    			console.log(resultData.toString());
    			if("0" === resultData.get("msgFlag")){
    				alert("设备入库申请成功，等待审批！");
    				//跳转到首页
    				window.location.href = rootDirectory+"/jsp/index/CommonIndex.jsp";
    			}else{
    				alert(resultData.get("errorMsg"));
    			}
    		});
        }
	});
	
});
/**
 * 获取设备类型参数
 * @returns
 */
function getDeviceTypeParam(rootDirectory){
	Common.callSvc(rootDirectory+"/assetClass/getAssetClassList.do",null,function(resultData){
		resultData = Wade.DataMap(resultData);
		if("0" === resultData.get("msgFlag")){
			console.log(resultData.toString());
			var resultJson = resultData.get('resultJson');
			var assetClasses = resultJson.get('assetClass');
			
			for(var i=0;i<assetClasses.items.length;i++){
				jQuery("#deviceType").append('<option value='+assetClasses.items[i].get("classID")+'> '+assetClasses.items[i].get("className")+'</option>');
			}
		}else{
			alert(resultData.get("errorMsg"));
		}
	});
}

