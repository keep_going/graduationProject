
$(document).ready(function(){
	//发送请求的根目录
	var rootDirectory = $("#rootDirectory").val();
	var sessionValue = $("#sessionInput").text();
	sessionValue = Wade.DataMap(sessionValue);
	if(sessionValue.length<1){
		alert("登录超时，请重新登录");
		window.location.href = rootDirectory+"/jsp/login/Login.jsp";
	}
	var staffRight = sessionValue.get("staffRight");
	//用户信息
	var userInfo = sessionValue.get("userInfo");
	//员工工号
	var staffID = userInfo.get('staffID');
	if(typeof staffRight == 'undefined'){
		console.error("staffRight == undefined");
	}else {
		/**
		 * staffRight
		 * 1	入库
		 * 2	领用
		 * 3	流动
		 * 4	报废
		 * 5	维修
		 */
		for(var i=0;i<staffRight.length;i++){
			if(1 == staffRight.items[i]){
				$("#addDeviceli").show();
			}
		}
	}
	/**
	 * new Date().getDaysAgoDate(1) 调用的是DateUtil插件
	 */
	$("#startTime").val(new Date().getDaysAgoDate(1));
	$("#endTime").val(new Date().getCurrentDate());
//	console.log(new Date().getDaysAgoDate(1));
//	console.log(new Date().getCurrentDate());
	//查询
	$("#searchBtn").click(function(){
		var startTime = $("#startTime").val();
		var endTime = $("#endTime").val();
		if(!CompareDate(startTime,endTime)){
			alert("开始时间不能大于结束时间");
			return;
		}
		//生成表单
		dataTableInit(startTime,endTime,staffID,rootDirectory);
	});
});
function dataTableInit(startTime,endTime,staffID,rootDirectory){
	var table = $('.data-table').dataTable({
		"pagingType": "full_numbers",
		"aLengthMenu": [2, 5, 10, 15],//设置一页展示2条记录
		"bServerSide": true,//这个用来指明是通过服务端来取数据
		"info": true,
		// 每次创建是否销毁以前的DataTable,默认false
        "destroy": true,
		"ajax": {
			url:rootDirectory+"/assetApply/getStaffSelfAssetApplication.do",
			type: "POST",
			data:{
//				startTime:startTime,
//				endTime:endTime,
				applyPerson:staffID
			},
            dataSrc: "resultJson.tableData"
        },
        "columns":[
        	{
        		"data":"assetID",
        	},{
        		"data":"assetName",
        	},{
        		"data":"applyNumber",
        	},{
        		"data":"applyPerson",
        	},{
        		"data":"applyDate",
        		"render":function(data,type,full,meta){
        			data = new Date(data).Format("yyyy-MM-dd");
        			return data;
        		}
        	},{
        		"data":"applyReason",
        	},{
        		"data":"applyState",
        		"render":function(data, type, full, meta){
        			if(data == 0){
        				data = "领用申请";
        			}else if(data == 1){
        				data = "入库申请";
        			}else if(data == 2){
        				data = "流动申请";
        			}else if(data == 3){
        				data = "维修申请";
        			}else if(data == 4){
        				data = "报废申请";
        			}else{
        				data = "无";
        			}
        			return data;
        		}
        	},{
        		"data":"approvePesron1"
        	},{
        		"data":"approvePesron2",
        		"render":function(data, type, full, meta){
        			if(!data || typeof data == 'underfined'){
        				data = "";
        			}
        			return data;
        		}
        	},{
        		"data":"approveState",
        		"render":function(data, type, full, meta){
        			if(data == 0){
        				data = "待审核";
        			}else if(data == 1){
        				data = "部门管理员通过";
        			}else if(data == 2){
        				data = "部门管理员不通过";
        			}else if(data == 3){
        				data = "审批成功";
        			}else if(data == 4){
        				data = "审批失败";
        			}else{
        				data = "无";
        			}
        			return data;
        		}
        	},{
        		"data":"approveRemork",
        		"render":function(data,type,full,meta){
        			if(!data || typeof data == 'underfined'){
        				data = "";
        			}
        			return data;
        		}
        	}
        ],
	});
}
//比较两个日期的大小
function CompareDate(d1,d2){
  return ((new Date(d1.replace(/-/g,"\/"))) <= (new Date(d2.replace(/-/g,"\/"))));
}
