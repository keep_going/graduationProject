<%@ page language="java" import="java.util.*,java.net.*" contentType="text/html;utf-8" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<title>入库新增</title>
<meta content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<jsp:include page="../head.jsp"></jsp:include>
<script src="${pageContext.request.contextPath}/jsp/device/addDevice.js"></script>
</head>
<body>
	<jsp:include page="../head2.jsp"></jsp:include>	
	<input type="hidden" id="rootDirectory" value="${pageContext.request.contextPath}"/>
	<span style="display: none" id="sessionInput"><%=session.getAttribute("userSession")%></span>
	<!--sidebar-menu-->
	<div id="sidebar">
		<ul>
			<li>
				<a href="${pageContext.request.contextPath}/jsp/index/CommonIndex.jsp">
					<i class="icon icon-home"></i> <span>首页</span>
				</a>
			</li>
			<li>
				<a href="${pageContext.request.contextPath}/jsp/device/SelfDeviceManage.jsp">
					<i class="icon icon-pencil"></i> <span>个人设备管理</span>
				</a>
			</li>
			<li>
				<a href="${pageContext.request.contextPath}/jsp/device/QueryDevice.jsp">
					<i class="icon icon-search"></i> <span>设备查询</span>
				</a>
			</li>
			<li id="addDeviceli" style="display: none;">
				<a href="${pageContext.request.contextPath}/jsp/device/AddDevice.jsp">
					<i class="icon icon-plus-sign"></i> <span>设备入库</span>
				</a>
			</li>
			<li>
				<a href="${pageContext.request.contextPath}/jsp/userInfo/StaffInfo.jsp">
					<i class="icon icon-plus-sign"></i> <span>个人信息</span>
				</a>
			</li>
			
		</ul>
	</div>
	<!--sidebar-menu-->

	<!--close-left-menu-stats-sidebar-->

	<div id="content">
		<div id="content-header">
			<div id="breadcrumb">
				<a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> 首页</a> 
				<a href="#" class="current">设备入库</a>
			</div>
			<h1>设备入库</h1>
		</div>
		<div class="container-fluid">
			<hr>
			<div class="row-fluid">
				<div class="span6">
					<div class="widget-box">
						<div class="widget-title">
							<span class="icon"> <i class="icon-align-justify"></i>
							</span>
							<h5>设备入库申请信息</h5>
						</div>
						<div class="widget-content nopadding">
							<form action="#" method="get" id="addDeviceForm" class="form-horizontal">
								<div class="control-group">
									<label class="control-label">入库类型 :</label>
									<div class="controls">
										<select id="deviceInType" class="span11">
											<option value="0">添加设备数量</option>
											<option value="1">新增设备类型</option>
										</select>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">设备编码 :</label>
									<div class="controls">
										<input type="text" class="span11" id="deviceId" name="deviceId" placeholder="请输入设备编码" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">设备名称 :</label>
									<div class="controls">
										<input type="text" class="span11" id="deviceName" name="deviceName" placeholder="请输入设备名称" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">入库数量 :</label>
									<div class="controls">
										<input type="number" class="span11" id="applyNumber" name="applyNumber" placeholder="入库数量" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">申请人 :</label>
									<div class="controls">
										<input type="text" class="span11" id="applyPerson" name="applyPerson" placeholder="请输入申请人员工id" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">设备类别 :</label>
									<div class="controls">
										<select id="deviceType" class="span11" name="deviceType">
											<option value="" selected>---设备类别---</option>
										</select>
									</div>
								</div>
								<!-- <div class="control-group">
									<label class="control-label">Description field:</label>
									<div class="controls">
										<input type="text" class="span11" /> <span class="help-block">Description
											field</span>
									</div>
								</div> -->
								<div class="control-group">
									<label class="control-label">入库申请原因 :</label>
									<div class="controls">
										<textarea class="span11" id="applyReason" name="applyReason"></textarea>
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" class="btn btn-success">提交</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--Footer-part-->
	<div class="row-fluid">
		<div id="footer" class="span12"> 2018 &copy; Fixed Assets Management. Brought to you by <a href="http://themedesigner.in/">Themedesigner.in</a> </div>
	</div>
</body>
</html>
